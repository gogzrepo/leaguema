<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@page session="true"%>
<html>
<head>
<meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title>User Form</title>
<link rel="stylesheet" href="resources/css/common.css">
</head>
<body>
	<c:url value="/logout" var="logoutUrl" />
	<form action="${logoutUrl}" method="post" id="logoutForm">
		<input type="hidden" name="${_csrf.parameterName}"
			value="${_csrf.token}" />
	</form>
	<script>
		function formSubmit() {
			document.getElementById("logoutForm").submit();
		}
	</script>
<table align="right">
<tr><td>
	<c:if test="${pageContext.request.userPrincipal.name != null}">
		<h5>
			Welcome : ${pageContext.request.userPrincipal.name} | <a
				href="javascript:formSubmit()"> Logout</a>
		</h5>
	</c:if>
	</td>
	</tr>
	</table>
	<h1 align="center">User Creation Form</h1>
	<h4 align="center">
			${msg}
		</h4>
		<div class="CommonDiv">
        <table class="PageTitle"><tr><td>Create New User</td></tr></table>
        <table class="CommonTable" ><tr>
	<form:form action="${pageContext.request.contextPath}/user/userMaintenanceAction" name="userForm" modelAttribute="stuRegForm" method="post" >
	<Table align="center" border="0">
	<Tr>
	<Td>Username</Td><Td><form:input path="username" id="username"/></Td>
	<Td>Password</Td><Td><form:password path="password" id="password"/></Td>
	</Tr>
	<Tr>
	<Td>Enable</Td><Td><form:checkbox path="enabled" id="enabled"/></Td>
	<Td>Employee Code</Td><Td><form:input path="userEmpCode" id="userEmpCode"/></Td>
	</Tr>
	<Tr>
	<Td>First Name</Td><Td><form:input path="userFName" id="userFName"/></Td>
	<Td>Last Name</Td><Td><form:input path="userLName" id="userLName"/></Td>
	</Tr>
	<Tr>
	<Td>Gender</Td><Td>
	<form:radiobutton path="userGender" id="male"/> Male
	<form:radiobutton path="userGender" id="female"/> Female
	</Td>
	</Tr>
	<Tr>
	<Td>Designation</Td><Td><form:input path="userDesgId" id="userDesgId"/></Td>
	<Td>Main Branch</Td><Td><form:input path="userMainBranch" id="userMainBranch"/></Td>
	</Tr>
	<Tr>
	<Td>OTP</Td><Td><form:input path="userOTP" id="userOTP"/></Td>
	<Td>Category</Td><Td><form:input path="userCategory" id="userCategory"/></Td>
	</Tr>
	<Tr>
	<Td>Joining Date</Td><Td><form:input path="userJoinDate" id="userJoinDate"/></Td>
	<Td>Status</Td><Td><form:input path="userStatus" id="userStatus"/></Td>
	</Tr>
	<Tr>
	<Td>Multi Branch</Td><Td><form:input path="userMultiBrh" id="userMultiBrh"/></Td>
	<Td>Auth Power</Td><Td><form:input path="userAuthPower" id="userAuthPower"/></Td>
	</Tr>
	<Tr>
	<Td>Day OC Power</Td><Td><form:checkbox path="userDayOCPower" id="userDayOCPower"/></Td>
	<Td>Pwd Expired</Td><Td><form:checkbox path="userIsPwdExpired" id="userIsPwdExpired"/></Td>
	</Tr>
	<Tr>
	<Td>Pwd Expired Days</Td><Td><form:input path="userPwdExpiredDays" id="userPwdExpiredDays"/></Td>
	<Td></Td><Td></Td>
	</Tr>
	<Tr>
	<Td></Td><Td colspan="2" align="center">&nbsp;</Td><Td></Td>
	</Tr>
	<Tr>
	<Td></Td><Td colspan="2" align="center"><input type="submit" name="submit" /></Td><Td></Td>
	</Tr>
	</Table>
	<input type="hidden" name="${_csrf.parameterName}"
			value="${_csrf.token}" />
	</form:form>
	<tr><td></td></tr></table>
            </div>
	

</body>
</html>