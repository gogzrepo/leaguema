<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>${title}</title>

<%@include file="clubHeader.jsp"%>
</head>
<body class="hold-transition skin-blue sidebar-mini" onload="getRegistrationData()">
	<div class="wrapper">

		<%@include file="clubBodyHeader.jsp"%>
		<!--[START] Left side column. contains the logo and sidebar -->
		<%@include file="clubLeftSidebar.jsp"%>
		<!--[END] Left side column. contains the logo and sidebar -->

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>
					Access Denied
				</h1>
			</section>

			<!-- Main content -->
			<section class="content">
				<div class="row">
					<!-- left column -->

					<!--/.col (left) -->
					<!-- right column -->
					<div class="col-md-10">
						<!-- Horizontal Form -->
						<!-- /.box -->
						<!-- general form elements disabled -->
						<div class="box box-info">
							<div class="box-header with-border">
							<h1>HTTP Status 403 - Access is denied</h1>
							</div>
							<!-- /.box-header -->
							<div class="box-body">
							<c:choose>
		<c:when test="${empty username}">
			<h2>You do not have permission to access this page!</h2>
		</c:when>
		<c:otherwise>
			<h2>Username : ${username} <br/>You do not have permission to access this page!</h2>
		</c:otherwise>
	</c:choose>
								<form name="clubForm">
									<!-- text input -->
									<Table width="100%">
										<Tr>
											<Td valign="top" width="49%">
												<div class="form-group">
																	

													
												</div>
											</Td>
											
										</Tr>
										
									</Table>
									 
									<!-- input states -->
								</form>
								<label><a href="${pageContext.request.contextPath}" class="btn btn-default btn-flat">Back to Dahsborad</a></label>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
					</div>
					<!--/.col (right) -->
				</div>
				<!-- /.row -->
			</section>
			<!-- /.content -->
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->
		<%@include file="clubFooter.jsp"%>

		<!-- Control Sidebar -->
		
		<!-- /.control-sidebar -->
		<!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
		<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->

</body>
<script>
</script>
</html>
