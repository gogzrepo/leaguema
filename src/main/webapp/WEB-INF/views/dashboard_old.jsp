<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<html>

<head>

 <%
            String [][] treeMenuData = null;
   
            if(session.getAttribute("UserMenuData")!=null){
                treeMenuData = (String[][]) session.getAttribute("UserMenuData");
            }
        %>

 <STYLE>
   body { 
    background-color: #f0f4fe;
    margin: 0px;
   }
    A {
      text-decoration: none;
      color: black;}
    .specialClass {
      font-family:garamond; 
      font-size:12pt;
      color:green;
      font-weight:bold;
      text-decoration:underline;
    }
    .TreeTable {
       border: 2px groove black;
       // border: 5px;
    }
    hr {
height: 2px;
color: #123455;
background-color: darkgray;
border: none;
}

  </STYLE>
  <script src="/BioMatApp/resources/commonJS/ua.js"></script>
  <script src="/BioMatApp/resources/commonJS/treeMenu.js"></script>
  
  <script type="text/javascript">
  function forwardMenu(menuNext) {
	  
	  //alert("menu No is-->"+menuNext);
	  document.forms[1].menuNext.value = ""+menuNext;
	  document.forms[1].action = "/BioMatApp/cmnurl/openMenuPage?menuNext="+menuNext;
	  //document.forms[1].method = "POST";
	  document.forms[1].submit();
	  
  }
  
  </script>
  
  
  <script>
   
var    USETEXTLINKS = 1
    
// Configures whether the tree is fully open upon loading of the page, or whether
// only the root node is visible.
var STARTALLOPEN = 0
// Specify if the images are in a subdirectory;
var ICONPATH = '/BioMatApp/resources/commonImages/';
var foldersTree = gFld("<i>Main Menu</i>", "");
  foldersTree.treeID = "Frameset";
  
  
  <% int len = treeMenuData.length;
  String au = "aux1";
  System.out.println("Length Of Menu Data Is---->>"+treeMenuData.length);
  for(int i=0;i<len;i++) {
      
      if(treeMenuData[i][1].equals("0")) {  // Means Folder
          if(treeMenuData[i][1].equals("0") && treeMenuData[i][2].equals("0")){  // Means Parent Folder
              au = "aux1";
              %>
          aux1 = insFld(foldersTree, gFld("<%= treeMenuData[i][0] %>", ""));
      <%
          }else { // Means Child Folder
              au = "aux2";
              %>
               aux2 = insFld(aux1, gFld("<%= treeMenuData[i][0] %>", "http://www.treeview.net/treemenu/demopics/beenthere_america.gif"))
     <%
          }
       } else if(!treeMenuData[i][1].equals("0")) {  // Means it is Menu
              if(!treeMenuData[i][1].equals("0") && treeMenuData[i][2].equals("0")) {// Means Main Menu
          %>
              insDoc(foldersTree, gLnk("S", "<%= treeMenuData[i][0] %>", "<%= treeMenuData[i][1] %>"));
    <%
      }else {  if(au.equals("aux1")) { %>
                insDoc(aux1, gLnk("S", "<%= treeMenuData[i][0] %>", "<%= treeMenuData[i][1] %>"));
                <%      } else if(au.equals("aux2")) { %>
                insDoc(aux2, gLnk("S", "<%= treeMenuData[i][0] %>", "<%= treeMenuData[i][1] %>"));
                <%  
                }
            }
        }
  }
  %>
 </script>
  
</head>

<body>

	
	
	    <body bgcolor="#e7f3f3">
    
        
            <table style="background: #123455;width: 100%;height: 10%;" border="0">
            <tr><td style="width: 20%;color: whitesmoke;">Space For Company's Logo..</td>
                <td align="center" style="width: 60%;color: whitesmoke;"><h2>${message}</h2>
                </td><Td align="right" style="width: 20%;color: whitesmoke;">
                <sec:authorize access="hasRole('ROLE_USER')">
		<!-- For login user -->
		<c:url value="/logout" var="logoutUrl" />
		<form action="${logoutUrl}" method="post" name="abcd" id="logoutForm">
			<input type="hidden" name="${_csrf.parameterName}"
				value="${_csrf.token}" />
		</form>
		<script>
			function formSubmit() {
				document.getElementById("logoutForm").submit();
			}
		</script>

		<c:if test="${pageContext.request.userPrincipal.name != null}">
			
				<h5>User : ${pageContext.request.userPrincipal.name} | <a
					href="javascript:formSubmit()"><font color="whitesmoke"> Logout</font></a></h5>
			
		</c:if>

	</sec:authorize>
                </td></tr>
        </table>
        <form name="mnudata" action="/cmnurl/openMenuPage">
        <div style="position: absolute;width: 100%;height: 90%;padding:0;overflow: auto;">
        <input type="hidden" name="menuNext" id="menuNext" />
        <table border="1" style="border-spacing:0;" width="100%" height="100%" ><tr>
            <td style="width: 25%;vertical-align: top;padding-left: 30;"><br>
             <A href="http://www.treemenu.net/" target=_blank > 
             </A>
                <SCRIPT>initializeDocument()</SCRIPT>
            </td> 
            <td style="width: 700px;"></td>
            </table>          
        </div>
 </form>
 
  <NOSCRIPT>
   A tree for site navigation will open here if you enable JavaScript in your browser.
  </NOSCRIPT>
    
	
	
</body>
</html>