<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Registration Page</title>

<%@include file="clubHeader.jsp"%>
<%@page import="com.leaguema.model.ClubRegistration"%>
<%
		ClubRegistration clubRegistration = new ClubRegistration();
		if (request.getAttribute("regData") != null)
			clubRegistration = (ClubRegistration) request.getAttribute("regData");
		System.out.println("-------------->" + clubRegistration.getClubName());
	%>
</head>
<body class="hold-transition skin-blue sidebar-mini"
	onload="loadEditData()">
	
	<div class="wrapper">

		<%@include file="clubBodyHeader.jsp"%>
		<!--[START] Left side column. contains the logo and sidebar -->
		<%@include file="clubLeftSidebar.jsp"%>
		<!--[END] Left side column. contains the logo and sidebar -->

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>${pageHeading}</h1>
			</section>

			<!-- Main content -->
			<section class="content">
				<div class="row">
					<!-- left column -->

					<!--/.col (left) -->
					<!-- right column -->
					<div class="col-md-10">
						<!-- Horizontal Form -->
						<!-- /.box -->
						<!-- general form elements disabled -->
						<div class="box box-info">
							<div class="box-header with-border">
								<h3 class="box-title">${message }</h3>
							</div>
							<!-- /.box-header -->
							<div class="box-body">
								<form name="clubForm">
									<!-- text input -->
									<Table width="100%">
										<Tr>
											<Td valign="top" width="49%">
												<div class="form-group">
													<label>Club Name</label> <input type="text" id="clubName"
														class="form-control"  data-error="enter club name" placeholder="Enter Club Name" required="true" />
												</div>
											</Td>
											<Td width="2%">&nbsp;</Td>
											<Td width="49%">


												<div class="form-group">
													<label>Club Code</label> <input type="text"
														id="clubCodeShow" class="form-control" disabled>
												</div>
											</Td>
										</Tr>

										<Tr>
											<Td>
												<div class="form-group">
													<label>Club Address</label>
													<textarea id="clubAddress" class="form-control" rows="3"
														placeholder="Enter Club Address"></textarea>
												</div>
											</Td>
											<Td></Td>
											<Td><label>Type of Club</label> <select
												class="form-control" id="clubType">
													<option value="Sub">Sub</option>
													<option value="Super">Super</option>
											</select></Td>

										</Tr>

										<Tr>
											<Td valign="top">
												<div class="form-group">
													<label>Contact Person First Name</label> <input type="text"
														id="clubFName" class="form-control"
														placeholder="Enter Contact Person First Name">
												</div>
											</Td>
											<Td>&nbsp;</Td>
											<Td>
												<div class="form-group">
													<label>Contact Person Last Name</label> <input type="text"
														id="clubLName" class="form-control"
														placeholder="Enter Contact Person Last Name">
												</div>
											</Td>
										</Tr>

										<Tr>
											<Td valign="top">
												<div class="form-group">
													<label>Website</label> <input type="text"
														class="form-control" name="clubWebsite" id="clubWebsite"
														placeholder="Enter Website">
												</div>
											</Td>
											<Td>&nbsp;</Td>
											<Td>
												<div class="form-group">
													<label>Email</label> <input type="email"
														class="form-control" id="clubEmail" placeholder="Email" required = "true">
												</div>
											</Td>
										</Tr>

										<Tr>
											<Td valign="top">
												<div class="form-group">
													<label>Belt Type</label> <select class="form-control"
														id="clubBeltType">
														<option value="None">None</option>
														<option value="Green">Green</option>
														<option value="Red">Red</option>
														<option value="Black">Black</option>
														<option value="Red Black">Red Black</option>
														<option value="Red Green">Red Green</option>
													</select>
												</div>
											</Td>
											<Td>&nbsp;</Td>
											<Td>
												<div class="form-group">
													<label>Certificate No</label> <input type="text"
														class="form-control" id="clubCertificateNo"
														placeholder="Enter Your Certificate No">
												</div>
											</Td>
										</Tr>

										<Tr>
											<Td valign="top">
												<div class="form-group">
													<label>Upload Certificate File</label> <input type="file"
														class="form-control" id="clubCertificateFile">
												</div>
											</Td>
											<Td>&nbsp;</Td>
											<Td>
												<div class="form-group">
													<label>Upload Id Card</label> <input type="file"
														class="form-control" id="clubCertificateIdCardFile">
												</div>
											</Td>
										</Tr>

										<Tr>
											<Td valign="top">
												<div class="form-group">
													<label>Club Status</label> <select class="form-control"
														id="isActive" disabled>
														<option value="false">Inactive</option>
														<option value="true">Active</option>
													</select>
												</div>
											</Td>
											<Td>&nbsp;</Td>
											<Td>
												<div class="form-group">
													<label>Belt No</label> <input type="text"
														class="form-control" id="clubBeltNo"
														placeholder="Enter Your Belt No">
												</div>
											</Td>
										</Tr>

										<Tr>
											<Td valign="top">
												<div class="form-group">
													<label>No of Student</label> <input type="text"
														class="form-control" id="clubNoOfStudent"
														placeholder="Enter Club Limit">
												</div>
											</Td>
											<Td>&nbsp;</Td>
											<Td>
												<div class="form-group">
													<label>Level Of Club</label> <select class="form-control"
														id="clubLevel">
														<option value="None">None</option>
														<option value="District">District</option>
														<option value="State">State</option>
														<option value="National">National</option>
													</select>
												</div> <input type="hidden" id="id" /> <input type="hidden"
												id="clubCode" />
											</Td>
										</Tr>
										<Tr>
											<Td colspan="3"><Hr></Td>
										</Tr>
										<Tr>
											<Td valign="top">
												<div class="form-group">
													<label>Cell No</label> <input type="text"
														class="form-control" id="mobileNo"
														placeholder="Enter Cell No">
												</div>
											</Td>
											<Td>&nbsp;</Td>
											<Td>
												<div class="form-group">
													<label>Role In Club</label> <select class="form-control"
														id="roleInClub">
														<option value="ROLE_CLUB_ADMIN">ROLE_CLUB_ADMIN</option>
														<option value="ROLE_CLUB_MEMBER">ROLE_CLUB_MEMBER</option>
														<option value="ROLE_SUPER_ADMIN">ROLE_SUPER_ADMIN</option>
														<option value="ROLE_CLUB_DBA">ROLE_CLUB_DBA</option>
														<option value="ROLE_CLUB_USER">ROLE_CLUB_USER</option>
													</select>
												</div>
											</Td>
										</Tr>
										<Tr>
											<Td valign="top">
												<div class="form-group">
													<label>Password</label> <input type="password"
														class="form-control" id="Pwd"
														placeholder="Enter Your Password">
												</div>
											</Td>
											<Td>&nbsp;</Td>
											<Td>
												<div class="form-group">
													<label>Confirm Password</label> <input type="password"
														class="form-control" id="clubPwd"
														placeholder="Enter Your Confirm Password">
												</div>
											</Td>
										</Tr>

										<Tr>
											<Td colspan="3" align="center">
												<div class="box-footer">
													<input type="button" class="btn btn-primary" value="Save"
														onClick="saveOrUpdateRegistration('${operationType}')" />
													<input type="button" class="btn btn-primary" value="Cancel"
														onClick="backToPage('${operationType}')" />
												</div>
											</Td>
										</Tr>

									</Table>

									<!-- input states -->
								</form>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
					</div>
					<!--/.col (right) -->
				</div>
				<!-- /.row -->
			</section>
			<!-- /.content -->
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->
		<%@include file="clubFooter.jsp"%>

		<!-- Control Sidebar -->

		<!-- /.control-sidebar -->
		<!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
		<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->

</body>
<script>
	function saveOrUpdateRegistration(actionType) {
		
		if(actionType == 'active' || actionType == 'activate' || actionType == 'inactive' || actionType == 'inactivate') 
		{
			if (confirm("Are you sure to activate the registration?") == true) {
				var formData = {
					"id" : $('#id').val(),
					"clubName" : $('#clubName').val(),
					"clubCode" : $('#clubCode').val(),
					"clubAddress" : $('#clubAddress').val(),
					"clubSubMainAddress" : $('#clubSubMainAddress').val(),
					"clubFName" : $('#clubFName').val(),
					"clubLName" : $('#clubLName').val(),
					"clubWebsite" : $('#clubWebsite').val(),
					"clubEmail" : $('#clubEmail').val(),
					"mobileNo" : $('#mobileNo').val(),
					"clubPwd" : $('#clubPwd').val(),
					"clubBeltType" : $('#clubBeltType').val(),
					"clubCertificateNo" : $('#clubCertificateNo').val(),
					"clubCertificateFile" : $('#clubCertificateFile').val(),
					"clubCertificateIdCardFile" : $('#clubCertificateIdCardFile').val(),
					"clubType" : $('#clubType').val(),
					"isActive" : $('#isActive').val(),
					"clubBeltNo" : $('#clubBeltNo').val(),
					"clubNoOfStudent" : $('#clubNoOfStudent').val(),
					"clubLevel" : $('#clubLevel').val(),
					"roleInClub" : $('#roleInClub').val()
				};
				$.ajax({
					type : 'POST',
					contentType : 'application/json; charset=UTF-8',
					url : '${pageContext.request.contextPath}/club/saveClubRegistration?operationType='+actionType,
					data : JSON.stringify(formData),
					//dataType: 'json',
					success : function(response) {
						if(response == 'mobileNoAlreadyFound') {
							alert('Entered mobile no already exist in our database.');
							return;
						}
						else if(response == 'clubNameAlreadyFound') {
							alert('Entered Club Name already exist in our database.');
							return;
						}
						else if(response == 'failure') {
							alert('Entered data is not correct.');
							return;
						}
						else {
							moveToVerifyAndSubmitRegistrationPage(response, actionType);
						}
					},
					error : function(response) {
						alert('Error while saving data into DB');
					}
				});	
		}
		}
		else {
		if(!checkValidationOnForm()) {
			return;
		}
		if (confirm("Are you sure to save the registration?") == true) {
			var formData = {
				"id" : $('#id').val(),
				"clubName" : $('#clubName').val(),
				"clubCode" : $('#clubCode').val(),
				"clubAddress" : $('#clubAddress').val(),
				"clubSubMainAddress" : $('#clubSubMainAddress').val(),
				"clubFName" : $('#clubFName').val(),
				"clubLName" : $('#clubLName').val(),
				"clubWebsite" : $('#clubWebsite').val(),
				"clubEmail" : $('#clubEmail').val(),
				"mobileNo" : $('#mobileNo').val(),
				"clubPwd" : $('#clubPwd').val(),
				"clubBeltType" : $('#clubBeltType').val(),
				"clubCertificateNo" : $('#clubCertificateNo').val(),
				"clubCertificateFile" : $('#clubCertificateFile').val(),
				"clubCertificateIdCardFile" : $('#clubCertificateIdCardFile').val(),
				"clubType" : $('#clubType').val(),
				"isActive" : $('#isActive').val(),
				"clubBeltNo" : $('#clubBeltNo').val(),
				"clubNoOfStudent" : $('#clubNoOfStudent').val(),
				"clubLevel" : $('#clubLevel').val(),
				"roleInClub" : $('#roleInClub').val()
			};
			$.ajax({
				type : 'POST',
				contentType : 'application/json; charset=UTF-8',
				url : '${pageContext.request.contextPath}/club/saveClubRegistration?operationType='+actionType,
				data : JSON.stringify(formData),
				//dataType: 'json',
				success : function(response) {
					if(response == 'mobileNoAlreadyFound') {
						alert('Entered mobile no already exist in our database.');
						return;
					}
					else if(response == 'clubNameAlreadyFound') {
						alert('Entered Club Name already exist in our database.');
						return;
					}
					else if(response == 'failure') {
						alert('Entered data is not correct.');
						return;
					}
					else {
						moveToVerifyAndSubmitRegistrationPage(response, actionType);
					}
				},
				error : function(response) {
					alert('Error while saving data into DB');
				}
			});
		  }
		}
	}
	function moveToVerifyAndSubmitRegistrationPage(registrationId, actionType) {

		window.location.href = "${pageContext.request.contextPath}/club/verifyRegistration?regId="+ registrationId+"&operationType="+actionType;

	}

	function checkValidationOnForm() {
		if($('#clubName').val() == "") {
			alert("Club Name can not be blank.");
			return false;
		}
		else if($('#clubAddress').val() == "") {
			alert("Club Address can not be blank.");
			return false;
		}
		else if($('#clubType').val() == "") {
			alert("Club Type can not be blank.");
			return false;
		}
		else if($('#clubFName').val() == "") {
			alert("Contact Person First Name can not be blank.");
			return false;
		}
		else if($('#clubLName').val() == "") {
			alert("Contact Person Last Name can not be blank.");
			return false;
		}
		else if($('#clubBeltType').val() == "None") {
			alert("Please select club belt type.");
			return false;
		}
		else if($('#clubCertificateNo').val() == "") {
			alert("Club Certificate No can not be blank.");
			return false;
		}
		else if($('#clubBeltNo').val() == "") {
			alert("Club Belt No can not be blank.");
			return false;
		}
		else if($('#clubNoOfStudent').val() == "") {
			alert("No of Student limit can not be blank.");
			return false;
		}
		else if($('#clubLevel').val() == "None") {
			alert("Please Select Club Level.");
			return false;
		}
		else if($('#mobileNo').val() == "" || $('#mobileNo').val() == "0") {
			alert("Cell No can not be blank or zero.");
			return false;
		}
		else if($('#Pwd').val() == "") {
			alert("Password can not be blank.");
			return false;
		}
		else if($('#clubPwd').val() == "") {
			alert("Confirm Password can not be blank.");
			return false;
		}
		if($('#Pwd').val() != $('#clubPwd').val()) {
			alert("Confirm Password does not match with password.");
			return false;
		}
		return true;
	}
	function loadEditData() {
		<%if (request.getAttribute("regData") != null) {%>
		$('#clubName').val('<%=clubRegistration.getClubName()%>');
		$('#id').val('<%=clubRegistration.getId()%>');
		$('#clubCodeShow').val('<%=clubRegistration.getClubCode()%>');
		$('#clubCode').val('<%=clubRegistration.getClubCode()%>');
		$('#clubAddress').val('<%=clubRegistration.getClubAddress()%>');
		$('#clubSubMainAddress').val('<%=clubRegistration.getClubSubMainAddress()%>');
		$('#clubFName').val('<%=clubRegistration.getClubFName()%>');
		$('#clubLName').val('<%=clubRegistration.getClubLName()%>');
		$('#clubWebsite').val('<%=clubRegistration.getClubWebsite()%>');
		$('#clubEmail').val('<%=clubRegistration.getClubEmail()%>');
		$('#mobileNo').val('<%=clubRegistration.getMobileNo()%>');
		$('#clubPwd').val('<%=clubRegistration.getClubPwd()%>');
		$('#Pwd').val('<%=clubRegistration.getClubPwd()%>');
		$('#clubBeltType').val('<%=clubRegistration.getClubBeltType()%>');
		$('#clubCertificateNo').val('<%=clubRegistration.getClubCertificateNo()%>');
		$('#clubCertificateFile').val('<%=clubRegistration.getClubCertificateFile()%>');
		$('#clubCertificateIdCardFile').val('<%=clubRegistration.getClubCertificateIdCardFile()%>');
		$('#isActive').val('<%=clubRegistration.getIsActive()%>');
		$('#clubType').val('<%=clubRegistration.getClubType()%>');
		$('#clubBeltNo').val('<%=clubRegistration.getClubBeltNo()%>');
		$('#clubNoOfStudent').val('<%=clubRegistration.getClubNoOfStudent()%>');
		$('#clubLevel').val('<%=clubRegistration.getClubLevel()%>');
		$('#roleInClub').val('<%=clubRegistration.getRoleInClub()%>');
	<%}%>
}
	
function backToPage(actionType) {
	if (actionType == 'updateEdit') {
		window.location.href = "${pageContext.request.contextPath}/club/openClubUpdatePage";
	}
	else if (actionType == 'create' || actionType == 'createEdit') {
		window.location.href = "${pageContext.request.contextPath}/";
	} else if (actionType == 'active' || actionType == 'activate' || actionType == 'inactive' || actionType == 'inactivate') {
		window.location.href = "${pageContext.request.contextPath}/club/openClubActiveInactivePage";
	}
  }
</script>
</html>
