<%@page import="com.leaguema.model.BMUser"%>
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="${pageContext.request.contextPath}/resources/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><%= user.getUserFName()+" "+user.getUserLName() %></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <!-- <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>-->
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li class="active treeview">
          <a href="/">
            <i class="fa fa-dashboard"></i> <span onclick="openDashboard()">Dashboard</span>
          </a>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-object-group"></i>
            <span>Club</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="${pageContext.request.contextPath}/club/clubRegistration"><i class="fa fa-sticky-note-o"></i> Register</a></li>
            <li><a href="${pageContext.request.contextPath}/club/openClubUpdatePage"><i class="fa fa-edit"></i> Update</a></li>
            <li><a href="${pageContext.request.contextPath}/club/openClubActiveInactivePage"><i class="fa fa-safari"></i> Activate/Deactivate</a></li>
          </ul>
        </li>
       
        <li class="treeview">
          <a href="#">
            <i class="fa fa-group"></i> <span>Player</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
           <li><a href="${pageContext.request.contextPath}/player/playerRegistration"><i class="fa fa-sticky-note-o"></i> Register</a></li>
            <li><a href="${pageContext.request.contextPath}/player/openPlayerUpdatePage"><i class="fa fa-edit"></i> Update</a></li>
            <li><a href="${pageContext.request.contextPath}/player/openPlayerActiveInactivePage"><i class="fa fa-safari"></i> Activate/Deactivate</a></li>
          </ul>
        </li>
        
        <li class="treeview">
          <a href="#">
            <i class="ion ion-pie-graph"></i> <span>Event</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
           <li><a href="pages/layout/top-nav.html"><i class="fa fa-sticky-note-o"></i> Create</a></li>
            <li><a href="pages/layout/boxed.html"><i class="fa fa-edit"></i> Update</a></li>
            <li><a href="pages/layout/fixed.html"><i class="fa fa-safari"></i> Activate/Deactivate</a></li>
          </ul>
        </li>
       
       
       
        <li>
          <a href="pages/calendar.html">
            <i class="fa fa-calendar"></i> <span>Calendar</span>
            <span class="pull-right-container">
              <small class="label pull-right bg-red">3</small>
              <small class="label pull-right bg-blue">17</small>
            </span>
          </a>
        </li>
        <li>
          <a href="pages/mailbox/mailbox.html">
            <i class="fa fa-envelope"></i> <span>Mailbox</span>
            <span class="pull-right-container">
              <small class="label pull-right bg-yellow">12</small>
              <small class="label pull-right bg-green">16</small>
              <small class="label pull-right bg-red">5</small>
            </span>
          </a>
        </li>
        </ul>
            </section>
    <!-- /.sidebar -->
  </aside>
  <script>
  function openDashboard() {

		window.location.href = "${pageContext.request.contextPath}/";

	}
	</script>