<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>${title}</title>

<%@include file="clubHeader.jsp"%>
</head>
<body class="hold-transition skin-blue sidebar-mini" onload="getRegistrationData()">
	<div class="wrapper">

		<%@include file="clubBodyHeader.jsp"%>
		<!--[START] Left side column. contains the logo and sidebar -->
		<%@include file="clubLeftSidebar.jsp"%>
		<!--[END] Left side column. contains the logo and sidebar -->

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>
					${pageHeading}
				</h1>
			</section>

			<!-- Main content -->
			<section class="content">
				<div class="row">
					<!-- left column -->

					<!--/.col (left) -->
					<!-- right column -->
					<div class="col-md-10">
						<!-- Horizontal Form -->
						<!-- /.box -->
						<!-- general form elements disabled -->
						<div class="box box-info">
							<div class="box-header with-border">
								<h3 class="box-title">${message }</h3>
							</div>
							<!-- /.box-header -->
							<div class="box-body">
								<form name="clubForm">
									<!-- text input -->
									<Table width="100%" >
										<Tr>
											<Td valign="top" width="100%" align="center">
												<div class="form-group">
													<label>${commonMessage}</label>
												</div>
											</Td>
											
										</Tr>
										<Tr>
											<Td valign="top" width="100%" align="center">
												<div class="form-group">
													
														<input type="button" class="btn btn-primary" value="Ok"
														onClick="backToPage('${operationType}')" />
												</div>
											</Td>
											
										</Tr>
										
									</Table>
									 
									<!-- input states -->
								</form>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
					</div>
					<!--/.col (right) -->
				</div>
				<!-- /.row -->
			</section>
			<!-- /.content -->
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->
		<%@include file="clubFooter.jsp"%>

		<!-- Control Sidebar -->
		
		<!-- /.control-sidebar -->
		<!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
		<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->

</body>
<script>
function backToPage(operationType) {
	if(operationType == 'ClubRegistration_createEdit' || operationType == 'ClubRegistration_create') {
		window.location.href = "${pageContext.request.contextPath}/club/clubRegistration";
	}
	else if(operationType == 'ClubRegistration_updateEdit'){
		window.location.href = "${pageContext.request.contextPath}/club/openClubUpdatePage";
	}
	else if(operationType == 'PlayerRegistration_createEdit' || operationType == 'PlayerRegistration_create') {
		window.location.href = "${pageContext.request.contextPath}/player/playerRegistration";
	}
	else if(operationType == 'PlayerRegistration_updateEdit'){
		window.location.href = "${pageContext.request.contextPath}/player/openPlayerUpdatePage";
	}
	else if(operationType == 'ClubRegistration_active' || operationType == 'ClubRegistration_activate' || operationType == 'ClubRegistration_inactive' || operationType == 'ClubRegistration_inactivate'){
		window.location.href = "${pageContext.request.contextPath}/club/openClubActiveInactivePage";
	}
	else if(operationType == 'PlayerRegistration_active' || operationType == 'PlayerRegistration_activate' || operationType == 'PlayerRegistration_inactive' || operationType == 'PlayerRegistration_inactivate'){
		window.location.href = "${pageContext.request.contextPath}/player/openPlayerActiveInactivePage";
	}
}
</script>
</html>
