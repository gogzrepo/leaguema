<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Verify Player Registration Page</title>

<%@include file="clubHeader.jsp"%>
</head>
<body class="hold-transition skin-blue sidebar-mini"
	onload="getPlayerRegistrationData()">
	<div class="wrapper">

		<%@include file="clubBodyHeader.jsp"%>
		<!--[START] Left side column. contains the logo and sidebar -->
		<%@include file="clubLeftSidebar.jsp"%>
		<!--[END] Left side column. contains the logo and sidebar -->

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>${pageHeading}</h1>
			</section>

			<!-- Main content -->
			<section class="content">
				<div class="row">
					<!-- left column -->

					<!--/.col (left) -->
					<!-- right column -->
					<div class="col-md-10">
						<!-- Horizontal Form -->
						<!-- /.box -->
						<!-- general form elements disabled -->
						<div class="box box-info">
							<div class="box-header with-border">
								<h3 class="box-title">${message }</h3>
							</div>
							<!-- /.box-header -->
							<div class="box-body">
								<form name="clubForm">
									<!-- text input -->
									<Table width="100%">
										<Tr>
											<Td valign="top" width="49%">
												<div class="form-group">
													<label>Club Name:</label> ${registrationData.clubName}
												</div>
											</Td>
											<Td width="2%">&nbsp;</Td>
											<Td width="49%">


												<div class="form-group">
													<label>Club Code:</label> ${registrationData.clubCode}
												</div>
											</Td>
										</Tr>

										<Tr>
											<Td valign="top" width="49%">
												<div class="form-group">
													<label>First Name:</label> ${registrationData.playerFName}
												</div>
											</Td>
											<Td width="2%">&nbsp;</Td>
											<Td width="49%">

												<div class="form-group">
													<label>Last Name:</label> ${registrationData.playerLName}
												</div>
											</Td>
										</Tr>

										<Tr>
											<Td>
												<div class="form-group">
													<label>Father First Name:</label>
													${registrationData.playerFthrFName}
												</div>
											</Td>
											<Td></Td>
											<Td>
												<div class="form-group">
													<label>Father Last Name:</label>
													${registrationData.playerFthrLName}
												</div>
											</Td>

										</Tr>

										<Tr>
											<Td valign="top">
												<div class="form-group">
													<label>Date of Birth:</label> ${registrationData.playerDOB}
												</div>
											</Td>
											<Td>&nbsp;</Td>
											<Td>
												<div class="form-group">
													<label>Player ID:</label> ${registrationData.playerID}
												</div>
											</Td>
										</Tr>

										<Tr>
											<Td valign="top">
												<div class="form-group">
													<label>Address:</label> ${registrationData.playerAddress}
												</div>
											</Td>
											<Td>&nbsp;</Td>
											<Td>
												<div class="form-group">
													<label>Permanent Address:</label>
													${registrationData.playerAddress2}
												</div>
											</Td>
										</Tr>

										<Tr>
											<Td valign="top">
												<div class="form-group">
													<label>Pincode:&nbsp;</label>
													${registrationData.playerAddressPincode}
												</div>
											</Td>
											<Td>&nbsp;</Td>
											<Td>
												<div class="form-group">
													<label>Pincode: </label>
													${registrationData.playerAddressPincode2}
												</div>
											</Td>
										</Tr>

										<Tr>
											<Td valign="top">
												<div class="form-group">
													<label>Email:</label> ${registrationData.playerEmail}
												</div>
											</Td>
											<Td>&nbsp;</Td>
											<Td>
												<div class="form-group">
													<label>Mobile:</label> ${registrationData.playerMobile}
												</div>
											</Td>
										</Tr>

										<Tr>
											<Td valign="top">
												<div class="form-group">
													<label>Education:&nbsp;</label>
													${registrationData.playerEducation}
												</div>
											</Td>
											<Td>&nbsp;</Td>
											<Td>
												<div class="form-group">
													<label>Others: &nbsp;</label>
													${registrationData.playerOthersEducation}
												</div>
											</Td>
										</Tr>

										<Tr>
											<Td valign="top">
												<div>
													<label>Player Level: &nbsp;</label>
													${registrationData.playerLevel}
												</div>
											</Td>
											<Td>&nbsp;</Td>
											<Td>
												<div class="form-group">
													<label>Player Photo</label> ${registrationData.playerPic}
												</div> <input type="hidden" id="id" value="${registrationData.id}" />
												<input type="hidden" id="clubCode"
												value="${registrationData.clubCode}" /> <input type="hidden"
												id="playerID" value="${registrationData.playerID}" />
											</Td>
										</Tr>
										<Tr>
											<Td valign="top">
												<div class="form-group">
													<label>Weight:</label> ${registrationData.playerWeight}
												</div>
											</Td>
											<Td>&nbsp;</Td>
											<Td>
												<div class="form-group">
													<label>Age:</label> ${registrationData.playerAge}
												</div>
											</Td>
										</Tr>

										<Tr>
											<Td valign="top">
												<div class="form-group">
													<label>Last Game Played:</label>
													${registrationData.playerLastGamePalyed}
												</div>
											</Td>
											<Td>&nbsp;</Td>
											<Td><label>Player Shift:</label> <c:forEach
													items="${allActiveShift}" var="shiftMst" varStatus="loop">

													<c:choose>
														<c:when
															test="${shiftMst.id==registrationData.playerShift}">
      														 ${shiftMst.shiftMstName }
    													</c:when>
													</c:choose>

												</c:forEach></Td>
											<Td>&nbsp;<input type="hidden" id="operationType"
												value="${operationType }" />
											</Td>

										</Tr>

										<Tr>
											<Td valign="top">
												<div class="form-group">
													<c:if
														test="${operationType == 'active' || operationType == 'activate' || operationType == 'inactive' || operationType == 'inactivate'}">
														<label>Enter CVN</label>
														<input type="text" id="verifyOTP" />
													</c:if>

												</div>
											</Td>
											<Td>&nbsp;</Td>
											<Td><c:if
													test="${operationType == 'active' || operationType == 'activate' || operationType == 'inactive' || operationType == 'inactivate'}">
													<input type="text" id="playerOTP"
														value="${registrationData.playerOTP}" />
												</c:if></Td>
										</Tr>

										<Tr>
											<Td colspan="3" align="center">
												<div class="box-footer">
													<input type="button" class="btn btn-primary" value="Verify"
														onClick="verifyPlayerRegistration()" /> &nbsp; <input
														type="button" class="btn btn-primary" value="Back"
														onClick="backToEditPlayerRegistration()" />
												</div>
											</Td>
										</Tr>

									</Table>

									<!-- input states -->
								</form>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
					</div>
					<!--/.col (right) -->
				</div>
				<!-- /.row -->
			</section>
			<!-- /.content -->
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->
		<%@include file="clubFooter.jsp"%>

		<!-- Control Sidebar -->

		<!-- /.control-sidebar -->
		<!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
		<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->

</body>
<script>
function verifyPlayerRegistration(){
    var formData = { "id" : $('#id').val()};
    var regitrationId = $('#id').val();
    var registrationCode = $('#playerID').val();
    var operationType = $('#operationType').val();
    var operationType = $('#operationType').val();
	if(operationType == 'active' || operationType == 'activate' || operationType == 'inactive' || operationType == 'inactivate') {
		if($('#playerOTP').val() == $('#verifyOTP').val()){
			if (confirm("Are you sure to verify the player data?") == true) {
			$.ajax({
				type : 'POST',
				contentType: 'application/json; charset=UTF-8',
				url : '${pageContext.request.contextPath}/player/saveVerifyPlayerRegistrationData?regId='+$('#id').val()+"&operationType=${operationType}",
				//dataType: 'json',
				success : function(response) {
					if(response == "success") {
						openCommonMessagePage(regitrationId,registrationCode,'PlayerRegistration_'+operationType);
					}
					else {
						alert("Error occurred:"+response);
					}
				},
				error : function(response) {
					alert('Error while saving data into DB.');
				}
			});
			}
		}
			else {
				alert('Please enter correct CVN.');	
			}
		}
	else {
		if (confirm("Are you sure to verify the player data?") == true) {
			$.ajax({
				type : 'POST',
				contentType: 'application/json; charset=UTF-8',
				url : '${pageContext.request.contextPath}/player/saveVerifyPlayerRegistrationData?regId='+$('#id').val()+"&operationType=${operationType}",
				//dataType: 'json',
				success : function(response) {
					if(response == "success") {
						openCommonMessagePage(regitrationId,registrationCode,'PlayerRegistration_'+operationType);
					}
					else {
						alert("Error occurred:"+response);
					}
				},
				error : function(response) {
					alert('Error while saving data into DB.');
				}
			});
			}
	}
}
function backToEditPlayerRegistration(){
	var regitrationId = $('#id').val();
	var operationType = $('#operationType').val();
	window.location.href = "${pageContext.request.contextPath}/player/editPlayerRegistration?regId="+regitrationId+"&operationType="+operationType;
}
function openCommonMessagePage(paramValue,registrationCode,operationValue) {
	paramValue = paramValue+"~"+registrationCode;
	window.location.href = "${pageContext.request.contextPath}/cmnurl/openMessagePage?paramValue="+paramValue+"&operationType="+operationValue;
}
</script>
</html>
