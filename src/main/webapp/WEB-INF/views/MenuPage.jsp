<%-- 
    Document   : MenuPage
    Created on : Oct 11, 2015, 6:15:02 PM
    Author     : sunil
--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"> 
<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>


<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
         <SCRIPT src="/resources/commonJS/ua.js"></SCRIPT>
         <SCRIPT src="/resources/commonJS/treeMenu.js"></SCRIPT>
        <SCRIPT>
        function op() {
        // This function is for folders that do not open pages themselves.
        // See the online instructions for more information.
         }
          function forwardMenu(obj){
     //alert(obj);
     document.location.href="/MenuMappingServlet?menuNo="+obj;
 }
  </SCRIPT>
  
  <STYLE>
   body { 
    //background-color: #b0c4de;
    margin: 0px;
   }
    A {
      text-decoration: none;
      color: black;}
    .specialClass {
      font-family:garamond; 
      font-size:12pt;
      color:green;
      font-weight:bold;
      text-decoration:underline;
    }
    .TreeTable {
       border: 2px groove black;
       // border: 5px;
    }
    hr {
height: 2px;
color: #123455;
background-color: darkgray;
border: none;
}

  </STYLE>
  
  <script>
    <%
            String [][] treeMenuData = null;
            if(session.getAttribute("UserMenuData")!=null){
                treeMenuData = (String[][]) session.getAttribute("UserMenuData");
               // session.removeAttribute("");
            }
        %>
    USETEXTLINKS = 1
// Configures whether the tree is fully open upon loading of the page, or whether
// only the root node is visible.
STARTALLOPEN = 0
// Specify if the images are in a subdirectory;
ICONPATH = '/resources/commonImages/';
foldersTree = gFld("<i>Main Menu</i>", "")
  foldersTree.treeID = "Frameset"
  //aux1 = insFld(foldersTree, gFld("<%= treeMenuData[1][0] %>Photos example", ""))
  
  <% int len = treeMenuData.length;
  String au = "aux1";
  System.out.println("Length Of Menu Data Is---->>"+treeMenuData.length);
  for(int i=0;i<len;i++) {
      
      if(treeMenuData[i][1].equals("0")) {  // Means Folder
          if(treeMenuData[i][1].equals("0") && treeMenuData[i][2].equals("0")){  // Means Parent Folder
              au = "aux1";
              %>
          aux1 = insFld(foldersTree, gFld("<%= treeMenuData[i][0] %>", ""))
      <%
          }else { // Means Child Folder
              au = "aux2";
              %>
               aux2 = insFld(aux1, gFld("<%= treeMenuData[i][0] %>", "http://www.treeview.net/treemenu/demopics/beenthere_america.gif"))
     <%
          }
       } else if(!treeMenuData[i][1].equals("0")) {  // Means it is Menu
              if(!treeMenuData[i][1].equals("0") && treeMenuData[i][2].equals("0")) {// Means Main Menu
          %>
              insDoc(foldersTree, gLnk("S", "<%= treeMenuData[i][0] %>", "<%= treeMenuData[i][1] %>"))
    <%
      }else {  if(au.equals("aux1")) { %>
                insDoc(aux1, gLnk("S", "<%= treeMenuData[i][0] %>", "<%= treeMenuData[i][1] %>"))
                <%      } else if(au.equals("aux2")) { %>
                insDoc(aux2, gLnk("S", "<%= treeMenuData[i][0] %>", "<%= treeMenuData[i][1] %>"))
                <%  
                }
            }
        }
  }
  %>
 </script>
    </head>
    <body bgcolor="#e7f3f3">
    <form name="mnudata" action="/OpenMenuPage" method="post">
        
            <table style="background: #123455;width: 100%;height: 10%;" border="0">
            <tr><td style="width: 20%;color: whitesmoke;">Space For Company's Logo..</td>
                <td align="center" style="width: 80%;color: whitesmoke;"><h2></h2></td></tr>
        </table>
        
        <div style="position: absolute;width: 100%;height: 90%;padding:0;overflow: auto;">
        
        <table border="1" style="border-spacing:0;" width="100%" height="100%" ><tr>
            <td style="width: 30%;vertical-align: top;padding-left: 30;"><br>
             <A href="http://www.treemenu.net/" target=_blank > 
             </A>
                <SCRIPT>initializeDocument()</SCRIPT>
            </td> 
            <td style="width: 700px;"></td>
            </table>          
        </div>
 </form>
 
  <NOSCRIPT>
   A tree for site navigation will open here if you enable JavaScript in your browser.
  </NOSCRIPT>
    </body>
</html>