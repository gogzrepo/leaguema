<%-- 
    Document   : LeftFrame
    Created on : Oct 13, 2015, 12:51:12 PM
    Author     : sunil
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page contentType="text/html"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
  <!-- This is the <STYLE> block for the default styles.  If   -->
  <!-- you want the black background, remove this <STYLE>      -->
  <!-- block.   
  -->
  <script>
      function forwardMenu(obj){
     alert(obj);
     document.location.href="/BMSQuery-war/MenuMappingServlet?menuNo="+obj;
 }
</script>
  
  <STYLE>
    BODY {
      background-color: white;}
    TD {
      font-size: 10pt; 
      font-family: verdana,helvetica; 
      text-decoration: none;
      white-space:nowrap;}
    A {
      text-decoration: none;
      color: black;}
    .specialClass {
      font-family:garamond; 
      font-size:12pt;
      color:green;
      font-weight:bold;
      text-decoration:underline}
  </STYLE>
  <!-- If you want the black background, replace the contents  -->
  <!-- of the <STYLE> tag above with the following...
    BODY {
      background-color: black;}
    TD {
      font-size: 10pt; 
      font-family: verdana,helvetica; 
      text-decoration: none;
      white-space:nowrap;}
    A {
      text-decoration: none;  449, 190, 480,, 
      color: white;}
  <!-- This is the end of the <STYLE> contents.                -->
 
  <!-- Code for browser detection. DO NOT REMOVE.              -->
  <SCRIPT src="/BMSQuery-war/view/common/commonJS/ua.js"></SCRIPT>

  <!-- Infrastructure code for the TreeView. DO NOT REMOVE.    -->
  <SCRIPT src="/BMSQuery-war/view/common/commonJS/treeMenu.js"></SCRIPT>

  <!-- Scripts that define the tree. DO NOT REMOVE.            -->
  <!--<SCRIPT src="/BMSQuery-war/view/common/commonJS/demoFramesetNodes.js"></SCRIPT>-->
<script>
    <%
            String [][] treeMenuData = null;
            if(session.getAttribute("UserMenuData")!=null){
                treeMenuData = (String[][]) session.getAttribute("UserMenuData");
               // session.removeAttribute("");
            }
        %>
    USETEXTLINKS = 1
// Configures whether the tree is fully open upon loading of the page, or whether
// only the root node is visible.
STARTALLOPEN = 0
// Specify if the images are in a subdirectory;
ICONPATH = '/BMSQuery-war/view/common/commonImages/';
foldersTree = gFld("<i>Main Menu</i>", "")
  foldersTree.treeID = "Frameset"
  //aux1 = insFld(foldersTree, gFld("<%= treeMenuData[1][0] %>Photos example", ""))
  
  <% int len = treeMenuData.length;
  String au = "aux1";
  System.out.println("Length Of Menu Data Is---->>"+treeMenuData.length);
  for(int i=0;i<len;i++) {
      
      if(treeMenuData[i][1].equals("0")) {  // Means Folder
          if(treeMenuData[i][1].equals("0") && treeMenuData[i][2].equals("0")){  // Means Parent Folder
              au = "aux1";
              %>
          aux1 = insFld(foldersTree, gFld("<%= treeMenuData[i][0] %>", ""))
      <%
          }else { // Means Child Folder
              au = "aux2";
              %>
               aux2 = insFld(aux1, gFld("<%= treeMenuData[i][0] %>", "http://www.treeview.net/treemenu/demopics/beenthere_america.gif"))
     <%
          }
       } else if(!treeMenuData[i][1].equals("0")) {  // Means it is Menu
              if(!treeMenuData[i][1].equals("0") && treeMenuData[i][2].equals("0")) {// Means Main Menu
          %>
              insDoc(foldersTree, gLnk("S", "<%= treeMenuData[i][0] %>", "<%= treeMenuData[i][1] %>"))
    <%
      }else {  if(au.equals("aux1")) { %>
                insDoc(aux1, gLnk("S", "<%= treeMenuData[i][0] %>", "<%= treeMenuData[i][1] %>"))
                <%      } else if(au.equals("aux2")) { %>
                insDoc(aux2, gLnk("S", "<%= treeMenuData[i][0] %>", "<%= treeMenuData[i][1] %>"))
                <%  
                }
            }
        }
  }
  %>

    </script>
    </head>
    <body>
        <form name="mnudata" action="MenuMappingServlet" method="post">
        
  <!------------------------------------------------------------->
  <!-- IMPORTANT NOTICE:                                       -->
  <!-- Removing the following link will prevent this script    -->
  <!-- from working.  Unless you purchase the registered       -->
  <!-- version of TreeView, you must include this link.        -->
  <!-- If you make any unauthorized changes to the following   -->
  <!-- code, you will violate the user agreement.  If you want -->
  <!-- to remove the link, see the online FAQ for instructions -->
  <!-- on how to obtain a version without the link.            -->
  <!------------------------------------------------------------->
  <DIV style="position:absolute; top:0; left:0;">
   <TABLE border=0><TR><TD><FONT size=-2>
    <A style="font-size:7pt;text-decoration:none;color:silver" href="http://www.treemenu.net/" target=_blank>Javascript Tree Menu
    </A>
    </FONT>
    </TD>
    </TR>
   </TABLE>
  </DIV>
    </form>
  <!-- Build the browser's objects and display default view  -->
  <!-- of the tree.                                          -->
  <SCRIPT>initializeDocument()</SCRIPT>
  <NOSCRIPT>
   A tree for site navigation will open here if you enable JavaScript in your browser.
  </NOSCRIPT>
  </body>
</html>
