<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Registration Page</title>

<%@include file="clubHeader.jsp"%>
<%@page import="com.leaguema.model.PlayerRegistration"%>
</head>
<body class="hold-transition skin-blue sidebar-mini" onload="loadEditData()">
<% 
PlayerRegistration playerRegistration = null;
if(request.getAttribute("regData") != null)
	playerRegistration = (PlayerRegistration)request.getAttribute("regData");
%>
	<div class="wrapper">

		<%@include file="clubBodyHeader.jsp"%>
		<!--[START] Left side column. contains the logo and sidebar -->
		<%@include file="clubLeftSidebar.jsp"%>
		<!--[END] Left side column. contains the logo and sidebar -->

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>
					${pageHeading}
				</h1>
			</section>

			<!-- Main content -->
			<section class="content">
				<div class="row">
					<!-- left column -->

					<!--/.col (left) -->
					<!-- right column -->
					<div class="col-md-10">
						<!-- Horizontal Form -->
						<!-- /.box -->
						<!-- general form elements disabled -->
						<div class="box box-info">
							<div class="box-header with-border">
								<h3 class="box-title">${message }</h3>
							</div>
							<!-- /.box-header -->
							<div class="box-body">
								<form name="clubForm">
									<!-- text input -->
									<Table width="100%">
										<Tr>
											<Td valign="top" width="49%">
												<div class="form-group">
													<label>Club Name</label> <input type="text" id="clubName"
														class="form-control" value="<%= clubName %>" readonly>
												</div>
											</Td>
											<Td width="2%">&nbsp;</Td>
											<Td width="49%">


												<div class="form-group">
													<label>Club Code</label> <input type="text" id="clubCode"
														class="form-control" value="<%= clubCode %>" disabled>
												</div>
											</Td>
										</Tr>
										
										<Tr>
											<Td valign="top">
												<div class="form-group">
													<label>First Name</label> <input type="text"
														id="playerFName" class="form-control"
														placeholder="Enter Player First Name">
												</div>
											</Td>
											<Td>&nbsp;</Td>
											<Td>
												<div class="form-group">
													<label>Last Name</label> <input type="text"
														id="playerLName" class="form-control"
														placeholder="Enter Player Last Name">
												</div>
											</Td>
										</Tr>
										
											<Tr>
											<Td valign="top">
												<div class="form-group">
													<label>Father First Name</label> <input type="text"
														id="playerFthrFName" class="form-control"
														placeholder="Enter Player Father First Name">
												</div>
											</Td>
											<Td>&nbsp;</Td>
											<Td>
												<div class="form-group">
													<label>Father Last Name</label> <input type="text"
														id="playerFthrLName" class="form-control"
														placeholder="Enter Player Father Last Name">
												</div>
											</Td>
										</Tr>
	
										<Tr>
											<Td valign="top">
												<div class="form-group">
													<label>Date of Birth</label> <input type="text"
														id="playerDOB" class="form-control"
														placeholder="Enter Payer DOB">
												</div>
											</Td>
											<Td>&nbsp;</Td>
											<Td>
												<div class="form-group">
													<label>Player ID</label> <input type="text"
														id="playerID" class="form-control"
														disabled>
												</div>
											</Td>
										</Tr>
										
										<Tr>
											<Td>
												<div class="form-group">
													<label>Address</label>
													<textarea id="playerAddress" class="form-control" rows="3"
														placeholder="Enter Player Address"></textarea>
												</div>
											</Td>
											<Td></Td>
											<Td>
												<label>Permanent Address</label>
													<textarea id="playerAddress2" class="form-control" rows="3"
														placeholder="Enter Player Address"></textarea>
											</Td>

										</Tr>
										
										<Tr>
											<Td>
												<div class="form-group">
													<label>Pincode</label>
													<textarea id="playerAddressPincode" class="form-control" rows="3"
														placeholder="Enter Address Pincode"></textarea>
												</div>
											</Td>
											<Td></Td>
											<Td>
												<label>Pincode</label>
													<textarea id="playerAddressPincode2" class="form-control" rows="3"
														placeholder="Enter Address Pincode"></textarea>
											</Td>

										</Tr>

										<Tr>
											<Td valign="top">
												<div class="form-group">
													<label>Email</label> <input type="text"
														id="playerEmail" class="form-control"
														placeholder="Enter Player Email">
												</div>
											</Td>
											<Td>&nbsp;</Td>
											<Td>
												<div class="form-group">
													<label>Mobile</label> <input type="text"
														id="playerMobile" class="form-control"
														placeholder="Enter Player Mobile">
												</div>
											</Td>
										</Tr>

										<Tr>
											<Td valign="top">
												<div class="form-group">
													<label>Education</label> <select class="form-control"
														id="playerEducation">
														<option value="Bachelor">Bachelor</option>
														<option value="High School">High School</option>
														<option value="Intermediate">Intermediate</option>
														<option value="Others">Others</option>
													</select>
												</div>
											</Td>
											<Td>&nbsp;</Td>
											<Td>
												<div class="form-group">
													<label>Others</label> <input type="text"
														class="form-control" id="playerOthersEducation"
														placeholder="Enter Education" disabled>
												</div>
											</Td>
										</Tr>

										<Tr>
											<Td valign="top">
												<div class="form-group">
													<label>Player Level</label> <select class="form-control"
														id="playerLevel">
														<option value="International">International</option>
														<option value="National">National</option>
														<option value="State">State</option>
														<option value="District">District</option>
														<option value="Intermediate">Intermediate</option>
														<option value="Beginer">Beginer</option>
													</select>
												</div>
											</Td>
											<Td>&nbsp;</Td>
											<Td>
												<div class="form-group">
													<label>Upload Player Image(Passport Size)</label> <input type="file"
														class="form-control" id="playerPic"/>
												</div>
											</Td>
										</Tr>

										<Tr>
											<Td valign="top">
												<div class="form-group">
													<label>Weight</label> <input type="text"
														class="form-control" id="playerWeight"
														placeholder="Enter Player Weight">
												</div>
											</Td>
											<Td>&nbsp;</Td>
											<Td>
												<div class="form-group">
													<label>Age</label> <input type="text"
														class="form-control" id="playerAge"
														placeholder="Enter Your Age">
												</div>
											</Td>
										</Tr>

										<Tr>
											<Td valign="top">
												<div class="form-group">
													<label>Last Game Played</label> <input type="text"
														class="form-control" id="playerLastGamePalyed"
														placeholder="Enter Last Game Played">
												</div>
											</Td>
											<Td>&nbsp;</Td>
											<Td>
												<div class="form-group">
													<label>Align Shift</label> 
													<select class="form-control"
														id="playerShift">
														<option value="0">Select</option>
														<c:forEach items="${allActiveShift}" var="shiftMst" varStatus="loop">
														<option value="${shiftMst.id }">${shiftMst.shiftMstName }</option>
														</c:forEach>
														</select>
												</div>
											</Td>
										</Tr>
										

										<Tr>
											<Td colspan="3" align="center">
												<div class="box-footer">
													
														<input type="button" class="btn btn-primary" value="Save"
														onClick="saveOrUpdateRegistration('${operationType}')" />
														<input type="button" class="btn btn-primary" value="Cancel"
														onClick="backToPage('${operationType}')" />
												</div>
												<input type="hidden" id="id"/>
												<input type="hidden" id="playerID"/>
												<input type="hidden" id="clubCode" value="<%= clubCode %>"/>
											</Td>
										</Tr>

									</Table>

									<!-- input states -->
								</form>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
					</div>
					<!--/.col (right) -->
				</div>
				<!-- /.row -->
			</section>
			<!-- /.content -->
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->
		<%@include file="clubFooter.jsp"%>

		<!-- Control Sidebar -->

		<!-- /.control-sidebar -->
		<!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
		<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->

</body>
<script>
	function saveOrUpdateRegistration(actionType) {
		if(actionType == 'active' || actionType == 'activate' || actionType == 'inactive' || actionType == 'inactivate') 
		{
			if (confirm("Are you sure to activate the registration?") == true) {
				var formData = {
						"id" : $('#id').val(),
						"playerID" : $('#playerID').val(),
						"clubName" : $('#clubName').val(),
						"clubCode" : $('#clubCode').val(),
						"playerFName" : $('#playerFName').val(),
						"playerLName" : $('#playerLName').val(),
						"playerFthrFName" : $('#playerFthrFName').val(),
						"playerFthrLName" : $('#playerFthrLName').val(),
						"playerDOB" : $('#playerDOB').val(),
						"playerID" : $('#playerID').val(),
						"playerAddress" : $('#playerAddress').val(),
						"playerAddress2" : $('#playerAddress2').val(),
						"playerAddressPincode" : $('#playerAddressPincode').val(),
						"playerAddressPincode2" : $('#playerAddressPincode2').val(),
						"playerEmail" : $('#playerEmail').val(),
						"playerMobile" : $('#playerMobile').val(),
						"playerEducation" : $('#playerEducation').val(),
						"playerOthersEducation" : $('#playerOthersEducation').val(),
						"playerLevel" : $('#playerLevel').val(),
						"playerPic" : $('#playerPic').val(),
						"playerWeight" : $('#playerWeight').val(),
						"playerAge" : $('#playerAge').val(),
						"playerLastGamePalyed" : $('#playerLastGamePalyed').val(),
						"playerShift" : $('#playerShift').val()
					};
					$.ajax({
						type : 'POST',
						contentType : 'application/json; charset=UTF-8',
						url : '${pageContext.request.contextPath}/player/savePlayerRegistration?operationType='+actionType,
						data : JSON.stringify(formData),
						//dataType: 'json',
						success : function(response) {
							if(response == 'mobileNoAlreadyFound') {
								alert('Entered mobile no already exist in our database.');
								return;
							}
							else if(response == 'emailAlreadyFound') {
								alert('Entered email already exist in our database.');
								return;
							}
							else if(response == 'failure') {
								alert('Entered data is not correct.');
								return;
							}
							else {
								moveToVerifyPlayerRegistrationPage(response, actionType);
							}
						},
						error : function(response) {
							alert('Error while saving data into DB');
						}
					});
			}
		}
		else {
		if (confirm("Are you sure to save the player data?") == true) {
			var formData = {
				"id" : $('#id').val(),
				"playerID" : $('#playerID').val(),
				"clubName" : $('#clubName').val(),
				"clubCode" : $('#clubCode').val(),
				"playerFName" : $('#playerFName').val(),
				"playerLName" : $('#playerLName').val(),
				"playerFthrFName" : $('#playerFthrFName').val(),
				"playerFthrLName" : $('#playerFthrLName').val(),
				"playerDOB" : $('#playerDOB').val(),
				"playerID" : $('#playerID').val(),
				"playerAddress" : $('#playerAddress').val(),
				"playerAddress2" : $('#playerAddress2').val(),
				"playerAddressPincode" : $('#playerAddressPincode').val(),
				"playerAddressPincode2" : $('#playerAddressPincode2').val(),
				"playerEmail" : $('#playerEmail').val(),
				"playerMobile" : $('#playerMobile').val(),
				"playerEducation" : $('#playerEducation').val(),
				"playerOthersEducation" : $('#playerOthersEducation').val(),
				"playerLevel" : $('#playerLevel').val(),
				"playerPic" : $('#playerPic').val(),
				"playerWeight" : $('#playerWeight').val(),
				"playerAge" : $('#playerAge').val(),
				"playerLastGamePalyed" : $('#playerLastGamePalyed').val(),
				"playerShift" : $('#playerShift').val()
			};
			$.ajax({
				type : 'POST',
				contentType : 'application/json; charset=UTF-8',
				url : '${pageContext.request.contextPath}/player/savePlayerRegistration?operationType='+actionType,
				data : JSON.stringify(formData),
				//dataType: 'json',
				success : function(response) {
					if(response == 'mobileNoAlreadyFound') {
						alert('Entered mobile no already exist in our database.');
						return;
					}
					else if(response == 'emailAlreadyFound') {
						alert('Entered email already exist in our database.');
						return;
					}
					else if(response == 'failure') {
						alert('Entered data is not correct.');
						return;
					}
					else {
						moveToVerifyPlayerRegistrationPage(response, actionType);
					}
				},
				error : function(response) {
					alert('Error while saving data into DB');
				}
			});
		  }
		}
	}
	function moveToVerifyPlayerRegistrationPage(registrationId, actionType) {

		window.location.href = "${pageContext.request.contextPath}/player/verifyPlayerRegistration?regId="+ registrationId+"&operationType="+actionType;

	}

	function checkValidationOnForm() {
		if($('#clubName').val() == "") {
			alert("Club Name can not be blank.");
			return false;
		}
		else if($('#clubAddress').val() == "") {
			alert("Club Address can not be blank.");
			return false;
		}
		else if($('#clubType').val() == "") {
			alert("Club Type can not be blank.");
			return false;
		}
		else if($('#clubFName').val() == "") {
			alert("Contact Person First Name can not be blank.");
			return false;
		}
		else if($('#clubLName').val() == "") {
			alert("Contact Person Last Name can not be blank.");
			return false;
		}
		else if($('#clubBeltType').val() == "None") {
			alert("Please select club belt type.");
			return false;
		}
		else if($('#clubCertificateNo').val() == "") {
			alert("Club Certificate No can not be blank.");
			return false;
		}
		else if($('#clubBeltNo').val() == "") {
			alert("Club Belt No can not be blank.");
			return false;
		}
		else if($('#clubNoOfStudent').val() == "") {
			alert("No of Student limit can not be blank.");
			return false;
		}
		else if($('#clubLevel').val() == "None") {
			alert("Please Select Club Level.");
			return false;
		}
		else if($('#mobileNo').val() == "" || $('#mobileNo').val() == "0") {
			alert("Cell No can not be blank or zero.");
			return false;
		}
		else if($('#Pwd').val() == "") {
			alert("Password can not be blank.");
			return false;
		}
		else if($('#clubPwd').val() == "") {
			alert("Confirm Password can not be blank.");
			return false;
		}
		if($('#Pwd').val() != $('#clubPwd').val()) {
			alert("Confirm Password does not match with password.");
			return false;
		}
		return true;
	}
	function loadEditData() {
		<%if(request.getAttribute("regData") != null) {%>
		
		$('#clubName').val('<%=playerRegistration.getClubName()%>');
		$('#id').val('<%=playerRegistration.getId()%>');
		$('#playerID').val('<%=playerRegistration.getPlayerID()%>');
		$('#clubCode').val('<%=playerRegistration.getClubCode()%>');
		$('#playerFName').val('<%=playerRegistration.getPlayerFName()%>');
		$('#playerLName').val('<%=playerRegistration.getPlayerLName()%>');
		$('#playerFthrFName').val('<%=playerRegistration.getPlayerFthrFName()%>');
		$('#playerFthrLName').val('<%=playerRegistration.getPlayerFthrLName()%>');
		$('#playerDOB').val('<%=playerRegistration.getPlayerDOB()%>');
		$('#playerAddress').val('<%=playerRegistration.getPlayerAddress()%>');
		$('#playerAddress2').val('<%=playerRegistration.getPlayerAddress2()%>');
		$('#playerAddressPincode').val('<%=playerRegistration.getPlayerAddressPincode()%>');
		$('#playerAddressPincode2').val('<%=playerRegistration.getPlayerAddressPincode2()%>');
		$('#playerEmail').val('<%=playerRegistration.getPlayerEmail()%>');
		$('#playerMobile').val('<%=playerRegistration.getPlayerMobile()%>');
		$('#playerEducation').val('<%=playerRegistration.getPlayerEducation()%>');
		$('#playerOthersEducation').val('<%=playerRegistration.getPlayerOthersEducation()%>');
		$('#playerLevel').val('<%=playerRegistration.getPlayerLevel()%>');
		$('#playerWeight').val('<%=playerRegistration.getPlayerWeight()%>');
		$('#playerAge').val('<%=playerRegistration.getPlayerAge()%>');
		$('#playerLastGamePalyed').val('<%=playerRegistration.getPlayerLastGamePalyed()%>');
		$('#playerShift').val('<%=playerRegistration.getPlayerShift()%>');
		
		<%}%>
	}
	
	function backToPage(actionType) {
		if(actionType == 'updateEdit')
			window.location.href = "${pageContext.request.contextPath}/player/openPlayerUpdatePage";
		else if(actionType == 'create' || actionType == 'createEdit') {
			window.location.href = "${pageContext.request.contextPath}/";
		}
		else if (actionType == 'active' || actionType == 'activate' || actionType == 'inactive' || actionType == 'inactivate') {
			window.location.href = "${pageContext.request.contextPath}/player/openPlayerActiveInactivePage";
		}
	}
</script>
</html>
