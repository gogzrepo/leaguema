<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Verify Registration Page</title>

<%@include file="clubHeader.jsp"%>
</head>
<body class="hold-transition skin-blue sidebar-mini" onload="getRegistrationData()">
	<div class="wrapper">

		<%@include file="clubBodyHeader.jsp"%>
		<!--[START] Left side column. contains the logo and sidebar -->
		<%@include file="clubLeftSidebar.jsp"%>
		<!--[END] Left side column. contains the logo and sidebar -->

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>
					${pageHeading}
				</h1>
			</section>

			<!-- Main content -->
			<section class="content">
				<div class="row">
					<!-- left column -->

					<!--/.col (left) -->
					<!-- right column -->
					<div class="col-md-10">
						<!-- Horizontal Form -->
						<!-- /.box -->
						<!-- general form elements disabled -->
						<div class="box box-info">
							<div class="box-header with-border">
								<h3 class="box-title">${message }</h3>
							</div>
							<!-- /.box-header -->
							<div class="box-body">
								<form name="clubForm">
									<!-- text input -->
									<Table width="100%">
										<Tr>
											<Td valign="top" width="49%">
												<div class="form-group">
													<label>Club Name:</label> ${registrationData.clubName}
												</div>
											</Td>
											<Td width="2%">&nbsp;</Td>
											<Td width="49%">


												<div class="form-group">
													<label>Club Code:</label> ${registrationData.clubCode}
												</div>
											</Td>
										</Tr>

										<Tr>
										<Td>
										<div class="form-group">
													<label>Club Address:</label> 
													${registrationData.clubAddress}
												</div>
										</Td>
										<Td></Td>
										<Td>
										<div class="form-group">
													<label>Type of Club:</label> 
													${registrationData.clubType}
												</div>
										</Td>
										
										</Tr>
										
										<Tr>
											<Td valign="top">
												<div class="form-group">
													<label>Contact Person First Name:</label> 
													${registrationData.clubFName}
												</div>
											</Td>
											<Td>&nbsp;</Td>
											<Td>
												<div class="form-group">
												<label>Contact Person Last Name:</label> 
													${registrationData.clubLName}
													</div>
											</Td>
										</Tr>
										
										<Tr>
											<Td valign="top">
												<div class="form-group">
													<label>Website:</label> 
													${registrationData.clubWebsite}
												</div>
											</Td>
											<Td>&nbsp;</Td>
											<Td>
												<div class="form-group">
												<label>Email:</label> 
													${registrationData.clubEmail}
												</div>
											</Td>
										</Tr>
										
										<Tr>
											<Td valign="top">
												<div class="form-group">
													<label>Belt Type:&nbsp;</label> 
								                  ${registrationData.clubBeltType}
								                </div>	
											</Td>
											<Td>&nbsp;</Td>
											<Td>
												<div class="form-group">
												<label>Certificate No: </label> 
													${registrationData.clubCertificateNo}
												</div>
											</Td>
										</Tr>
										
										<Tr>
											<Td valign="top">
												<div class="form-group">
													<label>Upload Certificate File:</label> 
								                  	${registrationData.clubCertificateFile}
								                </div>	
											</Td>
											<Td>&nbsp;</Td>
											<Td>
												<div class="form-group">
												<label>Upload Id Card:</label> 
													${registrationData.clubCertificateIdCardFile}
												</div>
											</Td>
										</Tr>
										
										<Tr>
											<Td valign="top">
												<div class="form-group">
													<label>Club Status:&nbsp;</label> 
								                  <c:if test="${registrationData.isActive == 'true'}">
								                  	Active
								                  </c:if>
								                  <c:if test="${registrationData.isActive == 'false'}">
								                  	Inactive
								                  </c:if>
								                </div>	
											</Td>
											<Td>&nbsp;</Td>
											<Td>
												<div class="form-group">
												<label>Belt No:&nbsp;</label> 
													${registrationData.clubBeltNo}
												</div>
											</Td>
										</Tr>
										
										<Tr>
											<Td valign="top">
												<div class="form-group">
													<label>No of Student: &nbsp;</label> 
													${registrationData.clubNoOfStudent}
												</div>
											</Td>
											<Td>&nbsp;</Td>
											<Td>
												<div class="form-group">
												<label>Level Of Club: &nbsp;</label> 
								                  ${registrationData.clubLevel}
												</div>
												<input type="hidden" id="id" value="${registrationData.id}"/>
												<input type="hidden" id="clubCode" value="${registrationData.clubCode}"/>
												<input type="hidden" id="operationType" value="${operationType }"/>
											</Td>
										</Tr>
										<Tr><Td colspan="3"><Hr></Td></Tr>
										<Tr>
											<Td valign="top">
												<div class="form-group">
													<label>Cell No</label> ${registrationData.mobileNo}
												</div>
											</Td>
											<Td>&nbsp;</Td>
											<Td>
												<div class="form-group">
													<label>Role In Club</label> ${registrationData.roleInClub}
												</div>
											</Td>
										</Tr>
										<Tr>
											<Td valign="top">
												<div class="form-group">
													<label>Password</label> ******
												</div>
											</Td>
											<Td>&nbsp;</Td>
											<Td>
												<div class="form-group">
													<label>Confirm Password</label> ******
												</div>
											</Td>
										</Tr>
										<Tr>
											<Td  valign="top">
												<div class="form-group">
												<c:if test="${operationType == 'active' || operationType == 'activate' || operationType == 'inactive' || operationType == 'inactivate'}">
													<label>Enter OTP</label> 
													<input type="text" id="verifyOTP" />
													</c:if>
													
												</div>
											</Td>
											<Td>&nbsp;</Td>
											<Td>
												 <c:if test="${operationType == 'active' || operationType == 'activate' || operationType == 'inactive' || operationType == 'inactivate'}">
                 									<input type = "text" id="clubOTP" value="${registrationData.clubOTP}"/>
                 								 </c:if>
											</Td>
										</Tr>
										<Tr>
										<Td colspan="3" align="center"> <div class="box-footer">
                <input type="button" class="btn btn-primary" value="Verify" onClick="verifyRegistration()" />
                &nbsp;
                 <input type="button" class="btn btn-primary" value="Back" onClick="backToEditClubRegistration()" />
                
              </div>
              </Td>
										</Tr>
										
									</Table>
									 
									<!-- input states -->
								</form>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
					</div>
					<!--/.col (right) -->
				</div>
				<!-- /.row -->
			</section>
			<!-- /.content -->
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->
		<%@include file="clubFooter.jsp"%>

		<!-- Control Sidebar -->
		
		<!-- /.control-sidebar -->
		<!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
		<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->

</body>
<script>
function verifyRegistration(){
	var operationType = $('#operationType').val();
	if(operationType == 'active' || operationType == 'activate' || operationType == 'inactive' || operationType == 'inactivate') {
		if($('#clubOTP').val() == $('#verifyOTP').val()){
	if (confirm("Are you sure to verify the club data?") == true) {
    var formData = { "id" : $('#id').val()};
    var regitrationId = $('#id').val();
    var registrationCode = $('#clubCode').val();
	$.ajax({
		type : 'POST',
		contentType: 'application/json; charset=UTF-8',
		url : '${pageContext.request.contextPath}/club/saveVerifyRegistrationData?regId='+$('#id').val()+'&operationType='+operationType,
		//dataType: 'json',
		success : function(response) {
			if(response == "success") {
				openCommonMessagePage(regitrationId,registrationCode,'ClubRegistration_'+operationType);
			}
			else {
				alert("Error occurred:"+response);
			}
		},
		error : function(response) {
			alert('Error while saving data into DB.');
		}
	});
	}
		} else {
			alert("Invalid OTP entered.");
		}
	}
	else {
		if (confirm("Are you sure to verify the club data?") == true) {
		    var formData = { "id" : $('#id').val()};
		    var regitrationId = $('#id').val();
		    var registrationCode = $('#clubCode').val();
			$.ajax({
				type : 'POST',
				contentType: 'application/json; charset=UTF-8',
				url : '${pageContext.request.contextPath}/club/saveVerifyRegistrationData?regId='+$('#id').val()+'&operationType='+operationType,
				//dataType: 'json',
				success : function(response) {
					if(response == "success") {
						openCommonMessagePage(regitrationId,registrationCode,'ClubRegistration_'+operationType);
					}
					else {
						alert("Error occurred:"+response);
					}
				},
				error : function(response) {
					alert('Error while saving data into DB.');
				}
			});
			}
	}
}
function backToEditClubRegistration(){
	var regitrationId = $('#id').val();
	var operationType = $('#operationType').val();
	window.location.href = "${pageContext.request.contextPath}/club/editClubRegistration?regId="+regitrationId+"&operationType="+operationType;
}
function openCommonMessagePage(paramValue,registrationCode,operationValue) {
	paramValue = paramValue+"~"+registrationCode;
	window.location.href = "${pageContext.request.contextPath}/cmnurl/openMessagePage?paramValue="+paramValue+"&operationType="+operationValue;
}
</script>
</html>
