<%-- 
    Document   : HelpPanel
    Created on : Nov 7, 2015, 1:36:50 PM
    Author     : sunil
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page contentType="text/html"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%
    String[][] helpData = null;
    if(session.getAttribute("HelpData")!=null){
        helpData = (String[][]) session.getAttribute("HelpData");
    }
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <form >
            <table border="1">
            <h1><%= helpData[0][0] %></h1>
            <% for(int i = 0; i<helpData.length; i++) { %>
                <tr><td><%= helpData[i][0] %> <%= helpData[i][1] %> <%= helpData[i][2] %> <%= helpData[i][2] %></td></tr>
            <% }%>
            </table>
        </form>
    </body>
</html>
