<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>${title}</title>

<%@include file="clubHeader.jsp"%>

	
        
</head>

<body class="hold-transition skin-blue sidebar-mini" onload="getRegistrationData()">

	<div class="wrapper">

		<%@include file="clubBodyHeader.jsp"%>
		<!--[START] Left side column. contains the logo and sidebar -->
		<%@include file="clubLeftSidebar.jsp"%>
		<!--[END] Left side column. contains the logo and sidebar -->
<!DOCTYPE HTML>
<html>
<head>  
<script>
window.onload = function () {

var chart = new CanvasJS.Chart("chartContainer", {
	animationEnabled: true,
	theme: "light1", // "light1", "light2", "dark1", "dark2"
	title:{
		text: "Top Oil Reserves"
	},
	axisY: {
		title: "Reserves(MMbbl)"
	},
	data: [{        
		type: "column",  
		showInLegend: true, 
		legendMarkerColor: "grey",
		legendText: "MMbbl = one million barrels",
		dataPoints: [      
			{ y: 300878, label: "Venezuela" },
			{ y: 266455,  label: "Saudi" },
			{ y: 169709,  label: "Canada" },
			{ y: 158400,  label: "Iran" },
			{ y: 142503,  label: "Iraq" },
			{ y: 101500, label: "Kuwait" },
			{ y: 97800,  label: "UAE" },
			{ y: 80000,  label: "Russia" }
		]
	}]
});
chart.render();

}
</script>
</head>
<body>
<div id="chartContainer" style="height: 370px; max-width: 920px; margin: 0px auto;"></div>
<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
</body>
</html>
		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>
					Access Denied
				</h1>
			</section>

			<!-- Main content -->
			<section class="content">
				<div class="row">
					<!-- left column -->

					<!--/.col (left) -->
					<!-- right column -->
					<div class="col-md-10">
						<!-- Horizontal Form -->
						<!-- /.box -->
						<!-- general form elements disabled -->
						<div class="box box-info">
							<div class="box-header with-border">
							<h1>HTTP Status 403 - Access is denied</h1>
							</div>
							<!-- /.box-header -->
							<div class="box-body">
							<c:choose>
		<c:when test="${empty username}">
			<h2>You do not have permission to access this page!</h2>
		</c:when>
		<c:otherwise>
			<h2>Username : ${username} <br/>You do not have permission to access this page!</h2>
		</c:otherwise>
	</c:choose>
								<form name="clubForm">
									<!-- text input -->
									<Table width="100%">
										<Tr>
											<Td valign="top" width="49%">
												<div class="form-group">
																	

													
												</div>
												<div id="chartcontainer">This is just a replacement in case Javascript is not available or used for SEO purposes</div>
											</Td>
											
										</Tr>
										
									</Table>
									 
									<!-- input states -->
								</form>
								<label><a href="${pageContext.request.contextPath}" class="btn btn-default btn-flat">Back to Dahsborad</a></label>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
					</div>
					<!--/.col (right) -->
				</div>
				<!-- /.row -->
			</section>
			<!-- /.content -->
			<!-- /.content -->
		</div>
		
	
        
	



		<!-- /.content-wrapper -->
		<%@include file="clubFooter.jsp"%>

		<!-- Control Sidebar -->
		
		<!-- /.control-sidebar -->
		<!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
		<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->

</body>

</html>
