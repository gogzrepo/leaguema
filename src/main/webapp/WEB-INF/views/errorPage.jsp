<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<body>
	<h1>Oops !!! Error Occurred !</h1>

	<c:choose>
		<c:when test="${not empty username}">
			<h2>Something went wrong, please check the details!</h2>
		</c:when>
		<c:otherwise>
			<h2>Something went wrong, please check the details!</h2>
			<h2><a
				href="${pageContext.request.contextPath}/">Home Page</a></h2>
		</c:otherwise>
	</c:choose>
	<h3>Error Message:-</h3>
	<h3>${errorMessage }</h3>
	
	<h3>Error Details:-</h3>
	<h4>${errorDetail }</h4>

	<c:url value="/logout" var="logoutUrl" />
	<form action="${logoutUrl}" method="post" id="logoutForm">
		<input type="hidden" name="${_csrf.parameterName}"
			value="${_csrf.token}" />
	</form>
	<script>
		function formSubmit() {
			document.getElementById("logoutForm").submit();
		}
	</script>

	<c:if test="${pageContext.request.userPrincipal.name != null}">
		<h2>
			User : ${pageContext.request.userPrincipal.name} | <a
				href="javascript:formSubmit()"> Logout</a>
		</h2>
	</c:if>
	

</body>
</html>