<!DOCTYPE html>
<%@page import="java.util.List"%>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Registration Page</title>

<%@include file="clubHeader.jsp"%>
<%@page import="com.leaguema.model.ClubRegistration"%>
</head>
<body class="hold-transition skin-blue sidebar-mini" onload="">
<% 

List<ClubRegistration> listClub = null;
if(request.getAttribute("AllClub") != null)
	listClub = (List<ClubRegistration>)request.getAttribute("AllClub");
%>
	<div class="wrapper">

		<%@include file="clubBodyHeader.jsp"%>
		<!--[START] Left side column. contains the logo and sidebar -->
		<%@include file="clubLeftSidebar.jsp"%>
		<!--[END] Left side column. contains the logo and sidebar -->

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>
					${pageHeading}
				</h1>
			</section>

			<!-- Main content -->
			<section class="content">
				<div class="row">
					<!-- left column -->

					<!--/.col (left) -->
					<!-- right column -->
					<div class="col-md-10">
						<!-- Horizontal Form -->
						<!-- /.box -->
						<!-- general form elements disabled -->
						<div class="box box-info">
							<div class="box-header with-border">
								<h3 class="box-title">${message }</h3>
							</div>
							<!-- /.box-header -->
							<div class="box-body">
								<form name="clubForm">
									<!-- text input -->
									<Table width="100%">
										<Tr>
											<Td valign="top" width="49%">
												<div class="form-group">
													<label>Club Name</label> <input type="text" id="clubName"
														class="form-control" placeholder="Enter Club Name">
												</div>
											</Td>
											<Td width="2%">&nbsp;</Td>
											<Td width="49%">


												<div class="form-group">
													<label>Club Code</label> <input type="text" id="clubCode"
														class="form-control" >
												</div>
											</Td>
										</Tr>

										<Tr>
										
										<Td valign="top" width="49%">
												<div class="form-group">
													<label>Club Owner</label> <input type="text" id="clubOwner"
														class="form-control" placeholder="Enter Club Owner">
												</div>
											</Td>
										<Td width="2%">&nbsp;</Td>
											<Td width="49%">
											
												<div class="box-footer">
													<input type="button" class="btn btn-primary" value="Search"
														onClick="saveRegistration()" />
												</div>
											</Td>
										</Tr>

									</Table>
										<HR/>
									<!-- input states -->
									
									<div class="box-body">
              <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
              <div class="row"><div class="col-sm-6">
              <div class="dataTables_length" id="example1_length"><label>Show <select name="example1_length" aria-controls="example1" class="form-control input-sm"><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select> entries</label></div></div></div>
              <div class="row"><div class="col-sm-12">
              <table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                <thead>
                <tr role="row">
                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 40px;">Sr. No</th>
                <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 182px;">Club No</th>
                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 224px;">Club Name</th>
                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending" style="width: 199px;">Club Owner</th>
                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending" style="width: 156px;">Owner Mobile</th>
                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 112px;">Club Email</th>
                <c:if test="${operationType == 'update' }">
                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 112px;">Level Of Club</th>
                </c:if>
                <c:if test="${operationType == 'active_inactive' }">
                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 112px;">Status</th>
                </c:if>
                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 112px;">Action</th>
                </tr>
                </thead>
                <tbody id="clubFullListData">
               <c:forEach items="<%=listClub %>" var="data" varStatus="loop">
                <tr role="row" class="odd">
                  <td class="sorting_1">${loop.count }</td>
                  <td>${data.clubCode }</td>
                  <td>${data.clubName }</td>
                  <td>${data.clubFName }</td>
                  <td>${data.mobileNo }</td>
                  <td>${data.clubEmail }</td>
                  <c:if test="${operationType == 'active_inactive' }">
                  <td>
                  <c:if test="${data.isActive == 'true'}">
                  Active
                  </c:if>
                  <c:if test="${data.isActive == 'false'}">
                  Inactive
                  </c:if>
                  
                  </td>
                  </c:if>
                  <c:if test="${operationType == 'update' }">
                   <td>${data.clubLevel }</td>
                   </c:if>
                  <td>
                  <c:if test="${operationType == 'update' }">
                  <button type="button" class="btn btn-default btn-sm" onclick="openClubRegistrationPage(${data.id},'updateEdit')">
          <span class="glyphicon glyphicon-edit"></span> Edit 
        </button>
        </c:if>
        <c:if test="${operationType == 'active_inactive' }">
        <c:if test="${data.isActive == 'true' }">
        <button type="button" class="btn btn-default btn-sm" onclick="openClubRegistrationPage(${data.id},'inactive')">
          <span class="fa fa-safari"></span> Inactive 
        </button>
        </c:if>
        <c:if test="${data.isActive == 'false' }">
        <button type="button" class="btn btn-default btn-sm" onclick="openClubRegistrationPage(${data.id},'active')">
          <span class="fa fa-safari"></span> Active 
        </button>
        </c:if>
        
        </c:if>
        
        
        </td>
                </tr>
                </c:forEach>
                </tbody>
                <tfoot>
                <!-- <tr><th rowspan="1" colspan="1">Rendering engine</th><th rowspan="1" colspan="1">Browser</th><th rowspan="1" colspan="1">Platform(s)</th><th rowspan="1" colspan="1">Engine version</th><th rowspan="1" colspan="1">CSS grade</th></tr>-->
                </tfoot>
              </table></div></div><div class="row"><div class="col-sm-5"><div class="dataTables_info" id="example1_info" role="status" aria-live="polite">Showing 1 to 10 of 57 entries</div></div><div class="col-sm-7"><div class="dataTables_paginate paging_simple_numbers" id="example1_paginate"><ul class="pagination"><li class="paginate_button previous disabled" id="example1_previous"><a href="#" aria-controls="example1" data-dt-idx="0" tabindex="0">Previous</a></li><li class="paginate_button active"><a href="#" aria-controls="example1" data-dt-idx="1" tabindex="0">1</a></li><li class="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="2" tabindex="0">2</a></li><li class="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="3" tabindex="0">3</a></li><li class="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="4" tabindex="0">4</a></li><li class="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="5" tabindex="0">5</a></li><li class="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="6" tabindex="0">6</a></li><li class="paginate_button next" id="example1_next"><a href="#" aria-controls="example1" data-dt-idx="7" tabindex="0">Next</a></li></ul></div></div></div></div>
            </div>
            <!-- /.box-body -->
									
								</form>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
					</div>
					<!--/.col (right) -->
				</div>
				<!-- /.row -->
			</section>
			<!-- /.content -->
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->
		<%@include file="clubFooter.jsp"%>

		<!-- Control Sidebar -->

		<!-- /.control-sidebar -->
		<!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
		<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->

</body>
<script>
	function openClubRegistrationPage(clubId, operationType) {
		
		window.location.href = "${pageContext.request.contextPath}/club/editClubRegistration?regId="+clubId+"&operationType="+operationType;
		
	}
</script>
</html>
