package com.leaguema.testing;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

class Student {
	
	private String name;
	
	private int age;

	
	
	public Student(String name, int age) {
		super();
		this.name = name;
		this.age = age;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the age
	 */
	public int getAge() {
		return age;
	}

	/**
	 * @param age the age to set
	 */
	public void setAge(int age) {
		this.age = age;
	}
	
	
	
}

public class Tesing {
	
	public static void main(String[] args) {
		
		List<Student> listStudent = new ArrayList<>();
		
		Student st1 = new Student("sunil", 26);
		Student st2 = new Student("naveen", 27);
		Student st3 = new Student("rajeev", 24);
		Student st4 = new Student("mohan", 28);
		Student st5 = new Student("govind", 36);
		
		listStudent.add(st1);
		listStudent.add(st2);
		listStudent.add(st3);
		listStudent.add(st4);
		listStudent.add(st5);
		
		
		//Collections.sort(listStudent);
		
	}

}
