package com.leaguema.testing;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.reflect.Constructor;

import sun.security.jca.GetInstance;

class Singleton implements Serializable {
	
	
	public static Singleton obj;
	
	/**
	 * @return the obj
	 */
	public static Singleton getObj() {
		return obj;
	}

	/**
	 * @param obj the obj to set
	 */
	public static void setObj(Singleton obj) {
		Singleton.obj = obj;
	}

	private Singleton() {
		
	}
	
	public static Singleton getInstance() {
		
		if(obj == null) {
			obj = new Singleton();
		}
		
		return obj;
		
	}
	public Object newInstance() throws InstantiationException {
        throw new InstantiationError( "Creating of this object is not allowed." );
    }
	public Object readResolve() {
		return getInstance();
	}
	
}
 public class JavaSingleton {
	 
	 public static void main(String[] args) throws Exception {
		
		 Singleton obj = Singleton.getInstance();
		 
		 System.out.println(obj.toString());
		 
		 
		 System.out.println(obj.toString());
		 
		 
		 FileOutputStream fos = new FileOutputStream("fos.txt");
		 ObjectOutputStream oos = new ObjectOutputStream(fos);
		 
		 oos.writeObject(obj);
		 
		 FileInputStream fis = new FileInputStream("fos.txt");
		 ObjectInputStream ois = new ObjectInputStream(fis);
		 
		 Singleton s2 = (Singleton) ois.readObject();
		 
		 System.out.println(s2.toString()+" ");
		 
		 
		 //Singleton s3 =  (Singleton) Class.forName("com.leaguema.testing.Singleton").newInstance();
		 
		 //System.out.println(s3.toString()+" ");
		 Singleton two = null; 
		 Constructor[] constructors = Singleton.class.getDeclaredConstructors();
         for (Constructor constructor : constructors) {
             //Below code will destroy the singleton pattern
             constructor.setAccessible(true);
             two = (Singleton) constructor.newInstance();
             break;
         }
         
         System.out.println(two.toString());
         
         Singleton s4 = s2;
         
         System.out.println(s4.toString());
	}
	 
 }
