/**
 * 
 */
package com.leaguema.testing;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * @author Sunil
 *
 */
public class AllProgram {
	
	public static void main(String[] args) {
		
		reverseArray();
		sortMapByValues();
		missingNo();
		stringToInt();
	}

	
	static void reverseArray() {
		
		int iArr[] = {8,4,2,5,7};
		
		for(int i = 0, j = iArr.length-1; i < iArr.length/2; i++, j--) {
			int c = iArr[j];
			iArr[j] = iArr[i];
			iArr[i] = c;
		}
		
		for (int i = 0; i < iArr.length; i++) {
			System.out.print(iArr[i]+" ");
		}
	}
	
	static void sortMapByValues() {
		
		Map<String, String> map = new HashMap<>();
		
		map.put("a", "a");
		map.put("c", "c");
		map.put("b", "b");
		map.put("x", "x");
		map.put("k", "k");
		
		List<Map.Entry<String, String>> list = new ArrayList<>(map.entrySet());
		
		LinkedHashMap<String, String> sortedMap =  new LinkedHashMap<>();
		
		Collections.sort(list, new Comparator<Map.Entry<String, String>>() {

			@Override
			public int compare(Entry<String, String> o1, Entry<String, String> o2) {
				
				
				return o2.getValue().compareTo(o1.getValue());
			}
		});
		
		for (Map.Entry<String, String> entry : list) {
			sortedMap.put(entry.getKey(), entry.getValue());
		}
		
		System.out.println(sortedMap);
		//sortedMap
		
	}
	
	
	static void missingNo() {
		
		int iArr[] = {1,2,3,5};
		int len = iArr.length;
		int actualSum = (len+1) * (len+2)/2;
		int sum = 0;
		for(int i = 0; i < iArr.length; i++) {
			
			sum+= iArr[i];
		}
		
		
		System.out.println("missing no is-->"+(actualSum-sum));
		
	}
	
	static void duplicateNo() {
		
		int iArr[] = {1,2,3,4,5,5,6};
		int len = iArr.length;
		int actualSum = (len+1) * (len+2)/2;
		int sum = 0;
		for(int i = 0; i < iArr.length; i++) {
			
			sum+= iArr[i];
		}
		
		
		System.out.println("missing no is-->"+(actualSum-sum));
		
	}
	
	static void stringToInt() {
		
		String s = "1234";
		
		char[] ch = s.toCharArray();
		int intValue = 0;
		int b = (int)'0';
		for(int i = 0; i < ch.length; i++) {
			int a = (int)ch[i];
			System.out.println(a+" "+b);
			intValue = (intValue*10)+(a-b);
			
		}
		System.out.println(intValue+"-->"+convert_String_To_Number());
	}
	 public static int convert_String_To_Number(){
		 String numStr = "1234";Integer.parseInt("");
	        char ch[] = numStr.toCharArray();
	        int sum = 0;
	        //get ascii value for zero
	        int zeroAscii = (int)'0';
	        for(char c:ch){
	            int tmpAscii = (int)c;
	            sum = (sum*10)+(tmpAscii-zeroAscii);
	        }
	        return sum;
	    }
	
}
