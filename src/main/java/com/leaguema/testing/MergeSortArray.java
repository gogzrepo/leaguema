/**
 * 
 */
package com.leaguema.testing;

/**
 * @author Sunil
 *
 */
public class MergeSortArray {
	
	public static void main(String[] args) {
		
		mergeSortArray();
		
	}
	
	static void mergeSortArray(){
		
		int iArr1[] = {4,6,9,3,2};
		
		int iArr2[] = {3,8,4,7,8};
		int iArr3[] = new int[iArr1.length+iArr2.length];
		int i = 0;
		int j = 0;
		int count = 0;
		while(i < iArr1.length && j < iArr2.length) {
			
			if(iArr1[i] < iArr2[j]) {
				iArr3[count++] = iArr1[i++];//4(0),6(1)
			}
			else {
				iArr3[count++] = iArr2[j++];//3(0),8(1)
			}
		}
		
		while(i < iArr1.length) {
			iArr3[count++] = iArr1[i++];
		}
		
		while(j < iArr2.length) {
			iArr3[count++] = iArr2[j++];
		}
		
		for (int k = 0; k < iArr3.length; k++) {
			System.out.print(" "+iArr3[k]);
		}
		
	}

}
