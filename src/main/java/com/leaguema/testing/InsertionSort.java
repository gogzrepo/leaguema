/**
 * 
 */
package com.leaguema.testing;

/**
 * @author Sunil
 *
 */
public class InsertionSort {
	
	public static void main(String[] args) {
		
		int iArr1[] = {4,5,3,2,1};
		
		int iArr2[] = new int[iArr1.length];
		int count = 0;
		int c = 0;
		for (int i = 0; i < iArr1.length; i++) {
			
			
			if( i == 0) {
				iArr2[i] = iArr1[i];
			}
			else{
				for( int j = i; j > 0; j--) {
					if(iArr2[j-1] > iArr1[i]) {
						c = iArr2[j-1];
						iArr2[j-1] = iArr1[i];
						iArr2[j] = c;
						//iArr1[i] = c;
					}
					else {
						if(iArr2[j-1] > iArr2[j])
							iArr2[count] = iArr1[i];
					}
				}
			}
			count++;
		}
		
		for (int i = 0; i < iArr2.length; i++) {
			System.out.print(" "+iArr2[i]);
		}
		
		insertionSort(iArr1);
		
	}
	
	private static void insertionSort(int[] arr) {
		for (int i = 1; i < arr.length; i++) {
			int valueToSort = arr[i];
			int j = i;
			while (j > 0 && arr[j - 1] > valueToSort) {
				arr[j] = arr[j - 1];
				j--;
			}
			arr[j] = valueToSort;
		}
	}

}
