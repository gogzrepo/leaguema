package com.leaguema.testing;

public class FactorProgram {
	
	public static void main(String[] args) {
		
		int no = 16;
		int a = 0;
		int div = 2;
		String factor = "";
		int firstTime = 1;
		while(no > 1) {
			
			if(no%div == 0) {
				if(firstTime != 1) 
					factor = factor +"*"+ div;
				else
					factor += div;
				firstTime = 2;
				no = no/div;
			}
			else {
				
				div++;
			}
		}
		
		System.out.println("-->"+factor);
		
		
	}

}
