package com.leaguema.testing;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
class Super {
	
	
	/*Super(String name) {
		System.out.println("super constructor called.");
		
	}*/
	private String city;

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}
	
	
}
public class SerializationTest extends Super implements Serializable {
	//private Tree tree = new Tree();

	//Super s;
	private String name;
	
	private static String address;
	
	transient int a = 9;
	transient Integer ai = new Integer(10);
	
	/*SerializationTest() {
		super("");
	}
	*/
	
	/**
	 * @return the address
	 */
	public static String getAddress() {
		return address;
	}



	/**
	 * @param address the address to set
	 */
	public static void setAddress(String address) {
		SerializationTest.address = address;
	}



	//private AB ab = new AB();
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}



	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}



	public static void main(String[] args) {
		//Super s = new
		SerializationTest f = new SerializationTest();
				f.setCity("seohara");
				f.setName("sunil");
				//setAddress("seohara");
				address = "abcd";
		//sf.;
		try {
			FileOutputStream fs = new FileOutputStream("Forest.ser");
			ObjectOutputStream os = new ObjectOutputStream(fs);
			os.writeObject(f);
			os.close();
			System.out.println("Serialization done.");
			
			FileInputStream fis  = new FileInputStream("Forest.ser");
			ObjectInputStream ois = new ObjectInputStream(fis);
			SerializationTest s=(SerializationTest)ois.readObject();
			
			System.out.println(s.getCity()+" "+s.getName()+" "+getAddress()+" "+s.ai+" "+s.a);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
}

class AB {
	
	private String address;

	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}
	
	
}
