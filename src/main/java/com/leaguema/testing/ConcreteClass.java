/**
 * 
 */
package com.leaguema.testing;

/**
 * @author Sunil
 *
 */
class Con1 implements Cloneable{
	
	public String s = "my name";
	
	@Override
	protected Object clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		return super.clone();
	}
	
	
}


public class ConcreteClass {

	/**
	 * @param args
	 * @throws CloneNotSupportedException 
	 */
	public static void main(String[] args) throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		
		Con1 c1 = new Con1();
		System.out.println(c1.s+" "+c1.toString());
		
		c1.s = "second";
		
		Con1 c2 = (Con1)c1.clone();
		System.out.println(c2.s+" "+c2.toString());
		c2.s="third";
		
		System.out.println(c1.s+" "+c1.toString());
		System.out.println(c2.s+" "+c2.toString());
		
		

	}

}
