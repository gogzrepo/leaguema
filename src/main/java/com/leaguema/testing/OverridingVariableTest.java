package com.leaguema.testing;

class First {
	
	int count = 10;
	
	void display() {
		
		System.out.println("This is display super class method.");
	}
}

class Second extends First {
	
	int count = 20;
	
	void display() {
		
		System.out.println("This is sub class method.");
	}
}

public class OverridingVariableTest {

	public static void main(String[] args) {
		
		First f = new Second();
		
		System.out.println(f.count);

		f.display();
	}
}
