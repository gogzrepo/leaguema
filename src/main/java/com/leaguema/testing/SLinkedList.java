/**
 * 
 */
package com.leaguema.testing;

/**
 * @author Sunil
 *
 */
class Node {
	
	String data;
	int size = 0;
	Node next;
	Node head;
	Node() {
		head = null;
		next = null;
	}
	Node(String data) {
		this.data = data;
	}
	
	public void add(String data) {
		Node node = new Node(data);
		//node.next = head;
		if(head == null) {
			head = node;
		head.size++;
		}
		else {
			Node tmpNode = head;
			while(tmpNode.next != null)  {
				tmpNode = tmpNode.next;
			}
			tmpNode.next = node;
			head.size++;
		}
		
	}
	
	public void delete(int index) {
		int count = 0;
		if(head.size > 0 && index <= head.size) {
			Node tmp = head;
			while(count+1 != index) {
				tmp = tmp.next;
				count++;
			}
			System.out.println(tmp);
			if(index < head.size)
			 tmp.next = (tmp.next).next;
			else
			 tmp.next = null;
			head.size--;
		}
	}
	
	public int getSize() {
		return head.size;
	}
	
}

public class SLinkedList {

	
	public static void main(String[] args) {
		
		
		Node n = new Node();
		n.add("a1");
		n.add("a2");
		n.add("a3");
		n.add("a4");
		
		System.out.println("size-->"+n.getSize());
		n.delete(2);
		System.out.println("size-->"+n.getSize());
		
		System.out.println();
		n.add("a5");
		
		System.out.println("size-->"+n.getSize());
	}
	
}
