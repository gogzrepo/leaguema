package com.leaguema.testing;

import java.sql.SQLException;

import com.leaguema.bean.Employee;

class AbstractClass {
	
	int a = 10;
	
	
	/*public void test(String...strings) {
		System.out.println("string varags method");
	}*/
	
	void sum(int a, int...n) {
		System.out.println("varags first method.");
	}
	
	void sum(int a, int b, int...n) {
		System.out.println("varags second method.");
	}
	
	void test(Object...objects) {
		System.out.println("object varags method.");
	}
	
	protected  AbstractClass() {
		
		System.out.println("Abstract class constructor..");
	}
	
	void display() throws SQLException {
		System.out.println("display super method.");
	}
	
	Object print () {
		
		System.out.println("super class print");
		
		return "";
	}
	
}

public class OverridingTest extends AbstractClass {
	
	int a = 20;
	public OverridingTest() {
		System.out.println("Overriding test constructor."+this.toString()+" "+super.toString());
	}
	
	/*public void test(String...strings) {
		System.out.println("sub varags method");
	}
	*/
	void display()  {
		System.out.println("display super method.");
	}
	
	Object print() {
		
		System.out.println("sub class method.");
		
		return new Employee();
	}
	
	void show() {
		
		System.out.println("This is sub class show method.");
	}
	
	public static void main(String[] args) throws InterruptedException {
		
		AbstractClass test = new OverridingTest();
		
		System.out.println(test.toString()+" "+test.a);
		
		test.test("sunil");
		test.test(null, new Employee());
		//test.sum(3, 4);
		
		//Thread.sleep(10* 60 * 1000);
		
		
	}

}
