package com.leaguema.testing;

class Animal 
{ 
    public void callme()
    {
        System.out.println("In callme of Animal");
    }
}


class Dog extends Animal 
{ 
    public void callme()
    {
        System.out.println("In callme of Dog");
    }

    public void callme2()
    {
        System.out.println("In callme2 of Dog");
    }
}

class Cat extends Animal 
{ 
    public void callme()
    {
        System.out.println("In callme of Cat");
    }

    public void callme2()
    {
        System.out.println("In callme2 of Cat");
    }
}

public class UseAnimals 
{
    public static void main (String [] args) 
    {
       
        
    }
    
    public void display(Object o) {
    	
    	 Cat d = new Cat();      
         Animal a = (Animal)o;
         a.callme();
    	
    }
}