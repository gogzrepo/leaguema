package com.leaguema.testing;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

interface I1 {
	
	default void show () {
		System.out.println("default method");
	}
	
	static void print() {
		
		System.out.println("hii");
	}
	
	void display(String name);
}

class Implementation implements I1 {

	@Override
	public void display(String name) {
		// TODO Auto-generated method stub
		
	}

	public void test() {
		// TODO Auto-generated method stub
		
	}

	
	
	
	
}
public class LambdaExpressionTesting {

	public static void main(String[] args) {

		
		List<Integer> list = Arrays.asList(5,7,3,4,8);
		for(Integer i: list) {
		
			System.out.println(i);
		}
		System.out.println("using lambda expression..");
		list.forEach(i-> System.out.println(i));
		
		I1 i1 = (String a)-> System.out.println("display method called-->"+a);
		
		i1.display("sunil kumar");
		
		I1 i21 = (name)-> System.out.println("display method called.");
		
	}

}
