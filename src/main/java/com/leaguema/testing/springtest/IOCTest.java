/**
 * 
 */
package com.leaguema.testing.springtest;

import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author Sunil
 *
 */
public class IOCTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		
		/*ApplicationContext ctx = new ClassPathXmlApplicationContext("testing-servlet.xml");
		Employee emp = (Employee) ctx.getBean("emp");
		
		System.out.println(emp.toString()+" "+emp.getAge());
		emp.setAge(20);
		Employee emp2 = (Employee) ctx.getBean("emp");
		
		
		System.out.println(emp2.toString()+" "+emp2.getAge());
		ApplicationContext ctx2 = new ClassPathXmlApplicationContext("testing-servlet.xml");
		
		Employee emp3 = (Employee) ctx2.getBean("emp");
		
		
		System.out.println(emp3.toString()+" "+emp3.getAge());*/
		
		AbstractApplicationContext ctx = new ClassPathXmlApplicationContext("testing-servlet.xml");
		
		Employee emp1 = (Employee)ctx.getBean("emp");
		
		System.out.println(emp1.getName());
		
		ctx.close();
		
		
		
		
	}

}
