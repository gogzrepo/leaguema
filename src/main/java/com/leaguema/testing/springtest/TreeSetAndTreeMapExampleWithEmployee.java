/**
 * 
 */
package com.leaguema.testing.springtest;

import java.util.Comparator;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.NavigableSet;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

/**
 * @author Sunil
 *
 */
class Student implements Comparable<Student>{
	String name;
	
	int age;

	/**
	 * @param name
	 * @param age
	 */
	public Student(String name, int age) {
		this.name = name;
		this.age = age;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the age
	 */
	public int getAge() {
		return age;
	}

	/**
	 * @param age the age to set
	 */
	public void setAge(int age) {
		this.age = age;
	}

	public int compareTo(Student o) {
		// TODO Auto-generated method stub
		return this.getName().compareToIgnoreCase(o.getName());
	}
	
	
}


class StudentNameComparator implements Comparator<Student> {

	public int compare(Student o1, Student o2) {
		// TODO Auto-generated method stub
		return o1.getAge()-(o2.getAge());
	}
	
}

public class TreeSetAndTreeMapExampleWithEmployee {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		Student emp1 = new Student("a",10);
		Student emp2 = new Student("b",20);
		Student emp3 = new Student("E",5);
		Student emp4 = new Student("A",40);
		Student emp5 = new Student("d",30);
		
		TreeMap<Student, Student> treeMap = new TreeMap<Student, Student>();
		treeMap.put(emp1, emp1);
		treeMap.put(emp2, emp2);
		treeMap.put(emp3, emp3);
		treeMap.put(emp4, emp4);
		treeMap.put(emp5, emp5);
		//treeMap.put(null, null);
		
		Set set0 = treeMap.entrySet();
		
		Iterator itr = set0.iterator();
		
		while(itr.hasNext()) {
			
		Entry<Student, Student> entry = (Entry<Student, Student>)	itr.next();
		Student emp = entry.getKey();
		System.out.println(emp.getAge()+" "+emp.getName());
		
		}
		System.out.println("========================================");
		for(Entry<Student, Student> empEntry: treeMap.entrySet()) {
			Student emp = empEntry.getKey();
			System.out.println(emp.getAge()+" "+emp.getName());
		}
		
		NavigableSet<Student> set = new TreeSet<Student>().descendingSet();
		
		set.add(emp1);
		set.add(emp2);
		set.add(emp3);
		set.add(emp4);
		set.add(emp5);
		System.out.println("========================================");
		for(Student stu: set) {
			System.out.println(stu.getAge()+" "+stu.getName());
		}
		
	

	}

}
