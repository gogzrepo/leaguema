/**
 * 
 */
package com.leaguema.testing.springtest;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

/**
 * @author Sunil
 *
 */
public class Employee {

	/**
	 * @param args
	 */
	
	/*@PostConstruct
	public void init() {
		System.out.println("init method by using @PostConstruct annotation.");
	}
	
	@PreDestroy
	public void destroy() {
		System.out.println("destroy method by using @PreDestroy annotation.");
	}*/
	
	private String name;
	
	private int age;

	
	
	/**
	 * @param name
	 * @param age
	 */
	public Employee(String name, int age) {
		this.name = name;
		this.age = age;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the age
	 */
	public int getAge() {
		return age;
	}

	/**
	 * @param age the age to set
	 */
	public void setAge(int age) {
		this.age = age;
	}

	/*public int compareTo(Employee o) {
		
		return this.getName().compareToIgnoreCase(o.getName());
	}*/

	/*public void afterPropertiesSet() throws Exception {

		System.out.println("afterPropertySet method of inilizeBean interface");
	}

	public void destroy() throws Exception {

		System.out.println("destroy method of disposal bean");
		
	}*/
	
	
	

}
