package com.leaguema.interceptor;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.HandlerMapping;
import org.springframework.web.servlet.ModelAndView;


/**
 * @author nysa
 *
 */
public class HandleInterceptor implements HandlerInterceptor {

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		
		HandlerMethod method = (HandlerMethod) handler;
        String clName = method.getMethod().getDeclaringClass().getName();
        String mName = method.getMethod().getName();
        String pageUrl = (String) request.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE);
       
        System.out.println("clName==>"+clName+"--methodName==>"+mName+"--requestedURL==>"+pageUrl);
        
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		if (auth != null) {
			Object name = auth.getName(); // get logged in username
			if (!name.equals("anonymousUser")) {
				if (request.getSession().getAttribute("USER_NAME") == null) {
					
					
						name = auth.getName();
					
					request.getSession().setAttribute("USER_NAME", name);
				}

			}
			DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
			Date date = new Date();
			request.getSession().setAttribute("CURRENT_DATE", dateFormat.format(date));
		}
		return true;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		
	}

}
