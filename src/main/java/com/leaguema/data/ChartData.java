package com.leaguema.data;

public class ChartData {
	private Datasets[] datasets;

	private String[] labels;

	public Datasets[] getDatasets() {
		return datasets;
	}

	public void setDatasets(Datasets[] datasets) {
		this.datasets = datasets;
	}

	public String[] getLabels() {
		return labels;
	}

	public void setLabels(String[] labels) {
		this.labels = labels;
	}

	@Override
	public String toString() {
		return "ClassPojo [datasets = " + datasets + ", labels = " + labels + "]";
	}
}