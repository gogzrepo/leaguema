package com.leaguema.data;

public class Datasets {
	private String pointHighlightStroke;

	private String pointStrokeColor;

	private String fillColor;

	private String pointHighlightFill;

	private String[] data;

	private String label;

	private String pointColor;

	private String strokeColor;

	public String getPointHighlightStroke() {
		return pointHighlightStroke;
	}

	public void setPointHighlightStroke(String pointHighlightStroke) {
		this.pointHighlightStroke = pointHighlightStroke;
	}

	public String getPointStrokeColor() {
		return pointStrokeColor;
	}

	public void setPointStrokeColor(String pointStrokeColor) {
		this.pointStrokeColor = pointStrokeColor;
	}

	public String getFillColor() {
		return fillColor;
	}

	public void setFillColor(String fillColor) {
		this.fillColor = fillColor;
	}

	public String getPointHighlightFill() {
		return pointHighlightFill;
	}

	public void setPointHighlightFill(String pointHighlightFill) {
		this.pointHighlightFill = pointHighlightFill;
	}

	public String[] getData() {
		return data;
	}

	public void setData(String[] data) {
		this.data = data;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getPointColor() {
		return pointColor;
	}

	public void setPointColor(String pointColor) {
		this.pointColor = pointColor;
	}

	public String getStrokeColor() {
		return strokeColor;
	}

	public void setStrokeColor(String strokeColor) {
		this.strokeColor = strokeColor;
	}

	@Override
	public String toString() {
		return "ClassPojo [pointHighlightStroke = " + pointHighlightStroke + ", pointStrokeColor = " + pointStrokeColor
				+ ", fillColor = " + fillColor + ", pointHighlightFill = " + pointHighlightFill + ", data = " + data
				+ ", label = " + label + ", pointColor = " + pointColor + ", strokeColor = " + strokeColor + "]";
	}
}