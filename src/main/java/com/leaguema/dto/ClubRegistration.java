/**
 * 
 */
package com.leaguema.dto;

import java.sql.Timestamp;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

/**
 * @author Sunil
 *
 */
public class ClubRegistration {
	
	private long id;
	
	private String clubName;
	
	private String clubCode;
	
	private String clubAddress;
	
	private String clubSubMainAddress;
	
	private String clubFName;
	
	private String clubLName;
	
	private String clubWebsite;
	
	private String clubEmail;
	
	private String mobileNo;
	
	private String clubPwd;
	
	private String clubBeltType;
	
	private String clubCertificateNo;
	
	private String clubCertificateFile;
	
	private String clubCertificateIdCardFile;
	
	private String clubType;
	
	private String clubBeltNo;
	
	private String clubNoOfStudent;
	
	private String clubLevel;
	
	private boolean isActive;
	
	private Timestamp createdOn;
	
	private int pincode;
	
	private boolean isVerifyRegistration = false;
	
	private String roleInClub;
	
	private long clubParentId;
	
	private String clubParentCode;
	
	private List<PlayerRegistration> playerRegistration;

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the clubName
	 */
	public String getClubName() {
		return clubName;
	}

	/**
	 * @param clubName the clubName to set
	 */
	public void setClubName(String clubName) {
		this.clubName = clubName;
	}

	/**
	 * @return the clubCode
	 */
	public String getClubCode() {
		return clubCode;
	}

	/**
	 * @param clubCode the clubCode to set
	 */
	public void setClubCode(String clubCode) {
		this.clubCode = clubCode;
	}

	/**
	 * @return the clubAddress
	 */
	public String getClubAddress() {
		return clubAddress;
	}

	/**
	 * @param clubAddress the clubAddress to set
	 */
	public void setClubAddress(String clubAddress) {
		this.clubAddress = clubAddress;
	}

	/**
	 * @return the clubSubMainAddress
	 */
	public String getClubSubMainAddress() {
		return clubSubMainAddress;
	}

	/**
	 * @param clubSubMainAddress the clubSubMainAddress to set
	 */
	public void setClubSubMainAddress(String clubSubMainAddress) {
		this.clubSubMainAddress = clubSubMainAddress;
	}

	/**
	 * @return the clubFName
	 */
	public String getClubFName() {
		return clubFName;
	}

	/**
	 * @param clubFName the clubFName to set
	 */
	public void setClubFName(String clubFName) {
		this.clubFName = clubFName;
	}

	/**
	 * @return the clubLName
	 */
	public String getClubLName() {
		return clubLName;
	}

	/**
	 * @param clubLName the clubLName to set
	 */
	public void setClubLName(String clubLName) {
		this.clubLName = clubLName;
	}

	/**
	 * @return the clubWebsite
	 */
	public String getClubWebsite() {
		return clubWebsite;
	}

	/**
	 * @param clubWebsite the clubWebsite to set
	 */
	public void setClubWebsite(String clubWebsite) {
		this.clubWebsite = clubWebsite;
	}

	/**
	 * @return the clubEmail
	 */
	public String getClubEmail() {
		return clubEmail;
	}

	/**
	 * @param clubEmail the clubEmail to set
	 */
	public void setClubEmail(String clubEmail) {
		this.clubEmail = clubEmail;
	}

	/**
	 * @return the mobileNo
	 */
	public String getMobileNo() {
		return mobileNo;
	}

	/**
	 * @param mobileNo the mobileNo to set
	 */
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	/**
	 * @return the clubPwd
	 */
	public String getClubPwd() {
		return clubPwd;
	}

	/**
	 * @param clubPwd the clubPwd to set
	 */
	public void setClubPwd(String clubPwd) {
		this.clubPwd = clubPwd;
	}

	/**
	 * @return the clubBeltType
	 */
	public String getClubBeltType() {
		return clubBeltType;
	}

	/**
	 * @param clubBeltType the clubBeltType to set
	 */
	public void setClubBeltType(String clubBeltType) {
		this.clubBeltType = clubBeltType;
	}

	/**
	 * @return the clubCertificateNo
	 */
	public String getClubCertificateNo() {
		return clubCertificateNo;
	}

	/**
	 * @param clubCertificateNo the clubCertificateNo to set
	 */
	public void setClubCertificateNo(String clubCertificateNo) {
		this.clubCertificateNo = clubCertificateNo;
	}

	/**
	 * @return the clubCertificateFile
	 */
	public String getClubCertificateFile() {
		return clubCertificateFile;
	}

	/**
	 * @param clubCertificateFile the clubCertificateFile to set
	 */
	public void setClubCertificateFile(String clubCertificateFile) {
		this.clubCertificateFile = clubCertificateFile;
	}

	/**
	 * @return the clubCertificateIdCardFile
	 */
	public String getClubCertificateIdCardFile() {
		return clubCertificateIdCardFile;
	}

	/**
	 * @param clubCertificateIdCardFile the clubCertificateIdCardFile to set
	 */
	public void setClubCertificateIdCardFile(String clubCertificateIdCardFile) {
		this.clubCertificateIdCardFile = clubCertificateIdCardFile;
	}

	/**
	 * @return the clubType
	 */
	public String getClubType() {
		return clubType;
	}

	/**
	 * @param clubType the clubType to set
	 */
	public void setClubType(String clubType) {
		this.clubType = clubType;
	}

	/**
	 * @return the clubBeltNo
	 */
	public String getClubBeltNo() {
		return clubBeltNo;
	}

	/**
	 * @param clubBeltNo the clubBeltNo to set
	 */
	public void setClubBeltNo(String clubBeltNo) {
		this.clubBeltNo = clubBeltNo;
	}

	/**
	 * @return the clubNoOfStudent
	 */
	public String getClubNoOfStudent() {
		return clubNoOfStudent;
	}

	/**
	 * @param clubNoOfStudent the clubNoOfStudent to set
	 */
	public void setClubNoOfStudent(String clubNoOfStudent) {
		this.clubNoOfStudent = clubNoOfStudent;
	}

	/**
	 * @return the clubLevel
	 */
	public String getClubLevel() {
		return clubLevel;
	}

	/**
	 * @param clubLevel the clubLevel to set
	 */
	public void setClubLevel(String clubLevel) {
		this.clubLevel = clubLevel;
	}

	/**
	 * @return the isActive
	 */
	public boolean getIsActive() {
		return isActive;
	}

	/**
	 * @param isActive the isActive to set
	 */
	public void setIsActive(boolean isActive) {
		this.isActive = isActive;
	}

	/**
	 * @return the createdOn
	 */
	public Timestamp getCreatedOn() {
		return createdOn;
	}

	/**
	 * @param createdOn the createdOn to set
	 */
	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * @return the pincode
	 */
	public int getPincode() {
		return pincode;
	}

	/**
	 * @param pincode the pincode to set
	 */
	public void setPincode(int pincode) {
		this.pincode = pincode;
	}

	/**
	 * @return the isVerifyRegistration
	 */
	public boolean isVerifyRegistration() {
		return isVerifyRegistration;
	}

	/**
	 * @param isVerifyRegistration the isVerifyRegistration to set
	 */
	public void setVerifyRegistration(boolean isVerifyRegistration) {
		this.isVerifyRegistration = isVerifyRegistration;
	}

	/**
	 * @return the roleInClub
	 */
	public String getRoleInClub() {
		return roleInClub;
	}

	/**
	 * @param roleInClub the roleInClub to set
	 */
	public void setRoleInClub(String roleInClub) {
		this.roleInClub = roleInClub;
	}

	/**
	 * @return the clubParentId
	 */
	public long getClubParentId() {
		return clubParentId;
	}

	/**
	 * @param clubParentId the clubParentId to set
	 */
	public void setClubParentId(long clubParentId) {
		this.clubParentId = clubParentId;
	}

	/**
	 * @return the clubParentCode
	 */
	public String getClubParentCode() {
		return clubParentCode;
	}

	/**
	 * @param clubParentCode the clubParentCode to set
	 */
	public void setClubParentCode(String clubParentCode) {
		this.clubParentCode = clubParentCode;
	}

	/**
	 * @return the playerRegistration
	 */
	public List<PlayerRegistration> getPlayerRegistration() {
		return playerRegistration;
	}

	/**
	 * @param playerRegistration the playerRegistration to set
	 */
	public void setPlayerRegistration(List<PlayerRegistration> playerRegistration) {
		this.playerRegistration = playerRegistration;
	}

}
