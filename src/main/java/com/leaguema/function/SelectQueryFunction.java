/**
 * 
 */
package com.leaguema.function;

import java.util.List;

import com.leaguema.model.Menu;

/**
 * @author Sunil
 *
 */
public class SelectQueryFunction {
	
	public static String[][] fetchData(int noOfColumn, List<Menu> menuList) {
		String data[][] = null;
		if(menuList == null || noOfColumn < 1 )
			return null;
		data = new String[menuList.size()][noOfColumn];
		int row = 0;
		for (Menu menu : menuList) {
			data[row][0] = menu.getMenuFName();
			data[row][1] = menu.getMenuNext();
			data[row][2] = menu.getMenuParentId();
			data[row][3] = menu.getMenuIndex();
			data[row][3] = menu.getMenuUid()+"";
			row++;
		}
		return data;
	}

}
