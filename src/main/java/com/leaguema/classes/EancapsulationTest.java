/**
 * 
 */
package com.leaguema.classes;

/**
 * @author Sunil
 *
 */

class Cap1 {
	
	private String name;

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	
}

public class EancapsulationTest {

	/**
	 * @param args
	 */
	
	
	public static void main(String[] args) {
		
		Cap1 objC = new Cap1();
		objC.setName("sunil hdjskgf");
		System.out.println(objC.getName());
		

	}

}
