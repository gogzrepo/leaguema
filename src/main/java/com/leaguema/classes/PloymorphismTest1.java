package com.leaguema.classes;



/**
 * This class is used for static polymorphism
 * */
public class PloymorphismTest1 {

	
	void show(int a) {
		System.out.println("int a");
	}
	
	void show(long a) {
		System.out.println("long a");
	}
	void show(String a) {
		System.out.println("String a");
	}
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		PloymorphismTest1 abc = new PloymorphismTest1();
		//long b = 9;
		//abc.show(9l);
		String a = "dfhdskjgh";
		abc.show(a);
	}

}
