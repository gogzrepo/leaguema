/**
 * 
 */
package com.leaguema.classes;

/**
 * @author Sunil
 *
 */
class A {
	
	int a = 20;
	
	 void display() {
		System.out.println("this is super class display method");
	}
}

public class InheritanceTest extends A{

	
	int a = 30;
	/**
	 * @param args
	 */
	void display() {
		System.out.println("this is sub class display method");
	}
	
	public static void main(String[] args) {
		
		A obja = new InheritanceTest();
		obja.display();
		
		//System.out.println(obja.a);
		
		
	}

}
