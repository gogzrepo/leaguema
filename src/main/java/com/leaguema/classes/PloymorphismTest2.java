package com.leaguema.classes;


class Base {
	
	
	int b = 40;
	void execute(int a) {
		
		a = a*2;
		System.out.println("a="+a);
		
	}
	
}
/**
 * This class is used for dynamic polymorphism
 * */
public class PloymorphismTest2 extends Base {

	int b = 30;
	void execute(int a) {
			a = a*3;
			System.out.println("a="+a);
			
		}
	
	void display(int a) {
		System.out.println("display");
	}
	
	
	public static void main(String[] args) {
		Base obj = new PloymorphismTest2();
		obj.execute(10);
		System.out.println(obj.b);
		
	}

}
