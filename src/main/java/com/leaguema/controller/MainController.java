package com.leaguema.controller;

import java.sql.SQLException;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.leaguema.model.BMUser;
import com.leaguema.service.ClubRegistrationService;
import com.leaguema.service.IMenuService;
import com.leaguema.service.IUserService;

@Controller
public class MainController {

	@Autowired
	IMenuService menuService;
	
	@Autowired
	private ClubRegistrationService clubRegistrationService;
	
	private String [][] menuData = null;
    private String[] userParam = null;
    private int ii = 0;
	
	@RequestMapping(value = { "/", "/home" }, method = RequestMethod.GET)
	public ModelAndView defaultPage(HttpServletRequest request) throws Exception {

		ModelAndView model = new ModelAndView();
		model.addObject("title", "dashboard");
		model.addObject("message", "Welcome to dashboard");
		HttpSession session = request.getSession(false);
		List<Object> objList = null;
		String userName = getPrincipal();
		if(userName == null || userName == "anonymousUser") {
			model.setViewName("login");
			if(session != null) {
				/*if(session.getAttribute("UserMenuData") != null)
					session.removeAttribute("UserMenuData");*/
				
				if(session.getAttribute("LoggedUserData") != null)
					session.removeAttribute("LoggedUserData");
			}
		}
		else {
			if(session.getAttribute("LoggedUserData") == null) {
				
				/* objList = menuService.getMenuData("1", "1");
	             String[][] treeMenuData = (String[][])objList.get(0);
	             ii = 0;
	             menuData = null;
	             SortMenuData(treeMenuData);
	             //int maxLen = (int)objList.get(0);
	             //session.setAttribute("SessionParam", sessionParam);
	             session.setAttribute("UserMenuData", menuData);*/
				
				/***Get logged user data and put into a session,and this session will be remove once user logout or session timeout***/
				BMUser user = clubRegistrationService.getLoggedUserData(userName);
				
				if(user != null) {
					session.setAttribute("LoggedUserData", user);
				}
			}
			if(session.getAttribute("LoggedUserData") != null) {
				System.out.println("User data is-->"+((BMUser)(session.getAttribute("LoggedUserData"))).getUserFName());
			}
			model.setViewName("redirect:/cmnurl/openDashboard");
		}
		return model;
	}
	
	@RequestMapping(value = { "/welcome" }, method = RequestMethod.GET)
	public ModelAndView openHomePage() {

		ModelAndView model = new ModelAndView();
		model.addObject("title", "Spring Security + Hibernate Example");
		model.addObject("message", "This is default page!");
		model.setViewName("welcome");
		return model;

	}

	@RequestMapping(value = "/admin**", method = RequestMethod.GET)
	public ModelAndView adminPage() {

		ModelAndView model = new ModelAndView();
		model.addObject("title", "Spring Security + Hibernate Example, ADMIN Page.");
		model.addObject("message", "This page is for ROLE_ADMIN only!");
		model.setViewName("admin");

		return model;

	}
	
	@RequestMapping(value = "/dba**", method = RequestMethod.GET)
	public ModelAndView dbaPage() {

		ModelAndView model = new ModelAndView();
		model.addObject("title", "Spring Security + Hibernate Example, DBA Page.");
		model.addObject("message", "This page is for ROLE_ADMIN and ROLE_DBA only!");
		model.setViewName("dba");

		return model;

	}

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView login(@RequestParam(value = "error", required = false) String error,
			@RequestParam(value = "logout", required = false) String logout, HttpServletRequest request) {
		HttpSession session = request.getSession();
		ModelAndView model = new ModelAndView();
		if (error!=null) {
			model.addObject("error", getErrorMessage(request, "SPRING_SECURITY_LAST_EXCEPTION"));
		}

		if (logout != null) {
			if(session.getAttribute("LoggedUserData") != null)
				session.removeAttribute("LoggedUserData");
			model.addObject("msg", "You've been logged out successfully.");
		}
		model.setViewName("login");

		return model;

	}

	// customize the error message
	private String getErrorMessage(HttpServletRequest request, String key) {

		Exception exception = (Exception) request.getSession().getAttribute(key);

		String error = "";
		if (exception instanceof BadCredentialsException) {
			error = "Invalid username and password!";
		} else if (exception instanceof LockedException) {
			error = exception.getMessage();
		} else {
			error = "Invalid username and password!";
		}

		return error;
	}

	// for 403 access denied page
	@RequestMapping(value = "/accessDenied", method = RequestMethod.GET)
	public ModelAndView accesssDenied() {

		ModelAndView model = new ModelAndView();

		// check if user is login
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (!(auth instanceof AnonymousAuthenticationToken)) {
			UserDetails userDetail = (UserDetails) auth.getPrincipal();
			System.out.println(userDetail);

			model.addObject("username", userDetail.getUsername());

		}

		model.setViewName("accessDenied");
		return model;

	}
	 private String getPrincipal(){
	        String userName = null;
	        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	 
	        if (principal instanceof UserDetails) {
	            userName = ((UserDetails)principal).getUsername();
	        } else {
	            userName = principal.toString();
	        }
	        return userName;
	    }
	///////////////////////////////////////
	
	/*@Autowired
	IUserService service;
	
	@RequestMapping(value = "user")
	public ModelAndView openUserMaintenancePage(@ModelAttribute BMUser userForm) {
		ModelAndView mv = new ModelAndView("user/user");
		mv.addObject("stuRegForm", userForm);
		return mv;
	}
	
	
	@RequestMapping(value = "/user/userMaintenanceAction", method = RequestMethod.POST)
	public ModelAndView createUser(@ModelAttribute("stuRegForm") BMUser userForm) {
		ModelAndView mv = new ModelAndView("/user/user");
		BMUser savedUser = service.createUser(userForm);
		if(savedUser !=null && savedUser.getUserUid() > 0)
			mv.addObject("msg", "User created successfully!!!");
		return mv;
	}*/
	///////////////////////////////////////
	/* data[row][0] = menu.getMenuFName();
		data[row][1] = menu.getMenuNext();
		data[row][2] = menu.getMenuParentId();
		data[row][3] = menu.getMenuIndex();
		data[row][4] = menu.getMenuUid()+"";
*/	 void SortMenuData(String[][] mData) {
	        if(mData!=null){
	            menuData = new String[mData.length][5];
	            //logger.info("Length of Menu Data in Acyion--->>"+mData.length);
	        int len = mData.length;
	        for(int i=0;i<len;i++){
	            if(mData[i][1].equals("0")) { // Means It is a Folder
	                if(mData[i][1].equals("0") && mData[i][2].equals("0")) { //Means It is a Parent Folder
	                menuData[ii] = mData[i];
	                ii++;
	                setMenu(mData[i][3],mData,len);
	            }
	        }else if(mData[i][2].equals("0")) { // Means Menu But Outside any Package 
	                menuData[ii] = mData[i];
	                ii++;
	                }
	            } 
	        }
	    }
	 
	 void setMenu(String value,String[][] mData, int l){
	        for(int j=0;j<l;j++) {
	            if(value.equals(mData[j][2])) { 
	                menuData[ii] = mData[j];
	                ii++;
	                if(mData[j][1].equals("0")){
	                    setMenu(mData[j][2]+mData[j][3],mData, l);
	                }
	             }
	         }
	    }
	 
	 
	 @RequestMapping(value = "OpenMenuPage", method = RequestMethod.POST)
	public ModelAndView menuPage(HttpServletRequest request) throws Exception {
		 ModelAndView mv = new ModelAndView("dashboard");
		 
		 HttpSession session = request.getSession();
	        try {
	                Enumeration<String> en = session.getAttributeNames();
	                String param = "";
	                while(en.hasMoreElements()) { 
	                    param = en.nextElement();
	                   /* param ==>MenuNextNo
						Info:   param ==>org.jboss.weld.context.conversation.ConversationIdGenerator
						Info:   param ==>UserCreationForm
						Info:   param ==>org.jboss.weld.context.ConversationContext.conversations
						Info:   param ==>LoginForm
						Info:   param ==>org.apache.struts.action.LOCALE*/
	                    //session.removeAttribute("UserCreationForm");
	                    session.removeAttribute("LoginForm");
	                    //session.removeAttribute("MenuNextNo");
	                    if(!param.equalsIgnoreCase("SessionParam") && !param.equalsIgnoreCase("UserMenuData")&& !param.equalsIgnoreCase("BranchName") && !param.equalsIgnoreCase("UserCode") && !param.equalsIgnoreCase("MainBranchId") && !param.equalsIgnoreCase("UserUID")) {
	                    //session.removeAttribute(param);
	                        System.out.println("param ==>"+param);
	                    }
	                }
	            
	        } catch(Exception e) {
	            e.printStackTrace();
	        }
	        
		 
		 return mv;
	 }
}