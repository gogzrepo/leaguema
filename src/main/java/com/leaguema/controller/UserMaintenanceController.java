/**
 * 
 */
package com.leaguema.controller;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.hibernate.service.spi.InjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import javax.validation.Valid;

import com.leaguema.model.BMUser;
import com.leaguema.service.IUserService;

/**
 * @author Sunil
 *
 */
@Controller
public class UserMaintenanceController {

	/**
	 * 
	 */
	@Autowired
	@Qualifier("userServiceImpl")
	IUserService service;
	
	public UserMaintenanceController() {
		// TODO Auto-generated constructor stub
	}
	
	
	@RequestMapping(value = "user/createUser")
	public ModelAndView openUserMaintenancePage(@ModelAttribute BMUser userForm) {
		ModelAndView mv = new ModelAndView("user/user");
		mv.addObject("stuRegForm", userForm);
		return mv;
	}
	
	
	@RequestMapping(value = "/user/userMaintenanceAction", method = RequestMethod.POST)
	public ModelAndView createUser(@Valid @ModelAttribute("stuRegForm") BMUser userForm, BindingResult result) {
		ModelAndView mv = new ModelAndView("/user/user");
		if(result.hasErrors()) {
			mv.addObject("msg", "Entered Data is incomplete or incorrect, please check and try again!!!");
			return mv;
		}
		BMUser savedUser = service.createUser(userForm);
		if(savedUser !=null && savedUser.getUserUid() > 0)
			mv.addObject("msg", "User created successfully!!!");
		else
			mv.addObject("msg", "Entered Data is incorrect, please check and try again!!!");
		return mv;
		
	}
	
	
	/**
	 * Below method for handling exception on spring page
	 * If any exception occurred then it forward to errorPage
	 * ***/
	/*@ExceptionHandler(value = Exception.class)
	public String handleException(Exception e)
	{
		return "errorPage";
	}*/
	
	
	///////////////////////////////////////////////////////
	/**
	 * Below method used for creating custom validation on date field in spring form
	 * 
	 * ***/
	@InitBinder
	public void initBinder(final WebDataBinder binder){
	  final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd"); 
	  binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
	}

}
