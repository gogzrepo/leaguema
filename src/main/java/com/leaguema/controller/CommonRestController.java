/**
 * 
 */
package com.leaguema.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author Sunil
 *
 */
@RestController
public class CommonRestController {
	
	@RequestMapping(value = { "/cmnurl/openMessagePage" }, method = RequestMethod.GET)
	public ModelAndView openCommonMessagePage(@RequestParam String paramValue, @RequestParam String operationType) throws Exception {
		ModelAndView mv = new ModelAndView("commonMessagePage");
		mv.addObject("operationType", operationType);
		if(operationType.equalsIgnoreCase("ClubRegistration_CreateEdit") || operationType.equalsIgnoreCase("ClubRegistration_Create")) {
			mv.addObject("commonMessage", "Club Registered Successfully and your club code is:  "+paramValue.split("~")[1]);
		}
		else if(operationType.equalsIgnoreCase("ClubRegistration_UpdateEdit")) {
			mv.addObject("commonMessage", "Club Updated Successfully and your club code is:  "+paramValue.split("~")[1]);
		}
		else if(operationType.equalsIgnoreCase("PlayerRegistration_CreateEdit") || operationType.equalsIgnoreCase("PlayerRegistration_Create")) {
			mv.addObject("commonMessage", "Player Registered Successfully and your player ID is:  "+paramValue.split("~")[1]);
		}
		else if(operationType.equalsIgnoreCase("PlayerRegistration_UpdateEdit")) {
			mv.addObject("commonMessage", "Player Updated Successfully and your player ID is:  "+paramValue.split("~")[1]);
		}
		else if(operationType.equalsIgnoreCase("ClubRegistration_active") || operationType.equalsIgnoreCase("ClubRegistration_activate")) {
			mv.addObject("commonMessage", "Club Activated Successfully and your club code is:  "+paramValue.split("~")[1]);
		}
		else if(operationType.equalsIgnoreCase("ClubRegistration_inactive") || operationType.equalsIgnoreCase("ClubRegistration_inactivate")) {
			mv.addObject("commonMessage", "Club Deactivated Successfully and your club code is:  "+paramValue.split("~")[1]);
		}
		else if(operationType.equalsIgnoreCase("PlayerRegistration_active") || operationType.equalsIgnoreCase("PlayerRegistration_activate")) {
			mv.addObject("commonMessage", "Player Activated Successfully and your player code is:  "+paramValue.split("~")[1]);
		}
		else if(operationType.equalsIgnoreCase("PlayerRegistration_inactive") || operationType.equalsIgnoreCase("PlayerRegistration_inactivate")) {
			mv.addObject("commonMessage", "Player Deactivated Successfully and your player code is:  "+paramValue.split("~")[1]);
		}
		return mv;
	}
	@RequestMapping(value = { "/cmnurl/openBarPage" }, method = RequestMethod.GET)
	public ModelAndView openBarChart() throws Exception {
		ModelAndView mv = new ModelAndView("Bar");
		return mv;
		
	}
}
