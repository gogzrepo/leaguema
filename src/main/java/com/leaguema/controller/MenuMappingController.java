package com.leaguema.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class MenuMappingController {
	
	
	@RequestMapping(value = "/cmnurl/openMenuPage")
	public String OpenMenuPage(@RequestParam("menuNext") String param,HttpServletRequest request) {
		HttpSession session = request.getSession();
		System.out.println("param==>"+param);
		String actionPath = "";
		if(param != null) {
			System.out.println("menu next is-->");
			session.setAttribute("menuNext", param);
			actionPath = getActionPath(Integer.parseInt(param));
		}
		return "redirect:"+actionPath;
	}
	
	private String getActionPath(int menuNext) {
		String actionPath = "";
		switch(menuNext) {
		case 1 :
			actionPath = "/user/createUser";
			break;
		case 2 :
			actionPath = "/user/editUser";
			break;
		}
		return actionPath;
	}

}
