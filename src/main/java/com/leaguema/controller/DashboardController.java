/**
 * 
 */
package com.leaguema.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.leaguema.data.ChartData;
import com.leaguema.data.Datasets;
import com.leaguema.model.BMUser;
import com.leaguema.model.ClubRegistration;
import com.leaguema.model.DashboardData;
import com.leaguema.model.PlayerRegistration;
import com.leaguema.service.IDashboardDataService;

/**
 * @author Sunil
 *
 */

@RestController
public class DashboardController {
	
	@Autowired
	IDashboardDataService dashboardDataService;
	
	@RequestMapping(value = "/cmnurl/openDashboard", method = RequestMethod.GET)
	private ModelAndView openDashboard(HttpServletRequest request) {
		ModelAndView mv = new ModelAndView("dashboard");
		mv.addObject("title", "Dashboard");
		mv.addObject("pageHeading", "Welcome to Dashboard");
		
		
		
		return mv;
	}
	
	@RequestMapping(value = "/cmnurl/loadDashboard", method = RequestMethod.GET, produces =MediaType.APPLICATION_JSON_VALUE)
	private @ResponseBody DashboardData loadDashboardData(HttpServletRequest request, ModelMap model)throws Exception {
		ModelAndView mv = new ModelAndView("dashboard");
		mv.addObject("title", "Dashboard");
		mv.addObject("pageHeading", "Welcome to Dashboard");
		///////////////////////
		BMUser user = null;
		System.out.println("-->"+request.getUserPrincipal());
		if(request.getSession().getAttribute("LoggedUserData") != null && request.getUserPrincipal().getName() != null) {
			user = (BMUser)request.getSession().getAttribute("LoggedUserData");
		}
		//passed parameter as logged user id
		DashboardData dashboardData =  dashboardDataService.loadDashboardData(user.getUserClubRegitrationId());
		model.addAttribute("listClubData", dashboardData.getListClubRegistration());
		//////////////////////
		
		return dashboardData;
	}
	
	@RequestMapping(value = "/cmnurl/loadChartData", method = RequestMethod.GET, produces =MediaType.APPLICATION_JSON_VALUE)
	private @ResponseBody ChartData loadChartData(HttpServletRequest request)throws Exception {
		ModelAndView mv = new ModelAndView("dashboard");
		mv.addObject("title", "Dashboard");
		mv.addObject("pageHeading", "Welcome to Dashboard");
		///////////////////////
		BMUser user = null;
		System.out.println("-->"+request.getUserPrincipal());
		if(request.getSession().getAttribute("LoggedUserData") != null && request.getUserPrincipal().getName() != null) {
			user = (BMUser)request.getSession().getAttribute("LoggedUserData");
		}
		//passed parameter as logged user id
		DashboardData dashboardData =  dashboardDataService.loadDashboardData(user.getUserClubRegitrationId());
		//////////////////////
		ChartData chartData = new ChartData();
		Datasets dataSet = new Datasets();
		dataSet.setLabel("Club Chart");
		dataSet.setFillColor("rgba(151,187,205,0.2)");
		dataSet.setStrokeColor("rgba(151,187,205,19)");
		dataSet.setPointColor("rgba(151,187,205,15)");
		dataSet.setPointStrokeColor("rgba(151,187,205,15)");
		dataSet.setPointHighlightFill("rgba(151,187,205,15)");
		dataSet.setPointHighlightStroke("rgba(151,187,205,13)");
		int size = dashboardData.getListClubRegistration().size();
		String data[] = new String[size];
		String labels[] = new String[size];
		int index = 0;
		for(ClubRegistration cr: dashboardData.getListClubRegistration()) {
			data[index] = cr.getPlayerRegistration().size()+"";
			labels[index] = cr.getClubCode();
			index++;
		}
		
		dataSet.setData(data);
		Datasets dataSets[] = new Datasets[1];
		dataSets[0] = dataSet;
				
		chartData.setDatasets(dataSets);
		chartData.setLabels(labels);
		
		return chartData;
	}
	
	
	@RequestMapping(value = "/testing/loadDashboard", method = RequestMethod.GET, produces =MediaType.APPLICATION_JSON_VALUE)
	private @ResponseBody List<ClubRegistration> testing()throws Exception {
		ModelAndView mv = new ModelAndView("dashboard");
		mv.addObject("title", "Dashboard");
		mv.addObject("pageHeading", "Welcome to Dashboard");
		///////////////////////
		
		//passed parameter as logged user id
		DashboardData dashboardData =  dashboardDataService.loadDashboardData(1);
		//model.addAttribute("listClubData", dashboardData.getListClubRegistration());
		//////////////////////
		return dashboardData.getListClubRegistration();
	}

}
