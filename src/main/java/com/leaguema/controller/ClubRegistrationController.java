/**
 * 
 */
package com.leaguema.controller;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.leaguema.model.BMUser;
import com.leaguema.model.ClubRegistration;
import com.leaguema.model.PlayerRegistration;
import com.leaguema.service.ClubRegistrationService;

/**
 * @author Sunil
 *
 */
@RestController
public class ClubRegistrationController {

	@Autowired
	private ClubRegistrationService clubRegistrationService;

	@RequestMapping(value = { "/club/clubRegistration" }, method = RequestMethod.GET)
	public ModelAndView defaultPage(HttpServletRequest request) throws SQLException {
		ModelAndView mv = new ModelAndView("clubRegistration");
		mv.addObject("operationType", "create");
		mv.addObject("pageHeading", "Club Registration");
		return mv;
	}

	@RequestMapping(value = { "/club/saveClubRegistration" }, method = RequestMethod.POST)
	public String saveClubRegistrationPage(HttpServletRequest request, @RequestBody ClubRegistration clubRegistration,
			@RequestParam("operationType") String operationType) {
		String resultValue = "failure";
		/******/
		BMUser user = null;
		long loggedClubId = 0;
		String loogedClubCode = "";
		try {
			if (request.getSession().getAttribute("LoggedUserData") != null
					&& request.getUserPrincipal().getName() != null) {
				user = (BMUser) request.getSession().getAttribute("LoggedUserData");
				loggedClubId = user.getUserClubRegitrationId();
				loogedClubCode = user.getUserEmpCode();// for clubCode
			}
			if (operationType.equalsIgnoreCase("active") || operationType.equalsIgnoreCase("activate")
					|| operationType.equalsIgnoreCase("inactive") || operationType.equalsIgnoreCase("inactivate")) {
				resultValue = clubRegistration.getId() + "";
			} else {
				if (clubRegistration.getClubCode() != null) {
					resultValue = clubRegistrationService.getClubAlreadyRegistered(clubRegistration, true,
							operationType);

					if (resultValue.equalsIgnoreCase("success")) {
						resultValue = clubRegistrationService.saveClubRegistrationData(clubRegistration, user,
								operationType);
					}
				} else {
					resultValue = clubRegistrationService.saveClubRegistrationData(clubRegistration, user,
							operationType);
				}
			}
		} catch (Exception ex) {
			System.out.println("Error occurred while inserting data into temp table.");
			ex.printStackTrace();
			resultValue = ex.getMessage();
		}
		/******/
		return resultValue;
	}

	@RequestMapping(value = { "/club/verifyRegistration" }, method = RequestMethod.GET)
	public ModelAndView openVerifyRegistrationPage(@RequestParam String regId, @RequestParam String operationType)
			throws Exception {
		ModelAndView mv = new ModelAndView("verifyRegistration");
		mv.addObject("operationType", operationType);
		if (operationType.equalsIgnoreCase("updateEdit")) {
			mv.addObject("pageHeading", "Verify Club Updated Data");
		} else if (operationType.equalsIgnoreCase("active") || operationType.equalsIgnoreCase("activate")
				|| operationType.equalsIgnoreCase("inactive") || operationType.equalsIgnoreCase("inactivate")) {
			if (operationType.equalsIgnoreCase("active") || operationType.equalsIgnoreCase("activate"))
				mv.addObject("pageHeading", "Verify Active Registration Data");
			if (operationType.equalsIgnoreCase("inactive") || operationType.equalsIgnoreCase("inactivate"))
				mv.addObject("pageHeading", "Verify Inactive Registration Data");
		} else {
			mv.addObject("pageHeading", "Verify Registration Data");
		}
		/*********/
		ClubRegistration clubRegistration = clubRegistrationService.getRegistrationData(regId, operationType);
		if (clubRegistration != null) {
			mv.addObject("registrationData", clubRegistration);
		} else {
			mv.addObject("message", "Incorrect request, please verify correct data.");
		}
		/********/

		return mv;
	}

	@RequestMapping(value = { "/club/editClubRegistration" }, method = RequestMethod.GET)
	public ModelAndView editClubRegistrationPage(HttpServletRequest request, @RequestParam String regId,
			@RequestParam String operationType) throws Exception {
		ModelAndView mv = new ModelAndView("clubRegistration");
		mv.addObject("operationType", operationType);
		if (operationType.equalsIgnoreCase("updateEdit")) {
			mv.addObject("pageHeading", "Edit Registration Data");
		} else if (operationType.equalsIgnoreCase("create") || operationType.equalsIgnoreCase("createEdit")) {
			mv.addObject("pageHeading", "Create->Edit Registration Data");
		} else if (operationType.equalsIgnoreCase("active") || operationType.equalsIgnoreCase("activate")) {
			mv.addObject("pageHeading", "Active Registration Data");
		} else if (operationType.equalsIgnoreCase("inactive") || operationType.equalsIgnoreCase("inactivate")) {
			mv.addObject("pageHeading", "Inactive Registration Data");
		}
		/*********/
		ClubRegistration clubRegistration = clubRegistrationService.getRegistrationData(regId, operationType);
		if (clubRegistration != null) {
			mv.addObject("registrationData", clubRegistration);
			request.setAttribute("regData", clubRegistration);
		}
		/********/

		return mv;
	}

	@RequestMapping(value = { "/club/openClubUpdatePage" }, method = RequestMethod.GET)
	public ModelAndView openClubUpdatePage(HttpServletRequest request) throws Exception {
		ModelAndView mv = new ModelAndView("updateClubPage");
		mv.addObject("pageHeading", "Club Updation");
		mv.addObject("title", "Club Updation");
		mv.addObject("operationType", "update");
		/*********/

		List<ClubRegistration> listClub = new ArrayList<>();

		listClub = clubRegistrationService.getAllActiveClub("active");

		if (listClub != null) {
			mv.addObject("AllClub", listClub);
			request.setAttribute("AllClub", listClub);
		}
		/********/

		return mv;
	}

	@RequestMapping(value = { "/club/saveVerifyRegistrationData" }, method = RequestMethod.POST)
	public String verifyRegistrationData(@RequestParam String regId, @RequestParam String operationType)
			throws Exception {
		/*********/
		String response = clubRegistrationService.saveVerifyRegistrationData(regId, operationType);
		/********/
		return response;
	}

	@RequestMapping(value = { "/club/openClubActiveInactivePage" }, method = RequestMethod.GET)
	public ModelAndView openClubActiveInactivePage(HttpServletRequest request) throws Exception {
		ModelAndView mv = new ModelAndView("updateClubPage");
		mv.addObject("pageHeading", "Active/Inactive Club");
		mv.addObject("title", "Active/Inactive Club");
		mv.addObject("operationType", "active_inactive");
		/*********/

		List<ClubRegistration> listClub = new ArrayList<>();

		listClub = clubRegistrationService.getAllActiveClub("all");

		if (listClub != null) {
			mv.addObject("AllClub", listClub);
			request.setAttribute("AllClub", listClub);
		}
		/********/

		return mv;
	}

}
