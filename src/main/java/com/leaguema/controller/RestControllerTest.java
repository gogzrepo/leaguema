/**
 * 
 */
package com.leaguema.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.leaguema.bean.Address;
import com.leaguema.bean.Employee;
import com.leaguema.service.ICommonService;

/**
 * @author Sunil
 *
 */
@Controller
public class RestControllerTest {
	
	@RequestMapping(value = "/restApi/getResponseBodyEmployeeData/{empname}", headers="Accept=application/json")
	public @ResponseBody Employee getResponseBodyEmployeeData(@PathVariable("empname") String empname) {
		Employee emp = new Employee();
		if(empname.equalsIgnoreCase("Sunil")) {
		emp.setName("Sunil");
		Address address = new Address();
		address.setCity("Seohara");
		address.setCountry("INDIA");
		address.setState("UP");
		address.setZipcode(246746);
		emp.setAddress(address);
		emp.setAge(27);
		}
		else if (empname.equalsIgnoreCase("Rohit")) {
			emp.setName("Rohit");
			Address address = new Address();
			address.setCity("Agra");
			address.setCountry("INDIA");
			address.setState("UP");
			address.setZipcode(246716);
			emp.setAddress(address);
			emp.setAge(27);
		}
		return emp;
	}
	
	//Below method is example of path variavle
	@RequestMapping(value = "/restApi/getResponseBodyEmployeeDataResponseEntity/{empname}", produces= MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Employee> getResponseBodyEmployeeDataResponseEntity(@PathVariable("empname") String empname) {
		Employee emp = new Employee();
		if(empname.equalsIgnoreCase("Sunil")) {
		emp.setName("Sunil");
		Address address = new Address();
		address.setCity("Seohara");
		address.setCountry("INDIA");
		address.setState("UP");
		address.setZipcode(246746);
		emp.setAddress(address);
		emp.setAge(27);
		}
		else if (empname.equalsIgnoreCase("Rohit")) {
			emp.setName("Rohit");
			Address address = new Address();
			address.setCity("Agra");
			address.setCountry("INDIA");
			address.setState("UP");
			address.setZipcode(246716);
			emp.setAddress(address);
			emp.setAge(27);
		}
		return new ResponseEntity<Employee>(emp, HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/restApi/getRequestBodyResponseEntityData", method = RequestMethod.POST, headers="Accept=application/json")
	public @ResponseBody Employee getRequestBodyResponseEntityData(@RequestBody Employee emp) {
		if(emp!=null) {
			///save
			System.out.println("Data Saved successfully!!!");
			emp.setName("Success");
		}
		return emp;
	}
	
	/*{
	    "name": "Sunil",
	    "age": 27,
	    "address": {
	        "city": "Seohara",
	        "state": "UP",
	        "country": "INDIA",
	        "zipcode": 246746
	    }
	}*/
	
	
	@Autowired
	ICommonService commonService;
	
	@RequestMapping(value = "/restApi/updateMergeTest", produces= MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody String updateMergeTest() {
		commonService.updateMergeTest();
		return "";
	}
	
	
	@RequestMapping(value = "/restApi/savePersistTest", produces= MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody String savePersistTest() {
		commonService.savePersistTest();
		return "";
	}
	
	@RequestMapping(value = "/restApi/hibernateCriteriaTest", produces= MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody String hibernateCriteriaTest() {
		commonService.CriteriaTest();
		return "";
	}
	
	@RequestMapping(value = "/restApi/hibernateHOLJoinTest", produces= MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody String hibernateHOLJoinTest() {
		commonService.HibernateHOLJoinTest();
		return "";
	}
	
	@RequestMapping(value = "/restApi/PathVariableTest/{empname}", produces= MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Employee> PathVariableTest(@PathVariable("empname") String empname) {
		Employee emp = new Employee();
		if(empname.equalsIgnoreCase("Sunil")) {
		emp.setName("PathVariable");
		Address address = new Address();
		address.setCity("Seohara");
		address.setCountry("INDIA");
		address.setState("UP");
		address.setZipcode(246746);
		emp.setAddress(address);
		emp.setAge(27);
		}
		else if (empname.equalsIgnoreCase("Rohit")) {
			emp.setName("Rohit");
			Address address = new Address();
			address.setCity("Agra");
			address.setCountry("INDIA");
			address.setState("UP");
			address.setZipcode(246716);
			emp.setAddress(address);
			emp.setAge(27);
		}
		return new ResponseEntity<Employee>(emp, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/restApi/RequestParamTest/{empname}", produces= MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Employee> RequestParamTest(@PathVariable("empname") String empname, @RequestParam("id") String id) {
		Employee emp = new Employee();
		if(id.equalsIgnoreCase("Sunil")) {
		emp.setName(id);
		Address address = new Address();
		address.setCity("Seohara");
		address.setCountry("INDIA");
		address.setState("UP");
		address.setZipcode(246746);
		emp.setAddress(address);
		emp.setAge(27);
		}
		else if (empname.equalsIgnoreCase("Rohit")) {
			emp.setName("Rohit");
			Address address = new Address();
			address.setCity("Agra");
			address.setCountry("INDIA");
			address.setState("UP");
			address.setZipcode(246716);
			emp.setAddress(address);
			emp.setAge(27);
		}
		return new ResponseEntity<Employee>(emp, HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/restApi/ConsumeRequestParamTest/", produces= MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
	public @ResponseBody Employee ConsumeRequestParamTest(@RequestBody Employee emp) {

		emp.setName("testing");
		Address address = new Address();
		address.setCity("Seohara");
		address.setCountry("INDIA");
		address.setState("UP");
		address.setZipcode(246746);
		emp.setAddress(address);
		emp.setAge(27);
		return emp;
	}
	

}
