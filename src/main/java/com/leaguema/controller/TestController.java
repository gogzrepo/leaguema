package com.leaguema.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.leaguema.service.ICommonService;

@Controller
public class TestController {

	@Autowired
	ICommonService commonService;
	
	
	@RequestMapping(value = "testPage", method = RequestMethod.GET)
	public String openTestPage() {
		return "testPage";
	}
	
	@RequestMapping(value = "CriteriaTest", method = RequestMethod.GET)
	public String CriteriaTest() {
		commonService.CriteriaTest();
		System.out.println("success");
		return "testPage";
	}
	
}
