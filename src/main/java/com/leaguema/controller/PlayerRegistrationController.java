/**
 * 
 */
package com.leaguema.controller;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.leaguema.model.BMUser;
import com.leaguema.model.ClubRegistration;
import com.leaguema.model.PlayerRegistration;
import com.leaguema.model.ShiftMst;
import com.leaguema.service.IPlayerRegistrationService;
import com.leaguema.service.IShiftService;

/**
 * @author Sunil
 *
 */

@RestController
public class PlayerRegistrationController {

	@Autowired
	IPlayerRegistrationService playerRegistrationService;
	
	@Autowired
	IShiftService shiftService;

	@RequestMapping(value = { "/player/playerRegistration" }, method = RequestMethod.GET)
	public ModelAndView openPlayerRegistration(HttpServletRequest request) throws SQLException {
		ModelAndView mv = new ModelAndView("playerRegistration");
		mv.addObject("operationType", "create");
		mv.addObject("pageHeading", "Player Registration");
		return mv;
	}

	@RequestMapping(value = { "/player/savePlayerRegistration" }, method = RequestMethod.POST)
	public String savePlayerRegistration(HttpServletRequest request, @RequestBody PlayerRegistration playerRegistration,
			@RequestParam String operationType) {
		String resultValue = "failure";
		/******/
		BMUser user = null;
		long loggedClubId = 0;
		String clubNo = "0";
		String CVN = "0";
		try {
			if (request.getSession().getAttribute("LoggedUserData") != null
					&& request.getUserPrincipal().getName() != null) {
				user = (BMUser) request.getSession().getAttribute("LoggedUserData");
				loggedClubId = user.getUserClubRegitrationId();
				clubNo = user.getUserEmpCode();
				CVN = user.getUserOTP();
			}
			if (operationType.equalsIgnoreCase("active") || operationType.equalsIgnoreCase("activate")
					|| operationType.equalsIgnoreCase("inactive") || operationType.equalsIgnoreCase("inactivate")) {
				resultValue = playerRegistration.getId() + "";
			} else {
				if (playerRegistration.getPlayerID() != null) {
					resultValue = playerRegistrationService.getPlayerAlreadyRegistered(playerRegistration, true,
							operationType);

					if (resultValue.equalsIgnoreCase("success")) {
						resultValue = playerRegistrationService.savePlayerRegistrationData(playerRegistration,
								loggedClubId, clubNo, operationType, CVN);
					}
				} else {
					resultValue = playerRegistrationService.savePlayerRegistrationData(playerRegistration, loggedClubId,
							clubNo, operationType, CVN);
				}
			}
		} catch (Exception ex) {
			System.out.println("Error occurred while inserting data into temp table.");
			ex.printStackTrace();
			resultValue = ex.getMessage();
		}
		/******/
		return resultValue;
	}

	@RequestMapping(value = { "/player/verifyPlayerRegistration" }, method = RequestMethod.GET)
	public ModelAndView openVerifyRegistrationPage(@RequestParam String regId, @RequestParam String operationType)
			throws Exception {
		ModelAndView mv = new ModelAndView("verifyPlayerRegistration");
		mv.addObject("operationType", operationType);
		if (operationType.equalsIgnoreCase("updateEdit")) {
			mv.addObject("pageHeading", "Verify Player Updated Data");
		} 
		else if (operationType.equalsIgnoreCase("active") || operationType.equalsIgnoreCase("activate")
				|| operationType.equalsIgnoreCase("inactive") || operationType.equalsIgnoreCase("inactivate")) {
			if (operationType.equalsIgnoreCase("active") || operationType.equalsIgnoreCase("activate"))
				mv.addObject("pageHeading", "Verify Active Registration Data");
			if (operationType.equalsIgnoreCase("inactive") || operationType.equalsIgnoreCase("inactivate"))
				mv.addObject("pageHeading", "Verify Inactive Registration Data");
		}
		else {
			mv.addObject("pageHeading", "Verify Player Registration Data");
		}
		/*********/
		PlayerRegistration playerRegistration = playerRegistrationService.getRegistrationData(regId, operationType);
		if (playerRegistration != null) {
			mv.addObject("registrationData", playerRegistration);
		} else {
			mv.addObject("message", "Incorrect request, please verify correct data.");
		}
		/********/

		return mv;
	}

	@RequestMapping(value = { "/player/editPlayerRegistration" }, method = RequestMethod.GET)
	public ModelAndView editPlayerRegistrationPage(HttpServletRequest request, @RequestParam String regId,
			@RequestParam String operationType) throws Exception {
		ModelAndView mv = new ModelAndView("playerRegistration");
		mv.addObject("operationType", operationType);
		if (operationType.equalsIgnoreCase("updateEdit")) {
			mv.addObject("pageHeading", "Edit Player Data");
		} else if (operationType.equalsIgnoreCase("create") || operationType.equalsIgnoreCase("createEdit")) {
			mv.addObject("pageHeading", "Create->Edit Registration Data");
		} else if (operationType.equalsIgnoreCase("active") || operationType.equalsIgnoreCase("activate")) {
			mv.addObject("pageHeading", "Active Registration Data");
		} else if (operationType.equalsIgnoreCase("inactive") || operationType.equalsIgnoreCase("inactivate")) {
			mv.addObject("pageHeading", "Inactive Registration Data");
		}
		/*********/
		PlayerRegistration playerRegistration = playerRegistrationService.getRegistrationData(regId, operationType);
		if (playerRegistration != null) {
			mv.addObject("registrationData", playerRegistration);
			request.setAttribute("regData", playerRegistration);
		}
		/********/

		return mv;
	}

	@RequestMapping(value = { "/player/openPlayerUpdatePage" }, method = RequestMethod.GET)
	public ModelAndView openClubUpdatePage(HttpServletRequest request) throws Exception {
		ModelAndView mv = new ModelAndView("updatePlayerPage");
		mv.addObject("pageHeading", "Player Updation");
		mv.addObject("title", "Player Updation");
		mv.addObject("operationType", "update");
		/*********/

		List<PlayerRegistration> listPlayer = new ArrayList<>();

		listPlayer = playerRegistrationService.getAllPlayer("active");

		if (listPlayer != null) {
			mv.addObject("AllPlayer", listPlayer);
			request.setAttribute("AllPlayer", listPlayer);
		}
		/********/

		return mv;
	}

	@RequestMapping(value = { "/player/openPlayerActiveInactivePage" }, method = RequestMethod.GET)
	public ModelAndView openPlayerActiveInactivePage(HttpServletRequest request) throws Exception {
		ModelAndView mv = new ModelAndView("updatePlayerPage");
		mv.addObject("pageHeading", "Active/Inactive Player");
		mv.addObject("title", "Active/Inactive Player");
		mv.addObject("operationType", "active_inactive");
		/*********/

		List<PlayerRegistration> listPlayer = new ArrayList<>();

		listPlayer = playerRegistrationService.getAllPlayer("all");

		if (listPlayer != null) {
			mv.addObject("AllPlayer", listPlayer);
			request.setAttribute("AllPlayer", listPlayer);
		}
		return mv;

	}

	@RequestMapping(value = { "/player/saveVerifyPlayerRegistrationData" }, method = RequestMethod.POST)
	public String savePerifiedPlayerRegistrationData(@RequestParam String regId, String operationType)
			throws Exception {
		/*********/
		String response = playerRegistrationService.saveVerifiedPlayerRegistrationData(regId, operationType);
		/********/
		return response;
	}
	
	
	@ModelAttribute("allActiveShift")
	public List<ShiftMst> allActiveShift(HttpServletRequest request) {
		
		BMUser user = null;
		long loggedClubId = 0;
		List<ShiftMst> listAllActiveShift = new ArrayList<>();
		if (request.getSession().getAttribute("LoggedUserData") != null
				&& request.getUserPrincipal().getName() != null) {
			user = (BMUser) request.getSession().getAttribute("LoggedUserData");
			loggedClubId = user.getUserClubRegitrationId();
			listAllActiveShift = shiftService.getAllActiveShift("active", loggedClubId+"");
		}
		
		return listAllActiveShift;
	}

}
