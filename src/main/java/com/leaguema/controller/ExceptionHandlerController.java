/**
 * 
 */
package com.leaguema.controller;

import javax.servlet.http.HttpServletRequest;

import org.hibernate.exception.DataException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author Sunil
 *
 */
@ControllerAdvice
public class ExceptionHandlerController {
	
	/**
	 * Below method for handling exception on spring page
	 * If any exception occurred then it forward to errorPage
	 * ***/
	@ExceptionHandler(value = Exception.class)
	public ModelAndView handleException(Exception e,HttpServletRequest request)
	{
		ModelAndView mv = new ModelAndView("errorPage");
		System.out.println("requested path->"+request.getRequestURI());
		String requestedPath = request.getRequestURL().toString();
		if(requestedPath!=null) {
			if(requestedPath.contains("/user/"))
			requestedPath = "/user/";
		}
		mv.addObject("requestedPath", requestedPath);
		mv.addObject("errorDetail", e.getStackTrace());
		DataException dae = null;
		if(e instanceof DataException) {
			dae = (DataException) e;
			mv.addObject("errorMessage", dae.getSQLException());
		}
		System.out.println("dae-->"+dae);
		return mv;
	}
	

}
