/**
 * 
 */
package com.leaguema.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author Sunil
 *
 */
@Entity
@Table(name = "address")
public class Address {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "addr_id")
	private long addrId;
	
	@Column(name = "city")
	private String city;
	
	@Column(name = "zipcode")
	private int zipcode;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "emp_id", nullable = false)
	private Employee emp;

	public Address() {
		
	}
	
	/**
	 * @param addrId
	 * @param city
	 * @param zipcode
	 * @param employee
	 */
	public Address(String city, int zipcode) {
		this.city = city;
		this.zipcode = zipcode;
	}

	/**
	 * @return the addrId
	 */
	public long getAddrId() {
		return addrId;
	}

	/**
	 * @param addrId the addrId to set
	 */
	public void setAddrId(long addrId) {
		this.addrId = addrId;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the zipcode
	 */
	public int getZipcode() {
		return zipcode;
	}

	/**
	 * @param zipcode the zipcode to set
	 */
	public void setZipcode(int zipcode) {
		this.zipcode = zipcode;
	}

	/**
	 * @return the employee
	 */
	public Employee getEmp() {
		return emp;
	}

	/**
	 * @param employee the employee to set
	 */
	public void setEmp(Employee emp) {
		this.emp = emp;
	}
	
	
	

}
