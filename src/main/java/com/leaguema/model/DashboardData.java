/**
 * 
 */
package com.leaguema.model;

import java.util.List;

/**
 * @author Sunil
 *
 */
public class DashboardData {
	
	private int totalClubs = 0;
	
	private int totalPlayers = 0;
	
	private int totalCoaches = 0;
	
	private int totalEvents = 0;
	
	private List<ClubRegistration> listClubRegistration;
	
	/**
	 * @return the totalClubs
	 */
	public int getTotalClubs() {
		return totalClubs;
	}

	/**
	 * @param totalClubs the totalClubs to set
	 */
	public void setTotalClubs(int totalClubs) {
		this.totalClubs = totalClubs;
	}

	/**
	 * @return the totalPlayers
	 */
	public int getTotalPlayers() {
		return totalPlayers;
	}

	/**
	 * @param totalPlayers the totalPlayers to set
	 */
	public void setTotalPlayers(int totalPlayers) {
		this.totalPlayers = totalPlayers;
	}

	/**
	 * @return the totalCoaches
	 */
	public int getTotalCoaches() {
		return totalCoaches;
	}

	/**
	 * @param totalCoaches the totalCoaches to set
	 */
	public void setTotalCoaches(int totalCoaches) {
		this.totalCoaches = totalCoaches;
	}

	/**
	 * @return the totalEvents
	 */
	public int getTotalEvents() {
		return totalEvents;
	}

	/**
	 * @param totalEvents the totalEvents to set
	 */
	public void setTotalEvents(int totalEvents) {
		this.totalEvents = totalEvents;
	}

	/**
	 * @return the listClubRegistration
	 */
	public List<ClubRegistration> getListClubRegistration() {
		return listClubRegistration;
	}

	/**
	 * @param listClubRegistration the listClubRegistration to set
	 */
	public void setListClubRegistration(List<ClubRegistration> listClubRegistration) {
		this.listClubRegistration = listClubRegistration;
	}
	
}
