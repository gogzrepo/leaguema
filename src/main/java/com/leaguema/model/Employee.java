/**
 * 
 */
package com.leaguema.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @author Sunil
 *
 */

@Entity
@Table(name = "emp")
public class Employee {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "emp_id")
	private long empId;
	
	@Column(name = "emp_name")
	private String empName;
	
	@Column(name = "emp_address")
	private String empAddress;
	
	
	@Column(name = "emp_age")
	private String empAge;

	@OneToMany(mappedBy = "emp",fetch = FetchType.LAZY, orphanRemoval = true, cascade = CascadeType.ALL)
	private Set<Address> address;
	
	public Employee() {
		
	}
	

	/**
	 * @param empId
	 * @param empName
	 * @param empAddress
	 * @param empAge
	 */
	public Employee(String empName, String empAddress, String empAge, Set<Address> address) {
		this.empName = empName;
		this.empAddress = empAddress;
		this.empAge = empAge;
		this.address = address;
	}


	/**
	 * @return the empId
	 */
	public long getEmpId() {
		return empId;
	}


	/**
	 * @param empId the empId to set
	 */
	public void setEmpId(long empId) {
		this.empId = empId;
	}


	/**
	 * @return the empName
	 */
	public String getEmpName() {
		return empName;
	}


	/**
	 * @param empName the empName to set
	 */
	public void setEmpName(String empName) {
		this.empName = empName;
	}


	/**
	 * @return the empAddress
	 */
	public String getEmpAddress() {
		return empAddress;
	}


	/**
	 * @param empAddress the empAddress to set
	 */
	public void setEmpAddress(String empAddress) {
		this.empAddress = empAddress;
	}


	/**
	 * @return the empAge
	 */
	public String getEmpAge() {
		return empAge;
	}


	/**
	 * @param empAge the empAge to set
	 */
	public void setEmpAge(String empAge) {
		this.empAge = empAge;
	}


	/**
	 * @return the address
	 */
	public Set<Address> getAddress() {
		return address;
	}


	/**
	 * @param address the address to set
	 */
	public void setAddress(Set<Address> address) {
		this.address = address;
	}
	

}
