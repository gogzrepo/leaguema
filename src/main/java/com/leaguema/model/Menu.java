/**
 * 
 */
package com.leaguema.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Sunil
 *
 */

@Entity
@Table(name = "tbl_menu")
public class Menu {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "menu_uid")
	private int menuUid;
	
	@Column(name = "menu_fname")
	private String menuFName;
	
	@Column(name = "menu_sname")
	private String menuSName;
	
	@Column(name = "menu_next")
	private String menuNext;
	
	@Column(name = "menu_parentid")
	private String menuParentId;
	
	@Column(name = "menu_index")
	private String menuIndex;
	
	@Column(name = "menu_status")
	private String menuStatus;

	/**
	 * @return the menuUid
	 */
	public int getMenuUid() {
		return menuUid;
	}

	/**
	 * @param menuUid the menuUid to set
	 */
	public void setMenuUid(int menuUid) {
		this.menuUid = menuUid;
	}

	/**
	 * @return the menuFName
	 */
	public String getMenuFName() {
		return menuFName;
	}

	/**
	 * @param menuFName the menuFName to set
	 */
	public void setMenuFName(String menuFName) {
		this.menuFName = menuFName;
	}

	/**
	 * @return the menuSName
	 */
	public String getMenuSName() {
		return menuSName;
	}

	/**
	 * @param menuSName the menuSName to set
	 */
	public void setMenuSName(String menuSName) {
		this.menuSName = menuSName;
	}

	/**
	 * @return the menuNext
	 */
	public String getMenuNext() {
		return menuNext;
	}

	/**
	 * @param menuNext the menuNext to set
	 */
	public void setMenuNext(String menuNext) {
		this.menuNext = menuNext;
	}

	/**
	 * @return the menuParentId
	 */
	public String getMenuParentId() {
		return menuParentId;
	}

	/**
	 * @param menuParentId the menuParentId to set
	 */
	public void setMenuParentId(String menuParentId) {
		this.menuParentId = menuParentId;
	}

	/**
	 * @return the menuIndex
	 */
	public String getMenuIndex() {
		return menuIndex;
	}

	/**
	 * @param menuIndex the menuIndex to set
	 */
	public void setMenuIndex(String menuIndex) {
		this.menuIndex = menuIndex;
	}

	/**
	 * @return the menuStatus
	 */
	public String getMenuStatus() {
		return menuStatus;
	}

	/**
	 * @param menuStatus the menuStatus to set
	 */
	public void setMenuStatus(String menuStatus) {
		this.menuStatus = menuStatus;
	}
	
}
