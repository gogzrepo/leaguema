package com.leaguema.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_shiftMst")
public class ShiftMst {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "shiftmst_id")
	private long id;
	
	@Column(name = "shiftmst_no")
	private String shiftMstNo = "";
	
	@Column(name = "shiftmst_name")
	private String shiftMstName = "";
	
	@Column(name = "shiftmst_clubId")
	private String shiftMstClubId = "";
	
	@Column(name = "shiftmst_clubNo")
	private String shiftMstClubCode = "";
	
	@Column(name = "shiftmst_inTime")
	private String shiftMstInTime = "";
	
	@Column(name = "shiftmst_outTime")
	private String shiftMstOutTime = "";
	
	@Column(name = "shiftmst_hours")
	private String shiftMstHours = "";
	
	@Column(name = "shiftmst_type")
	private String shiftMstType = "";
	
	@Column(name = "shiftmst_days")
	private String shiftMstDays = "";

	@Column(name = "shiftmst_status")
	private String shiftMstStatus = "";
	
	@Column(name = "shiftmst_isShiftBreakTime")
	private String shiftMstIsShiftBreakTime = "";
	
	@Column(name = "shiftmst_shiftBreakTime")
	private String shiftMstShiftBreakTime = "";

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the shiftMstNo
	 */
	public String getShiftMstNo() {
		return shiftMstNo;
	}

	/**
	 * @param shiftMstNo the shiftMstNo to set
	 */
	public void setShiftMstNo(String shiftMstNo) {
		this.shiftMstNo = shiftMstNo;
	}

	/**
	 * @return the shiftMstName
	 */
	public String getShiftMstName() {
		return shiftMstName;
	}

	/**
	 * @param shiftMstName the shiftMstName to set
	 */
	public void setShiftMstName(String shiftMstName) {
		this.shiftMstName = shiftMstName;
	}

	/**
	 * @return the shiftMstClubCode
	 */
	public String getShiftMstClubCode() {
		return shiftMstClubCode;
	}

	/**
	 * @param shiftMstClubCode the shiftMstClubCode to set
	 */
	public void setShiftMstClubCode(String shiftMstClubCode) {
		this.shiftMstClubCode = shiftMstClubCode;
	}

	/**
	 * @return the shiftMstInTime
	 */
	public String getShiftMstInTime() {
		return shiftMstInTime;
	}

	/**
	 * @param shiftMstInTime the shiftMstInTime to set
	 */
	public void setShiftMstInTime(String shiftMstInTime) {
		this.shiftMstInTime = shiftMstInTime;
	}

	/**
	 * @return the shiftMstOutTime
	 */
	public String getShiftMstOutTime() {
		return shiftMstOutTime;
	}

	/**
	 * @param shiftMstOutTime the shiftMstOutTime to set
	 */
	public void setShiftMstOutTime(String shiftMstOutTime) {
		this.shiftMstOutTime = shiftMstOutTime;
	}

	/**
	 * @return the shiftMstHours
	 */
	public String getShiftMstHours() {
		return shiftMstHours;
	}

	/**
	 * @param shiftMstHours the shiftMstHours to set
	 */
	public void setShiftMstHours(String shiftMstHours) {
		this.shiftMstHours = shiftMstHours;
	}

	/**
	 * @return the shiftMstType
	 */
	public String getShiftMstType() {
		return shiftMstType;
	}

	/**
	 * @param shiftMstType the shiftMstType to set
	 */
	public void setShiftMstType(String shiftMstType) {
		this.shiftMstType = shiftMstType;
	}

	/**
	 * @return the shiftMstDays
	 */
	public String getShiftMstDays() {
		return shiftMstDays;
	}

	/**
	 * @param shiftMstDays the shiftMstDays to set
	 */
	public void setShiftMstDays(String shiftMstDays) {
		this.shiftMstDays = shiftMstDays;
	}

	/**
	 * @return the shiftMstStatus
	 */
	public String getShiftMstStatus() {
		return shiftMstStatus;
	}

	/**
	 * @param shiftMstStatus the shiftMstStatus to set
	 */
	public void setShiftMstStatus(String shiftMstStatus) {
		this.shiftMstStatus = shiftMstStatus;
	}

	/**
	 * @return the shiftMstIsShiftBreakTime
	 */
	public String getShiftMstIsShiftBreakTime() {
		return shiftMstIsShiftBreakTime;
	}

	/**
	 * @param shiftMstIsShiftBreakTime the shiftMstIsShiftBreakTime to set
	 */
	public void setShiftMstIsShiftBreakTime(String shiftMstIsShiftBreakTime) {
		this.shiftMstIsShiftBreakTime = shiftMstIsShiftBreakTime;
	}

	/**
	 * @return the shiftMstShiftBreakTime
	 */
	public String getShiftMstShiftBreakTime() {
		return shiftMstShiftBreakTime;
	}

	/**
	 * @param shiftMstShiftBreakTime the shiftMstShiftBreakTime to set
	 */
	public void setShiftMstShiftBreakTime(String shiftMstShiftBreakTime) {
		this.shiftMstShiftBreakTime = shiftMstShiftBreakTime;
	}
	
	
}
