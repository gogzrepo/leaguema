/**
 * @author Sunil
 */
package com.leaguema.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.validator.constraints.NotEmpty;

import com.sun.istack.internal.NotNull;

@Entity
@Table(name = "user", catalog = "leaguema")
public class BMUser {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "user_uid", length = 10)
	private long    userUid = 0;
	
	
	@Column(name = "username", nullable = false, length = 25)
	@NotEmpty
	private String  username;
	
	@Column(name = "password", nullable = false, length = 60)
	private String  password;
	
	@Column(name = "enabled", nullable = false)
	private boolean enabled;
	
	@Column(name = "user_empCode", nullable = false, length = 30)
	private String  userEmpCode = "";
	
	@Column(name = "user_FName",nullable = false, length = 25)
	private String  userFName = "";
	
	@Column(name = "user_LName",nullable = false, length = 25)
	private String  userLName = "";
	
	@Column(name = "user_Gender",nullable = false, length = 1)
	private String  userGender = "";
	
	@Column(name = "user_DesgId",nullable = false, length = 3)
	private long    userDesgId =  0;
	
	@Column(name = "user_MainBranch",nullable = false, length = 10)
	private long    userMainBranch = 0;
	
	@Column(name = "user_OTP",nullable = false, length = 6)
	private String  userOTP = "";
	
	@Column(name = "user_PSDate")
	private Date    userPSDate = null;
	
	@Column(name = "user_JoinDate")
	private Date    userJoinDate = null;
	
	@Column(name = "user_CreateDate",nullable = false)
	private Date    userCreateDate = null;
	
	@Column(name = "user_Status",nullable = false)
	private String  userStatus = "";
	
	@Column(name = "user_MultiBrh",nullable = false)
	private String  userMultiBrh = "";
	
	@Column(name = "user_AuthPower",nullable = false, length = 25)
	private String  userAuthPower = "";
	
	@Column(name = "user_DayOCPower")
	private boolean userDayOCPower = false;
	
	@Column(name = "user_Category",nullable = false, length = 30)
	private String  userCategory = "";
	
	@Column(name = "user_AddrId",nullable = false, length = 10)
	private long    userAddrId = 0;
	
	@Column(name = "user_MkrId",nullable = false, length = 9)
	private long    userMkrId = 0;
	
	@Column(name = "user_Ckr",nullable = false, length = 9)
	private long    userCkr = 0;
	
	@Column(name = "user_IsPwdExpired",nullable = false)
	private boolean userIsPwdExpired = false;
	
	@Column(name = "user_PwdExpiredDays",nullable = false, length = 9)
	private int     userPwdExpiredDays = 0;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
	//@Cascade(value = CascadeType.ALL)
	private Set<BMUserRole> userRole = new HashSet<BMUserRole>(0);
	
	@Column(name = "user_clubRegistrationId")
	private long userClubRegitrationId = 0;
	
	@Column(name = "user_isVerifyRegistration")
	private boolean isVerifyRegistration = false;
	
	@Column(name = "user_clubName")
	private String userClubName = "";
	
	/*@OneToOne
	@JoinColumn ( name = "c_id")
	private ClubRegistration clubRegistration;*/
	
	
	public BMUser() {
		
	}
	
	/**
	 * @param userUid
	 * @param username
	 * @param password
	 * @param enabled
	 * @param userEmpCode
	 * @param userFName
	 * @param userLName
	 * @param userGender
	 * @param userDesgId
	 * @param userMainBranch
	 * @param userOTP
	 * @param userPSDate
	 * @param userJoinDate
	 * @param userCreateDate
	 * @param userStatus
	 * @param userMultiBrh
	 * @param userAuthPower
	 * @param userDayOCPower
	 * @param userCategory
	 * @param userAddrId
	 * @param userMkrId
	 * @param userCkr
	 * @param userIsPwdExpired
	 * @param userPwdExpiredDays
	 * @param userRole
	 */
	public BMUser(long userUid, String username, String password, boolean enabled, String userEmpCode, String userFName,
			String userLName, String userGender, long userDesgId, long userMainBranch, String userOTP, Date userPSDate,
			Date userJoinDate, Date userCreateDate, String userStatus, String userMultiBrh, String userAuthPower,
			boolean userDayOCPower, String userCategory, long userAddrId, long userMkrId, long userCkr,
			boolean userIsPwdExpired, int userPwdExpiredDays, Set<BMUserRole> userRole) {
		this.userUid = userUid;
		this.username = username;
		this.password = password;
		this.enabled = enabled;
		this.userEmpCode = userEmpCode;
		this.userFName = userFName;
		this.userLName = userLName;
		this.userGender = userGender;
		this.userDesgId = userDesgId;
		this.userMainBranch = userMainBranch;
		this.userOTP = userOTP;
		this.userPSDate = userPSDate;
		this.userJoinDate = userJoinDate;
		this.userCreateDate = userCreateDate;
		this.userStatus = userStatus;
		this.userMultiBrh = userMultiBrh;
		this.userAuthPower = userAuthPower;
		this.userDayOCPower = userDayOCPower;
		this.userCategory = userCategory;
		this.userAddrId = userAddrId;
		this.userMkrId = userMkrId;
		this.userCkr = userCkr;
		this.userIsPwdExpired = userIsPwdExpired;
		this.userPwdExpiredDays = userPwdExpiredDays;
		this.userRole = userRole;
	}

	
	/**
	 * @param userUid
	 * @param username
	 * @param password
	 * @param enabled
	 * @param userEmpCode
	 * @param userFName
	 * @param userLName
	 * @param userGender
	 * @param userDesgId
	 * @param userMainBranch
	 * @param userOTP
	 * @param userPSDate
	 * @param userJoinDate
	 * @param userCreateDate
	 * @param userStatus
	 * @param userMultiBrh
	 * @param userAuthPower
	 * @param userDayOCPower
	 * @param userCategory
	 * @param userAddrId
	 * @param userMkrId
	 * @param userCkr
	 * @param userIsPwdExpired
	 * @param userPwdExpiredDays
	 */
	public BMUser(long userUid, String username, String password, boolean enabled, String userEmpCode, String userFName,
			String userLName, String userGender, long userDesgId, long userMainBranch, String userOTP, Date userPSDate,
			Date userJoinDate, Date userCreateDate, String userStatus, String userMultiBrh, String userAuthPower,
			boolean userDayOCPower, String userCategory, long userAddrId, long userMkrId, long userCkr,
			boolean userIsPwdExpired, int userPwdExpiredDays) {
		this.userUid = userUid;
		this.username = username;
		this.password = password;
		this.enabled = enabled;
		this.userEmpCode = userEmpCode;
		this.userFName = userFName;
		this.userLName = userLName;
		this.userGender = userGender;
		this.userDesgId = userDesgId;
		this.userMainBranch = userMainBranch;
		this.userOTP = userOTP;
		this.userPSDate = userPSDate;
		this.userJoinDate = userJoinDate;
		this.userCreateDate = userCreateDate;
		this.userStatus = userStatus;
		this.userMultiBrh = userMultiBrh;
		this.userAuthPower = userAuthPower;
		this.userDayOCPower = userDayOCPower;
		this.userCategory = userCategory;
		this.userAddrId = userAddrId;
		this.userMkrId = userMkrId;
		this.userCkr = userCkr;
		this.userIsPwdExpired = userIsPwdExpired;
		this.userPwdExpiredDays = userPwdExpiredDays;
	}

	/**
	 * @return the userUid
	 */
	public long getUserUid() {
		return userUid;
	}

	/**
	 * @param userUid the userUid to set
	 */
	public void setUserUid(long userUid) {
		this.userUid = userUid;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the enabled
	 */
	public boolean isEnabled() {
		return enabled;
	}

	/**
	 * @param enabled the enabled to set
	 */
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	/**
	 * @return the userEmpCode
	 */
	public String getUserEmpCode() {
		return userEmpCode;
	}

	/**
	 * @param userEmpCode the userEmpCode to set
	 */
	public void setUserEmpCode(String userEmpCode) {
		this.userEmpCode = userEmpCode;
	}

	/**
	 * @return the userFName
	 */
	public String getUserFName() {
		return userFName;
	}

	/**
	 * @param userFName the userFName to set
	 */
	public void setUserFName(String userFName) {
		this.userFName = userFName;
	}

	/**
	 * @return the userLName
	 */
	public String getUserLName() {
		return userLName;
	}

	/**
	 * @param userLName the userLName to set
	 */
	public void setUserLName(String userLName) {
		this.userLName = userLName;
	}

	/**
	 * @return the userGender
	 */
	public String getUserGender() {
		return userGender;
	}

	/**
	 * @param userGender the userGender to set
	 */
	public void setUserGender(String userGender) {
		this.userGender = userGender;
	}

	/**
	 * @return the userDesgId
	 */
	public long getUserDesgId() {
		return userDesgId;
	}

	/**
	 * @param userDesgId the userDesgId to set
	 */
	public void setUserDesgId(long userDesgId) {
		this.userDesgId = userDesgId;
	}

	/**
	 * @return the userMainBranch
	 */
	public long getUserMainBranch() {
		return userMainBranch;
	}

	/**
	 * @param userMainBranch the userMainBranch to set
	 */
	public void setUserMainBranch(long userMainBranch) {
		this.userMainBranch = userMainBranch;
	}

	/**
	 * @return the userOTP
	 */
	public String getUserOTP() {
		return userOTP;
	}

	/**
	 * @param userOTP the userOTP to set
	 */
	public void setUserOTP(String userOTP) {
		this.userOTP = userOTP;
	}

	/**
	 * @return the userPSDate
	 */
	public Date getUserPSDate() {
		return userPSDate;
	}

	/**
	 * @param userPSDate the userPSDate to set
	 */
	public void setUserPSDate(Date userPSDate) {
		this.userPSDate = userPSDate;
	}

	/**
	 * @return the userJoinDate
	 */
	public Date getUserJoinDate() {
		return userJoinDate;
	}

	/**
	 * @param userJoinDate the userJoinDate to set
	 */
	public void setUserJoinDate(Date userJoinDate) {
		this.userJoinDate = userJoinDate;
	}

	/**
	 * @return the userCreateDate
	 */
	public Date getUserCreateDate() {
		return userCreateDate;
	}

	/**
	 * @param userCreateDate the userCreateDate to set
	 */
	public void setUserCreateDate(Date userCreateDate) {
		this.userCreateDate = userCreateDate;
	}

	/**
	 * @return the userStatus
	 */
	public String getUserStatus() {
		return userStatus;
	}

	/**
	 * @param userStatus the userStatus to set
	 */
	public void setUserStatus(String userStatus) {
		this.userStatus = userStatus;
	}

	/**
	 * @return the userMultiBrh
	 */
	public String getUserMultiBrh() {
		return userMultiBrh;
	}

	/**
	 * @param userMultiBrh the userMultiBrh to set
	 */
	public void setUserMultiBrh(String userMultiBrh) {
		this.userMultiBrh = userMultiBrh;
	}

	/**
	 * @return the userAuthPower
	 */
	public String getUserAuthPower() {
		return userAuthPower;
	}

	/**
	 * @param userAuthPower the userAuthPower to set
	 */
	public void setUserAuthPower(String userAuthPower) {
		this.userAuthPower = userAuthPower;
	}

	/**
	 * @return the userDayOCPower
	 */
	public boolean isUserDayOCPower() {
		return userDayOCPower;
	}

	/**
	 * @param userDayOCPower the userDayOCPower to set
	 */
	public void setUserDayOCPower(boolean userDayOCPower) {
		this.userDayOCPower = userDayOCPower;
	}

	/**
	 * @return the userCategory
	 */
	public String getUserCategory() {
		return userCategory;
	}

	/**
	 * @param userCategory the userCategory to set
	 */
	public void setUserCategory(String userCategory) {
		this.userCategory = userCategory;
	}

	/**
	 * @return the userAddrId
	 */
	public long getUserAddrId() {
		return userAddrId;
	}

	/**
	 * @param userAddrId the userAddrId to set
	 */
	public void setUserAddrId(long userAddrId) {
		this.userAddrId = userAddrId;
	}

	/**
	 * @return the userMkrId
	 */
	public long getUserMkrId() {
		return userMkrId;
	}

	/**
	 * @param userMkrId the userMkrId to set
	 */
	public void setUserMkrId(long userMkrId) {
		this.userMkrId = userMkrId;
	}

	/**
	 * @return the userCkr
	 */
	public long getUserCkr() {
		return userCkr;
	}

	/**
	 * @param userCkr the userCkr to set
	 */
	public void setUserCkr(long userCkr) {
		this.userCkr = userCkr;
	}

	/**
	 * @return the userIsPwdExpired
	 */
	public boolean isUserIsPwdExpired() {
		return userIsPwdExpired;
	}

	/**
	 * @param userIsPwdExpired the userIsPwdExpired to set
	 */
	public void setUserIsPwdExpired(boolean userIsPwdExpired) {
		this.userIsPwdExpired = userIsPwdExpired;
	}

	/**
	 * @return the userPwdExpiredDays
	 */
	public int getUserPwdExpiredDays() {
		return userPwdExpiredDays;
	}

	/**
	 * @param userPwdExpiredDays the userPwdExpiredDays to set
	 */
	public void setUserPwdExpiredDays(int userPwdExpiredDays) {
		this.userPwdExpiredDays = userPwdExpiredDays;
	}

	/**
	 * @return the userRole
	 */
	public Set<BMUserRole> getUserRole() {
		return userRole;
	}

	/**
	 * @param userRole the userRole to set
	 */
	public void setUserRole(Set<BMUserRole> userRole) {
		this.userRole = userRole;
	}

	/**
	 * @return the userClubRegitrationId
	 */
	public long getUserClubRegitrationId() {
		return userClubRegitrationId;
	}

	/**
	 * @param userClubRegitrationId the userClubRegitrationId to set
	 */
	public void setUserClubRegitrationId(long userClubRegitrationId) {
		this.userClubRegitrationId = userClubRegitrationId;
	}

	/**
	 * @return the isVerifyRegistration
	 */
	public boolean isVerifyRegistration() {
		return isVerifyRegistration;
	}

	/**
	 * @param isVerifyRegistration the isVerifyRegistration to set
	 */
	public void setVerifyRegistration(boolean isVerifyRegistration) {
		this.isVerifyRegistration = isVerifyRegistration;
	}

	/**
	 * @return the userClubName
	 */
	public String getUserClubName() {
		return userClubName;
	}

	/**
	 * @param userClubName the userClubName to set
	 */
	public void setUserClubName(String userClubName) {
		this.userClubName = userClubName;
	}
	
	
}