/**
 * 
 */
package com.leaguema.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 * @author Sunil
 *
 */

@Entity
@Table(name = "tbl_playerRegistration")
@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class)
public class PlayerRegistration {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "pr_id")
	private long id;
	
	@Column(name = "pr_name")
	private String clubName;
	
	@Column(name = "pr_clubCode")
	private String clubCode;
	
	@Column(name = "pr_playerFName")
	private String playerFName;
	
	@Column(name = "pr_playerLName")
	private String playerLName;
	
	@Column(name = "pr_playerFthrFName")
	private String playerFthrFName;
	
	@Column(name = "pr_playerFthrLName")
	private String playerFthrLName;
	
	@Column(name = "pr_playerDOB")
	private String playerDOB;
			
	@Column(name = "pr_playerID")
	private String playerID;
	
	@Column(name = "pr_playerAddress")
	private String playerAddress;
	
	@Column(name = "pr_playerAddress2")
	private String playerAddress2;
	
	@Column(name = "pr_playerAddressPincode")
	private String playerAddressPincode;
	
	@Column(name = "pr_playerAddressPincode2")
	private String playerAddressPincode2;
	
	@Column(name = "pr_playerEmail")
	private String playerEmail;
	
	@Column(name = "pr_playerMobile")
	private String playerMobile;
	
	@Column(name = "pr_playerEducation")
	private String playerEducation;
	
	@Column(name = "pr_playerOthersEducation")
	private String playerOthersEducation;
	
	@Column(name = "pr_playerLevel")
	private String playerLevel;
	
	@Column(name = "pr_playerPic")
	private byte playerPic;
	
	@Column(name = "pr_playerWeight")
	private float playerWeight;
	
	@Column(name = "pr_playerAge")
	private int playerAge;
	
	@Column(name = "pr_playerLastGamePalyed")
	private String playerLastGamePalyed;
	
	@Column(name = "pr_isVerifyRegistration")
	private boolean isVerifyRegistration = false;
	
	@Column(name = "pr_isActive")
	private boolean isActive;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name ="pr_c_id")
	private ClubRegistration clubRegistration;
	
	@Column(name = "pr_isCNVVerify")
	private boolean playerIsCNVVerify = false;
	
	@Column(name = "pr_CNV")
	private String playerOTP = "";
	
	@Column(name = "pr_shift")
	private String playerShift = "";
	
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the clubName
	 */
	public String getClubName() {
		return clubName;
	}

	/**
	 * @param clubName the clubName to set
	 */
	public void setClubName(String clubName) {
		this.clubName = clubName;
	}

	/**
	 * @return the clubCode
	 */
	public String getClubCode() {
		return clubCode;
	}

	/**
	 * @param clubCode the clubCode to set
	 */
	public void setClubCode(String clubCode) {
		this.clubCode = clubCode;
	}

	/**
	 * @return the playerFName
	 */
	public String getPlayerFName() {
		return playerFName;
	}

	/**
	 * @param playerFName the playerFName to set
	 */
	public void setPlayerFName(String playerFName) {
		this.playerFName = playerFName;
	}

	/**
	 * @return the playerLName
	 */
	public String getPlayerLName() {
		return playerLName;
	}

	/**
	 * @param playerLName the playerLName to set
	 */
	public void setPlayerLName(String playerLName) {
		this.playerLName = playerLName;
	}

	/**
	 * @return the playerFthrFName
	 */
	public String getPlayerFthrFName() {
		return playerFthrFName;
	}

	/**
	 * @param playerFthrFName the playerFthrFName to set
	 */
	public void setPlayerFthrFName(String playerFthrFName) {
		this.playerFthrFName = playerFthrFName;
	}

	/**
	 * @return the playerFthrLName
	 */
	public String getPlayerFthrLName() {
		return playerFthrLName;
	}

	/**
	 * @param playerFthrLName the playerFthrLName to set
	 */
	public void setPlayerFthrLName(String playerFthrLName) {
		this.playerFthrLName = playerFthrLName;
	}

	/**
	 * @return the playerDOB
	 */
	public String getPlayerDOB() {
		return playerDOB;
	}

	/**
	 * @param playerDOB the playerDOB to set
	 */
	public void setPlayerDOB(String playerDOB) {
		this.playerDOB = playerDOB;
	}

	/**
	 * @return the playerID
	 */
	public String getPlayerID() {
		return playerID;
	}

	/**
	 * @param playerID the playerID to set
	 */
	public void setPlayerID(String playerID) {
		this.playerID = playerID;
	}

	/**
	 * @return the playerAddress
	 */
	public String getPlayerAddress() {
		return playerAddress;
	}

	/**
	 * @param playerAddress the playerAddress to set
	 */
	public void setPlayerAddress(String playerAddress) {
		this.playerAddress = playerAddress;
	}

	/**
	 * @return the playerAddress2
	 */
	public String getPlayerAddress2() {
		return playerAddress2;
	}

	/**
	 * @param playerAddress2 the playerAddress2 to set
	 */
	public void setPlayerAddress2(String playerAddress2) {
		this.playerAddress2 = playerAddress2;
	}

	/**
	 * @return the playerAddressPincode
	 */
	public String getPlayerAddressPincode() {
		return playerAddressPincode;
	}

	/**
	 * @param playerAddressPincode the playerAddressPincode to set
	 */
	public void setPlayerAddressPincode(String playerAddressPincode) {
		this.playerAddressPincode = playerAddressPincode;
	}

	/**
	 * @return the playerAddressPincode2
	 */
	public String getPlayerAddressPincode2() {
		return playerAddressPincode2;
	}

	/**
	 * @param playerAddressPincode2 the playerAddressPincode2 to set
	 */
	public void setPlayerAddressPincode2(String playerAddressPincode2) {
		this.playerAddressPincode2 = playerAddressPincode2;
	}

	/**
	 * @return the playerEmail
	 */
	public String getPlayerEmail() {
		return playerEmail;
	}

	/**
	 * @param playerEmail the playerEmail to set
	 */
	public void setPlayerEmail(String playerEmail) {
		this.playerEmail = playerEmail;
	}

	/**
	 * @return the playerMobile
	 */
	public String getPlayerMobile() {
		return playerMobile;
	}

	/**
	 * @param playerMobile the playerMobile to set
	 */
	public void setPlayerMobile(String playerMobile) {
		this.playerMobile = playerMobile;
	}

	/**
	 * @return the playerEducation
	 */
	public String getPlayerEducation() {
		return playerEducation;
	}

	/**
	 * @param playerEducation the playerEducation to set
	 */
	public void setPlayerEducation(String playerEducation) {
		this.playerEducation = playerEducation;
	}

	/**
	 * @return the playerOthersEducation
	 */
	public String getPlayerOthersEducation() {
		return playerOthersEducation;
	}

	/**
	 * @param playerOthersEducation the playerOthersEducation to set
	 */
	public void setPlayerOthersEducation(String playerOthersEducation) {
		this.playerOthersEducation = playerOthersEducation;
	}

	/**
	 * @return the playerLevel
	 */
	public String getPlayerLevel() {
		return playerLevel;
	}

	/**
	 * @param playerLevel the playerLevel to set
	 */
	public void setPlayerLevel(String playerLevel) {
		this.playerLevel = playerLevel;
	}

	/**
	 * @return the playerPic
	 */
	public byte getPlayerPic() {
		return playerPic;
	}

	/**
	 * @param playerPic the playerPic to set
	 */
	public void setPlayerPic(byte playerPic) {
		this.playerPic = playerPic;
	}

	/**
	 * @return the playerWeight
	 */
	public float getPlayerWeight() {
		return playerWeight;
	}

	/**
	 * @param playerWeight the playerWeight to set
	 */
	public void setPlayerWeight(float playerWeight) {
		this.playerWeight = playerWeight;
	}

	/**
	 * @return the playerAge
	 */
	public int getPlayerAge() {
		return playerAge;
	}

	/**
	 * @param playerAge the playerAge to set
	 */
	public void setPlayerAge(int playerAge) {
		this.playerAge = playerAge;
	}

	/**
	 * @return the playerLastGamePalyed
	 */
	public String getPlayerLastGamePalyed() {
		return playerLastGamePalyed;
	}

	/**
	 * @param playerLastGamePalyed the playerLastGamePalyed to set
	 */
	public void setPlayerLastGamePalyed(String playerLastGamePalyed) {
		this.playerLastGamePalyed = playerLastGamePalyed;
	}

	/**
	 * @return the isVerifyRegistration
	 */
	public boolean isVerifyRegistration() {
		return isVerifyRegistration;
	}

	/**
	 * @param isVerifyRegistration the isVerifyRegistration to set
	 */
	public void setVerifyRegistration(boolean isVerifyRegistration) {
		this.isVerifyRegistration = isVerifyRegistration;
	}

	/**
	 * @return the isActive
	 */
	public boolean getIsActive() {
		return isActive;
	}

	/**
	 * @param isActive the isActive to set
	 */
	public void setIsActive(boolean isActive) {
		this.isActive = isActive;
	}

	/**
	 * @return the clubRegistration
	 */
	public ClubRegistration getClubRegistration() {
		return clubRegistration;
	}

	/**
	 * @param clubRegistration the clubRegistration to set
	 */
	public void setClubRegistration(ClubRegistration clubRegistration) {
		this.clubRegistration = clubRegistration;
	}

	/**
	 * @return the playerIsCNVVerify
	 */
	public boolean isPlayerIsCNVVerify() {
		return playerIsCNVVerify;
	}

	/**
	 * @param playerIsCNVVerify the playerIsCNVVerify to set
	 */
	public void setPlayerIsCNVVerify(boolean playerIsCNVVerify) {
		this.playerIsCNVVerify = playerIsCNVVerify;
	}

	/**
	 * @return the playerOTP
	 */
	public String getPlayerOTP() {
		return playerOTP;
	}

	/**
	 * @param playerOTP the playerOTP to set
	 */
	public void setPlayerOTP(String playerOTP) {
		this.playerOTP = playerOTP;
	}

	/**
	 * @return the playerShift
	 */
	public String getPlayerShift() {
		return playerShift;
	}

	/**
	 * @param playerShift the playerShift to set
	 */
	public void setPlayerShift(String playerShift) {
		this.playerShift = playerShift;
	}
	
}
