/**
 * 
 */
package com.leaguema.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Sunil
 *
 */
@Entity
@Table(name = "tbl_clubCVN")
public class ClubValidationNumber {

	/**
	 * @param args
	 */
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private long id;
	
	@Column(name = "cvn_clubId")
	private long clubId = 0;
	
	@Column(name = "cvn_clubNo")
	private String clubCode = "";
	
	@Column(name = "cvn_activationCode")
	private String clubActivationCode = "";
	
	@Column(name = "cvn_isActive")
	private boolean isActive = false;

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the clubId
	 */
	public long getClubId() {
		return clubId;
	}

	/**
	 * @param clubId the clubId to set
	 */
	public void setClubId(long clubId) {
		this.clubId = clubId;
	}

	/**
	 * @return the clubCode
	 */
	public String getClubCode() {
		return clubCode;
	}

	/**
	 * @param clubCode the clubCode to set
	 */
	public void setClubCode(String clubCode) {
		this.clubCode = clubCode;
	}

	/**
	 * @return the clubActivationCode
	 */
	public String getClubActivationCode() {
		return clubActivationCode;
	}

	/**
	 * @param clubActivationCode the clubActivationCode to set
	 */
	public void setClubActivationCode(String clubActivationCode) {
		this.clubActivationCode = clubActivationCode;
	}

	/**
	 * @return the isActive
	 */
	public boolean isActive() {
		return isActive;
	}

	/**
	 * @param isActive the isActive to set
	 */
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}
	
}
