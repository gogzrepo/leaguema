package com.leaguema.service;

import java.util.List;

import com.leaguema.model.ShiftMst;

public interface IShiftService {
	
	public List<ShiftMst> getAllActiveShift(String status, String clubId);

}
