/**
 * 
 */
package com.leaguema.service;

import com.leaguema.model.BMUser;

/**
 * @author Sunil
 *
 */
public interface IUserService {
	
	public BMUser createUser(BMUser user);

}
