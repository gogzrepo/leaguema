/**
 * 
 */
package com.leaguema.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.leaguema.dao.IPlayerRegistrationDao;
import com.leaguema.model.PlayerRegistration;

/**
 * @author Sunil
 *
 */
@Service
public class PlayerRegistrationServiceImpl implements IPlayerRegistrationService {

	@Autowired
	IPlayerRegistrationDao playerRegistrationDao;
	
	@Override
	public String getPlayerAlreadyRegistered(PlayerRegistration playerRegistration, boolean isVerifyRegistration, String operationType)
			throws Exception {
		return playerRegistrationDao.getPlayerAlreadyRegistered(playerRegistration, isVerifyRegistration, operationType);
	}

	@Override
	public String savePlayerRegistrationData(PlayerRegistration playerRegistration, long loggedClubId, String clubNo, String operationType, String CVN)
			throws Exception {
		return playerRegistrationDao.savePlayerRegistrationData(playerRegistration, loggedClubId, clubNo, operationType, CVN);
	}

	@Override
	public PlayerRegistration getRegistrationData(String regId, String operationType) throws Exception {
		return playerRegistrationDao.getRegistrationData(regId, operationType);
	}

	@Override
	public String saveVerifiedPlayerRegistrationData(String regId, String operationType) throws Exception {
		return playerRegistrationDao.saveVerifiedPlayerRegistrationData(regId, operationType);
	}
	
	@Override
	public List<PlayerRegistration> getAllPlayer(String searchCriteria) throws Exception {
		return playerRegistrationDao.getAllPlayer(searchCriteria);
	}

}
