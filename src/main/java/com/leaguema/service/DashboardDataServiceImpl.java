/**
 * 
 */
package com.leaguema.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.leaguema.dao.IDashboardDataDao;
import com.leaguema.model.ClubRegistration;
import com.leaguema.model.DashboardData;

/**
 * @author Sunil
 *
 */
@Service
public class DashboardDataServiceImpl implements IDashboardDataService{

	@Autowired
	IDashboardDataDao dashboardData;
	
	@Override
	public DashboardData loadDashboardData(long userId) throws Exception {
		// TODO Auto-generated method stub
		return dashboardData.loadDashboardData(userId);
	}

}
