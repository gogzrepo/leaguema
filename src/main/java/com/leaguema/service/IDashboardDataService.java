/**
 * 
 */
package com.leaguema.service;

import java.util.List;

import com.leaguema.model.ClubRegistration;
import com.leaguema.model.DashboardData;

/**
 * @author Sunil
 *
 */
public interface IDashboardDataService {
	
	public DashboardData loadDashboardData(long userId) throws Exception;

}
