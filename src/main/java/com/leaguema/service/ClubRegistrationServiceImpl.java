package com.leaguema.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.leaguema.dao.ClubRegistrationDao;
import com.leaguema.model.BMUser;
import com.leaguema.model.ClubRegistration;

@Service
public class ClubRegistrationServiceImpl implements ClubRegistrationService{

	@Autowired
	private ClubRegistrationDao clubRegistrationDao;
	
	@Override
	public String saveClubRegistrationData(ClubRegistration clubRegistration, BMUser user, String operationType) throws Exception {
		return clubRegistrationDao.saveClubRegistrationData(clubRegistration, user, operationType);
	}

	@Override
	public ClubRegistration getRegistrationData(String regId, String operationType) throws Exception {
		return clubRegistrationDao.getRegistrationData(regId, operationType);
	}

	@Override
	public String saveVerifyRegistrationData(String regId, String operationType) throws Exception {
		return clubRegistrationDao.saveVerifyRegistrationData(regId, operationType);
	}

	@Override
	public String getClubAlreadyRegistered(ClubRegistration clubRegistration, boolean verifyStatus, String operationType) throws Exception {
		return clubRegistrationDao.getClubAlreadyRegistered(clubRegistration, verifyStatus, operationType);
	}

	@Override
	public BMUser getLoggedUserData(String username) throws Exception {
		return clubRegistrationDao.getLoggedUserData(username);
	}

	@Override
	public List<ClubRegistration> getAllActiveClub(String clubStatus) throws Exception {
		return clubRegistrationDao.getAllActiveClub(clubStatus);
	}

}
