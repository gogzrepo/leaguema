/**
 * 
 */
package com.leaguema.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.leaguema.dao.ICommonDao;

/**
 * @author Sunil
 *
 */

@Service
public class CommonService implements ICommonService {

	@Autowired
	ICommonDao commonDao;
	
	
	public String updateMergeTest() {
		// TODO Auto-generated method stub
		return commonDao.updateMergeTest();
	}


	@Transactional
	public String savePersistTest() {
		// TODO Auto-generated method stub
		return commonDao.savePersistTest();
	}
	
	@Transactional
	public void CriteriaTest() {
		commonDao.CriteriaTest();
	}
	
	@Transactional
	public void HibernateHOLJoinTest() {
		commonDao.HibernateHOLJoinTest();
	}

}
