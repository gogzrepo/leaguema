package com.leaguema.service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.leaguema.dao.IMenuDao;

@Service
public class MenuServiceImpl implements IMenuService {

	@Autowired
	IMenuDao menuDao;
	
	@Override
    public List<Object> getMenuData(String userId,String desigId) throws SQLException {
		
		return menuDao.getMenuData(userId, desigId);
        
    }
	
	

}
