/**
 * 
 */
package com.leaguema.service;

/**
 * @author Sunil
 *
 */
public interface ICommonService {
	
	public String updateMergeTest();
	
	public String savePersistTest();
	
	public void CriteriaTest();
	
	public void HibernateHOLJoinTest();

}
