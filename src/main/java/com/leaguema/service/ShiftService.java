package com.leaguema.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.leaguema.dao.IShiftDao;
import com.leaguema.model.ShiftMst;

@Service
public class ShiftService implements IShiftService{

	
	@Autowired
	IShiftDao shiftDao;
	
	@Override
	public List<ShiftMst> getAllActiveShift(String status, String clubId) {
		// TODO Auto-generated method stub
		return shiftDao.getAllActiveShift(status, clubId);
	}

}
