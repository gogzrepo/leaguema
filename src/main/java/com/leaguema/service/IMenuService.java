package com.leaguema.service;

import java.sql.SQLException;
import java.util.List;

public interface IMenuService {
	
	public List<Object> getMenuData(String userId, String desgId) throws SQLException;

}
