/**
 * 
 */
package com.leaguema.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.leaguema.dao.BMUserDao;
import com.leaguema.model.BMUser;

/**
 * @author Sunil
 *
 */
@Service
public class UserServiceImpl implements IUserService{

	/**
	 * 
	 */
	@Autowired
	BMUserDao userDao;
	
	public UserServiceImpl() {
		// TODO Auto-generated constructor stub
	}

	@Transactional
	public BMUser createUser(BMUser user) {
		return userDao.createUser(user);
	}
	
	/*@Autowired
	private JdbcTemplate jdbcTemplate;*/ 
	
	/*@Transactional
	public BMUser createUser(BMUser user) {
		try {
		jdbcTemplate.update("insert into txn1 values(1,'sunil1','seohara1')");
		jdbcTemplate.update("insert into txn2 values(2,'sunil12345678901234567','seohara1')");
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		//return userDao.transactionTesting();
		return new BMUser();
	}*/
	

}
