/**
 * 
 */
package com.leaguema.experiment;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Serializable;

/**
 * @author Sunil
 *
 */
class SerializableTest implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6913476333376975477L;
	
	private String name;
	
	private int age;

	private static int no;
	
	private transient int marks;
	
	private String address;
	
	/**
	 * @param name
	 * @param age
	 */
	public SerializableTest(String name, int age, int no, int marks) {
		this.name = name;
		this.age = age;
		this.no = no;
	}
	
	public SerializableTest(String name, int age) {
		this.name = name;
		this.age = age;
	}
	
	
	/**
	 * @return the marks
	 */
	public int getMarks() {
		return marks;
	}

	/**
	 * @param marks the marks to set
	 */
	public void setMarks(int marks) {
		this.marks = marks;
	}

	/**
	 * @return the no
	 */
	public static int getNo() {
		return no;
	}

	/**
	 * @param no the no to set
	 */
	public static void setNo(int no) {
		SerializableTest.no = no;
	}


	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the age
	 */
	public int getAge() {
		return age;
	}

	/**
	 * @param age the age to set
	 */
	public void setAge(int age) {
		this.age = age;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "SerializableTest [name=" + name + ", age=" + age +"] ";
	}
	
	
	
	
}

public class Experiment {

	/**
	 * @param args
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 * @throws ClassNotFoundException 
	 */
	public static void main(String[] args) throws FileNotFoundException, IOException, ClassNotFoundException {
		
		SerializableTest objTest1 = new SerializableTest("sunil", 26,10, 98);
		//SerializableTest objTest1 = new SerializableTest("sunil", 26);
		
		System.out.println(objTest1.toString());
		
		ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream("test.txt"));
		os.writeObject(objTest1);
		
		System.out.println();
		
		ObjectInputStream in = new ObjectInputStream(new FileInputStream("test.txt"));
		SerializableTest objTest2 = (SerializableTest) in.readObject();
		
		System.out.println(objTest2.toString());
		
	}

}
