/**
 * 
 */
package com.leaguema.experiment;

import java.sql.SQLException;

/**
 * @author Sunil
 *
 */
class AOne {
	
	public AOne() {
		// TODO Auto-generated constructor stub
	}
	
	public AOne(int a) {
		// TODO Auto-generated constructor stub
	}
	
	public void display() {
		System.out.println("display super");
		final long a = 2;
		
	}
	
}

class BTwo extends AOne {
	
	public BTwo() {
		
		
		// TODO Auto-generated constructor stub
	}
	
	public void display()  {
		System.out.println("display subclass");
	}
	
}


public class OverridingTest {

	/**
	 * @param args
	 */
	
	public void print(int a, int b) {
		
		System.out.println("int a int b");
		
	}
	
	public void print(long a, long b) {
		
		System.out.println("long a long b");
		
	}
	
	public void print(Integer a, Integer b) {
		
		System.out.println("Integer a Integer b");
		
	}
	
	/*public void print(Long a, Long b) {
		
		System.out.println("Long a Long b");
		
	}*/
	
	
	public static void main(String[] args) {
		
		/*AOne objOne = new BTwo();
		
		objOne.display();
		
		OverridingTest objTest = new OverridingTest();
		Long a = new Long (10);
		Long b = new Long(20);
		objTest.print(a, b);
		
		String abc = "sunil";
		
		StringBuilder sb = new StringBuilder("sunil");
		StringBuilder sb2 = new StringBuilder("sunil");
		
		StringBuffer buf = new StringBuffer("sunil");
		StringBuffer buf2 = new StringBuffer("sunil");
		
		System.out.println(buf.toString().equals(buf2.toString()));
		
		//int iArr[] = new int[-9];
		
		
		//System.out.println(iArr[0]);// it will throw runtime exception as NegativeArraySizeException
		
		int a2 = 10/0;
		
		System.out.println(a2);*/
		try {
		int a[] = new int[1];
		a[1] = 30/0;
		a[2] = 50;
		}
		
		catch(ArithmeticException ex) {
			System.out.println("a");
		}
		catch(ArrayIndexOutOfBoundsException e) {
			System.out.println("b");
		}
		System.out.println("c");
		

	}

}
