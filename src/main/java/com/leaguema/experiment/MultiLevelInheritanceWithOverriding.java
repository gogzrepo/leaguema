/**
 * 
 */
package com.leaguema.experiment;

/**
 * @author Sunil
 *
 */

class A {
	int a = 10;
	void display () {
		System.out.println("display A.");
	}
}

class B extends A{
	int a = 20;
	void display() {
		System.out.println("display B.");
	}
}

class C extends B{
	int a = 30;
	void display() {
		System.out.println("display C.");
	}
}

class D extends C{
	int a = 40;
	void display() {
		System.out.println("display D.");
	}
}

public class MultiLevelInheritanceWithOverriding {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		A a = new B();
		System.out.println(a.a);
		a.display();
		A a2 = new C();
		System.out.println(a2.a);
		a2.display();
		A a3 = new D();
		System.out.println(a3.a);
		a3.display();
		B b = new D();
		System.out.println(b.a);
		b.display();
		
	}

}
