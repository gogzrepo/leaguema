/**
 * 
 */
package com.leaguema.experiment;

import java.util.StringTokenizer;

/**
 * @author Sunil
 *
 */

class abc1 {
	
	private String name;

	public abc1() {
	}
	
	/**
	 * @param name
	 */
	public abc1(String name) {
		this.name = name;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	
}

final class Immutable1 {
	
	public final String name;
	
	public final int age;
	
	/**
	 * @param name
	 * @param age
	 */
	public Immutable1(String name, int age) {
		this.name = name;
		this.age = age;
		
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the age
	 */
	public int getAge() {
		return age;
	}


	
	
	
	
	
	
	
}



public class ImmutableObjectTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		Immutable1 obj1 = new Immutable1("s1", 20);
		
		System.out.println(obj1.getAge()+" "+obj1.getName()+" ");
		
		String str = "my@n#ame@is@sun#il@kum#ar";
		
		/*str = str.replaceAll("@", " ");
		str = str.replaceAll("#", "");
		*/
		int math = 78;
		
		int english = 65;
		
		int science = 67;
		
		String subjectMraks = math+"@"+english+"@"+science;
		
		System.out.println(str);
		StringTokenizer st = new StringTokenizer(str, "@#");
		while(st.hasMoreTokens()) {
			
			System.out.println(st.nextToken());
		}
	}

}
