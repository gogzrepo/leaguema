/**
 * 
 */
package com.leaguema.experiment;

/**
 * @author Sunil
 *
 */
class V1 {
	int a = 10;
	
	void display() {
		
		System.out.println("this is display super.");
	}
	
}

public class OverridingVariableTest extends V1{

	/**
	 * @param args
	 */
	int a = 20;
	
	void display() {
		//super.display();
		System.out.println("sub class display method");
		super.display();
	}
	
	public static void main(String[] args) {
		
		V1 v1 = new OverridingVariableTest();
		v1.display();
		System.out.println(v1.a);

	}

}
