/**
 * 
 */
package com.leaguema.experiment;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/**
 * @author Sunil
 *
 */
abstract class Super implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2026849152435459526L;
	int a;
	
	Super(int a) {
		System.out.println("this is super class constructor.");
		this.a = a;
		
	}
	
	Super() {
		
	}
	
	void print() {
		System.out.println("super class method.");
	}
	
}

public class SerializationTestWithChild extends Super{

	int a;
	int b = 100;
	SerializationTestWithChild(int a) {
		this.a = a;
		System.out.println("this is sub class constructor.");
		// TODO Auto-generated constructor stub
	}
	
	
	void print() {
		System.out.println("sub class method.");
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception{

		//Super s = new SerializationTestWithChild(10);
		
		SerializationTestWithChild s = new SerializationTestWithChild(20);
		
		ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream("test.txt"));
		os.writeObject(s);
		
		ObjectInputStream on = new ObjectInputStream(new FileInputStream("test.txt"));
		SerializationTestWithChild s2 = (SerializationTestWithChild)on.readObject();
		
		System.out.println(s2.a+" ");
		
		boolean b = true;
		
		if(b=false)
			System.out.println("false");
		
		System.out.println("b="+b);
		
		
	}

}
