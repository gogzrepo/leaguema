/**
 * 
 */
package com.leaguema.dao;

/**
 * @author Sunil
 *
 */
public interface ICommonDao {
	
	public String updateMergeTest();
	
	public String savePersistTest();
	
	public void CriteriaTest();
	
	public void HibernateHOLJoinTest();

}
