package com.leaguema.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.leaguema.function.SelectQueryFunction;
import com.leaguema.model.Menu;

@Repository
public class MenuDaoImpl implements IMenuDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Object> getMenuData(String userId, String desigId) throws SQLException {
		List<Object> objList = new ArrayList<Object>();
		Session session = sessionFactory.openSession();
        String[][] menuData = null;
        String sQuery =  "Select MENU_FNAME,MENU_NEXT,MENU_PARENTID,MENU_INDEX,MENU_UID from TBL_MENU where MENU_STATUS = 'Y' order by MENU_PARENTID,MENU_INDEX";
        try
        {
        		
        		Criteria crit = session.createCriteria(Menu.class);
        		crit.add(Restrictions.eq("menuStatus", "Y"))
        		.addOrder(Order.asc("menuParentId")).addOrder(Order.asc("menuIndex"));
        		List<Menu> menuList = (List<Menu>)  crit.list();
        menuData = fetchData(5, menuList);
        if(menuData!=null){
            objList.add(menuData);
           // logger.info("Return Data Length in Bean ---------->>>"+menuData.length);
        }
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        finally
        {
            session.close();
        }
        return objList;
	}

	public String[][] fetchData(int noOfColumn, List<Menu> menuList) {
		String data[][] = null;
		if(menuList == null || noOfColumn < 1 )
			return null;
		data = new String[menuList.size()][noOfColumn];
		int row = 0;
		for (Menu menu : menuList) {
			data[row][0] = menu.getMenuFName();
			data[row][1] = menu.getMenuNext();
			data[row][2] = menu.getMenuParentId();
			data[row][3] = menu.getMenuIndex();
			data[row][4] = menu.getMenuUid()+"";
			row++;
		}
		return data;
	}
	
}
