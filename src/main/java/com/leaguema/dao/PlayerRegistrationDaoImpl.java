/**
 * 
 */
package com.leaguema.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.leaguema.model.BMUser;
import com.leaguema.model.ClubRegistration;
import com.leaguema.model.PlayerRegistration;

/**
 * @author Sunil
 *
 */
@Repository
public class PlayerRegistrationDaoImpl implements IPlayerRegistrationDao {

	@Autowired
	SessionFactory sessionFactory;
	
	@Override
	public String getPlayerAlreadyRegistered(PlayerRegistration playerRegistration, boolean verifyStatus, String operationType) throws Exception {
		Session session = null;
		String result = "0";
		try {
		session = sessionFactory.openSession();
		Criteria crit = session.createCriteria(PlayerRegistration.class);
		crit.add(Restrictions.eq("playerMobile", playerRegistration.getPlayerMobile()));
		if(operationType.equalsIgnoreCase("UpdateEdit")) {
			crit.add(Restrictions.ne("id", playerRegistration.getId()));
			crit.add(Restrictions.eq("isActive", true));
		}
		else
		crit.add(Restrictions.eq("isVerifyRegistration", verifyStatus));
		PlayerRegistration playerReg = (PlayerRegistration)crit.uniqueResult();
		if(playerReg != null && playerReg.getPlayerMobile().equalsIgnoreCase(playerRegistration.getPlayerMobile())) {
			result = "mobileNoAlreadyFound";
		}
		else {
				crit = session.createCriteria(PlayerRegistration.class);
				crit.add(Restrictions.eq("playerEmail", playerRegistration.getClubName()));
				if(operationType.equalsIgnoreCase("UpdateEdit")) {
					crit.add(Restrictions.ne("id", playerRegistration.getId()));
					crit.add(Restrictions.eq("isActive", true));
				}
				else
				crit.add(Restrictions.eq("isVerifyRegistration", verifyStatus));
				playerReg = (PlayerRegistration)crit.uniqueResult();
				if(playerReg != null && playerReg.getPlayerEmail().equalsIgnoreCase(playerRegistration.getPlayerEmail())) {
					result = "emailAlreadyFound";
				}
				else {
					result = "success";
				}
			}
		}
		
		catch(Exception ex) {
			ex.printStackTrace();
			result = "failure";
		}
		finally {
			session.close();
		}
		return result;
	}

	@Override
	public String savePlayerRegistrationData(PlayerRegistration playerRegistration, long loggedClubId, String clubNo, String operationType, String CVN)
			throws Exception {
		Session session = null;
		Transaction txn = null;
		String result = "0";
		long playerId = 0;
		try {
		session = sessionFactory.openSession();
		txn = session.beginTransaction();
		
		ClubRegistration clubReg = new ClubRegistration();
		clubReg.setId(loggedClubId);
		playerRegistration.setClubRegistration(clubReg);
		/*if(operationType.equalsIgnoreCase("UpdateEdit")) {
			playerRegistration.setActive(true);
		}
		else
			playerRegistration.setActive(false);*/
		
		
		playerRegistration.setPlayerOTP(CVN);
		
		if(operationType.equalsIgnoreCase("create") || operationType.equalsIgnoreCase("createEdit")) {
			playerRegistration.setVerifyRegistration(false);
			playerRegistration.setIsActive(false);
		}
		if(operationType.equalsIgnoreCase("updateEdit")) {
			playerRegistration.setIsActive(true);
		}
		if(!playerRegistration.getPlayerID().isEmpty() && playerRegistration.getId() > 0) {
			session.update(playerRegistration);
			playerId = playerRegistration.getId();
		}
		else {
		session.saveOrUpdate(playerRegistration);
		playerId = playerRegistration.getId();
		playerRegistration.setPlayerID(playerRegistration.getClubCode()+"/"+"PLR"+playerId);
		System.out.println("return id is-->"+playerId);
		}
		
		txn.commit();
		result = playerId+"";
		}
		catch(Exception ex) {
			ex.printStackTrace();
			txn.rollback();
			result = ex.getMessage();
		}
		finally {
			session.close();
		}
		return result;
	}

	@Override
	public PlayerRegistration getRegistrationData(String regId, String operationType) throws Exception {
		Session session = null;
		Transaction txn = null;
		PlayerRegistration playerRegistration = null;
		try {
		session = sessionFactory.openSession();
		Criteria crit = session.createCriteria(PlayerRegistration.class);
		crit.add(Restrictions.eq("id", Long.parseLong(regId)));
		/*if(operationType.equalsIgnoreCase("UpdateEdit")) {
			crit.add(Restrictions.eq("isActive", true));
		}
		else
		crit.add(Restrictions.eq("isVerifyRegistration", false));*/
		playerRegistration = (PlayerRegistration)crit.uniqueResult();
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		finally {
			session.close();
		}
		return playerRegistration;
	}

	@Override
	public String saveVerifiedPlayerRegistrationData(String regId, String operationType) throws Exception {
		Session session = null;
		Transaction txn = null;
		PlayerRegistration playerRegistration = null;
		String returnValue = "failure";
		try {
		session = sessionFactory.openSession();
		txn = session.beginTransaction();
		Criteria crit = session.createCriteria(PlayerRegistration.class);
		crit.add(Restrictions.eq("id", Long.parseLong(regId)));
		//crit.add(Restrictions.eq("isVerifyRegistration", false));
		playerRegistration = (PlayerRegistration)crit.uniqueResult();
		if(playerRegistration != null) {
			if(operationType.equalsIgnoreCase("active") || operationType.equalsIgnoreCase("activate")) {
				playerRegistration.setPlayerIsCNVVerify(true);
				playerRegistration.setIsActive(true);
			}
			else if(operationType.equalsIgnoreCase("inactive") || operationType.equalsIgnoreCase("inactivate")) {
				playerRegistration.setPlayerIsCNVVerify(true);
				playerRegistration.setIsActive(false);
			}
			//playerRegistration.setActive(true);
			playerRegistration.setVerifyRegistration(true);
			session.saveOrUpdate(playerRegistration);
			returnValue = "success";
		}
		txn.commit();
		}
		catch(Exception ex) {
			ex.printStackTrace();
			txn.rollback();
			returnValue = ex.getMessage();
		}
		finally {
			session.close();
		}
		return returnValue;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PlayerRegistration> getAllPlayer(String searchCriteria) throws Exception {
		
			Session session = null;
			List<PlayerRegistration> listPlayer = null;
			try {
				session = sessionFactory.openSession();
				Criteria crit = session.createCriteria(PlayerRegistration.class);
				if(searchCriteria.equalsIgnoreCase("all"))
					crit.add(Restrictions.eq("isVerifyRegistration", true));
				else if(searchCriteria.equalsIgnoreCase("active")) {
					crit.add(Restrictions.eq("isActive", true));
					crit.add(Restrictions.eq("isVerifyRegistration", true));
				}
				listPlayer = (List<PlayerRegistration>) crit.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
			} catch (Exception ex) {
				ex.printStackTrace();
			} finally {
				session.close();
			}
			return listPlayer;
		}
	}


