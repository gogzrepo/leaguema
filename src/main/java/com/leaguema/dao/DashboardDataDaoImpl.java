/**
 * 
 */
package com.leaguema.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projection;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.leaguema.dto.PlayerRegistration;
import com.leaguema.model.BMUser;
import com.leaguema.model.ClubRegistration;
import com.leaguema.model.DashboardData;

/**
 * @author Sunil
 *
 */
@Repository
public class DashboardDataDaoImpl implements IDashboardDataDao {

	@Autowired
	SessionFactory sessionFactroy;

	@SuppressWarnings("unchecked")
	@Override
	public DashboardData loadDashboardData(long clubRegId) throws Exception {
		Session session = null;
		DashboardData dashboardData = new DashboardData();
		List<ClubRegistration> listClubData = new ArrayList<>();
		try {
			session = sessionFactroy.openSession();
			/*
			 * Criteria crit = session.createCriteria(ClubRegistration.class);
			 * crit.add(Restrictions.eq("isVerifyRegistration", true));
			 * crit.add(Restrictions.eq("isActive", true));
			 * crit.setProjection(Projections.rowCount()); Integer
			 * totalActiveClubCount = (Integer)crit.uniqueResult();
			 * dashboardData.setTotalClubs(totalActiveClubCount);
			 * dashboardData.setTotalPlayers(0);
			 * dashboardData.setTotalCoaches(totalActiveClubCount);
			 * dashboardData.setTotalEvents(0);
			 */

			Criteria crit = session.createCriteria(ClubRegistration.class);
			crit.add(Restrictions.eq("isVerifyRegistration", true));
			crit.add(Restrictions.eq("isActive", true));
			listClubData = (List<ClubRegistration>) crit.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();

			dashboardData.setListClubRegistration(listClubData);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return dashboardData;
	}

}
