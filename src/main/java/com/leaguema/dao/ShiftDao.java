package com.leaguema.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.leaguema.model.ClubRegistration;
import com.leaguema.model.ShiftMst;

@Repository
public class ShiftDao implements IShiftDao {

	
	@Autowired
	SessionFactory sessionFactroy;
	
	@Override
	public List<ShiftMst> getAllActiveShift(String status, String clubId) {
		// TODO Auto-generated method stub
		Session session = null;
		List<ShiftMst> listAllActiveShift = null;
		try {
			
			session = sessionFactroy.openSession();
			Criteria crit = session.createCriteria(ShiftMst.class);
			crit.add(Restrictions.eq("shiftMstStatus", status));
			crit.add(Restrictions.eq("shiftMstClubId", clubId));
			listAllActiveShift = (List<ShiftMst>) crit.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
			
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		finally {
			
			session.close();
		}
		return listAllActiveShift;
	}

}
