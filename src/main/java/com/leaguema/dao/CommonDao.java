/**
 * 
 */
package com.leaguema.dao;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projection;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.leaguema.model.Address;
import com.leaguema.model.Employee;

/**
 * @author Sunil
 *
 */
@Repository
public class CommonDao implements ICommonDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	public String updateMergeTest() {
		Session session = sessionFactory.openSession();
		
		Employee emp1 = (Employee)session.get(Employee.class, new Long(1));
		System.out.println("emp1.getEmpAddress()--"+emp1.getEmpAddress());
		session.close();
		emp1.setEmpAddress("nagina");//detached state object
		
		Session session2  = sessionFactory.openSession();
		Employee emp2 = (Employee)session2.get(Employee.class, new Long(1));
		System.out.println("emp2.getEmpAddress()-->"+emp2.getEmpAddress());
		emp2.setEmpName("vipin");// will not update
		Transaction txn = session2.beginTransaction();
		session2.merge(emp1);// will update in DB in transaction only
		txn.commit();
		session2.close();
		return "success";
	}

	@Transactional
	public String savePersistTest() {
		Session session = sessionFactory.openSession();
		Transaction txn = session.beginTransaction();
		
		Employee emp1 = new Employee();
		emp1.setEmpName("sunil");
		emp1.setEmpAge("26");
		emp1.setEmpAddress("seohara");
		
		Address addr1 = new Address("Seohara",246746);
		addr1.setCity("city1");
		addr1.setZipcode(246746);
		addr1.setEmp(emp1);
		
		Address addr2 = new Address("Bijnor",246701);
		addr2.setCity("city2");
		addr2.setZipcode(246701);
		addr2.setEmp(emp1);
		
		Set<Address> setAddress = new HashSet<Address>();
		setAddress.add(addr1);
		setAddress.add(addr2);
		
		emp1.setAddress(setAddress);
		
		
		session.persist(emp1);
		txn.commit();
		return "success";
	}
	
	public void CriteriaTest2() {
		
		Session session = sessionFactory.getCurrentSession();
	    Criteria crit = session.createCriteria(Employee.class);
		crit.add(Restrictions.eq("empName","sunil"));
		crit.setFirstResult(0).addOrder(Order.desc("empId"));
		crit.setMaxResults(2);
	    List<Employee> empList = (List<Employee>) crit.list();
		
		System.out.println("size-");
		
		for (Employee employee : empList) {
			System.out.println("emp->"+employee.getEmpId()+" "+employee.getEmpName()+" "+employee.getEmpAge()+" "+employee.getEmpAddress());
		}
		
	}
	public void CriteriaTest() {
		
		Session session = sessionFactory.openSession();
	    Transaction txn = session.beginTransaction();
		Employee emp = (Employee) session.get(Employee.class, new Long(1));
		
		Set<Address> addr = emp.getAddress();
		
		int i = 0;
		for (Address address : addr) {
			if(i==0) {
				session.delete(address);//session.delete("1", Address.class);
			}
			i++;
		}
		System.out.println("address removed.");
		System.out.println("address removed.");
		txn.commit();
		
	    
	    
		
	}
	/*public void CriteriaTest() {
		
		Session session = sessionFactory.getCurrentSession();
	    Criteria crit = session.createCriteria(Employee.class);
	    ProjectionList pList = Projections.projectionList();
	    pList.add(Projections.property("empName"));
	    pList.add(Projections.property("empId"));
	    crit.setProjection(pList);
	    
	    for (Object employee : crit.list()) {
			//Employee emp = Object;
	    	Object s[] = (Object[]) employee;
	    	System.out.println("employee->"+s[0]+"employee-->"+s[1]);
		}
	    
	    
		
	}*/
	
	public void HibernateHOLJoinTest() {
		
		Session session = sessionFactory.getCurrentSession();
	    Query query = session.createQuery("from Employee where empId =:empId");
	    query.setInteger("empId", 1);
		List<Employee> empList = (List<Employee>)query.list();
		System.out.println("size-");
		
		for (Employee employee : empList) {
			System.out.println("emp->"+employee.getEmpId()+" "+employee.getEmpName()+" "+employee.getEmpAge()+" "+employee.getEmpAddress());
		}
		
	}

}
