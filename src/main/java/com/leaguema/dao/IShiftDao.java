package com.leaguema.dao;

import java.util.List;

import com.leaguema.model.ShiftMst;

public interface IShiftDao {
	
	public List<ShiftMst> getAllActiveShift(String status, String clubId);


}
