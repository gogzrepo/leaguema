/**
 * 
 */
package com.leaguema.dao;

import java.util.List;

import com.leaguema.model.PlayerRegistration;

/**
 * @author Sunil
 *
 */
public interface IPlayerRegistrationDao {
	
	public String getPlayerAlreadyRegistered(PlayerRegistration playerRegistration, boolean isVerifyRegistration, String operationType) throws Exception;
	
	public String savePlayerRegistrationData(PlayerRegistration playerRegistration,long loggedClubId, String clubNo, String operationType, String CVN) throws Exception;
	
	public PlayerRegistration getRegistrationData(String regId, String operationType) throws Exception;
	
	public String saveVerifiedPlayerRegistrationData(String regId, String operationType) throws Exception;

	public List<PlayerRegistration> getAllPlayer(String searchCriteria) throws Exception;

}
