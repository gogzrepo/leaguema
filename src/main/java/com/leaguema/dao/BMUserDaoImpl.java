/**
 * 
 */
package com.leaguema.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.leaguema.model.BMUser;

@Repository
public class BMUserDaoImpl implements BMUserDao {

	@Autowired
	private SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	@Transactional
	public BMUser findByUserName(String username) {

		List<BMUser> users = new ArrayList<BMUser>();

		Criteria crit = sessionFactory.getCurrentSession()
			.createCriteria(BMUser.class);
			crit.add(Restrictions.eq("username", username));
			users = crit.list();

		if (users.size() > 0) {
			return users.get(0);
		} else {
			return null;
		}

	}

	public BMUser createUser(BMUser user) {
		
		Session session = sessionFactory.getCurrentSession();
		if(user.getUserCreateDate()==null)
			user.setUserCreateDate(new Date());
		if(user.getUserJoinDate()==null || "".equals(user.getUserJoinDate()))
			user.setUserJoinDate(new Date());
		Serializable id = session.save(user);
		System.out.println("created id is==>"+id);
		return user;
	}

	public BMUser transactionTesting() {
		Session session = sessionFactory.openSession();
		Transaction txn = null;
		try {
			txn = session.beginTransaction();
			session.createSQLQuery("insert into txn1 values(1,'sunil1','seohara1') ").executeUpdate();
			session.createSQLQuery("insert into txn1 values(2,'sunil12345678901234567','seohara1')").executeUpdate();
			txn.commit();
		}
		catch(Exception ex) {
			ex.printStackTrace();
			txn.rollback();
		}
		return new BMUser();
	}

}