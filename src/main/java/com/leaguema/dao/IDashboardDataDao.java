package com.leaguema.dao;

import java.util.List;

import com.leaguema.model.ClubRegistration;
import com.leaguema.model.DashboardData;

public interface IDashboardDataDao {
	
	public DashboardData loadDashboardData(long userId) throws Exception;

}
