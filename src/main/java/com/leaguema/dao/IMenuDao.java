package com.leaguema.dao;

import java.sql.SQLException;
import java.util.List;

public interface IMenuDao {
	
	public List<Object> getMenuData(String userId, String desigId) throws SQLException;

}
