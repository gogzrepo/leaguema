/**
 * 
 */
package com.leaguema.dao;

import com.leaguema.model.BMUser;

/**
 * @author Sunil
 *
 */
public interface BMUserDao {

	BMUser findByUserName(String username);
	
	public BMUser createUser(BMUser user);
	
	//public BMUser transactionTesting();
	
}
