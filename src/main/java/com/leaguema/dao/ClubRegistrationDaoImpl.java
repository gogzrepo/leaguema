/**
 * 
 */
package com.leaguema.dao;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.leaguema.commonfun.CommonConstants;
import com.leaguema.commonfun.GenerateOTP;
import com.leaguema.model.BMUser;
import com.leaguema.model.BMUserRole;
import com.leaguema.model.ClubRegistration;
import com.leaguema.model.ClubValidationNumber;

/**
 * @author Sunil
 *
 */
@Repository
public class ClubRegistrationDaoImpl implements ClubRegistrationDao {

	@Autowired
	SessionFactory sessionFactroy;

	@Override
	public String saveClubRegistrationData(ClubRegistration clubRegistration, BMUser loggedUser, String operationType)
			throws Exception {
		Session session = null;
		Transaction txn = null;
		String result = "0";
		long clubId = 0;
		boolean isNeedToUpdate = false;
		boolean isOTPSend = false;
		try {
			session = sessionFactroy.openSession();
			txn = session.beginTransaction();

			if (operationType.equalsIgnoreCase("updateEdit")) {

					//call OTP generation logic here and put into setter method and set isOTPVerify as false
				    //String OTP=GenerateOTP.getOTP(CommonConstants.OTP_LENGTH);
				    //clubRegistration.setClubOTP(OTP);
				    //clubRegistration.setClubIsOTPVerify(false);

				    clubRegistration.setClubParentId(loggedUser.getUserClubRegitrationId());
					clubRegistration.setClubParentCode(loggedUser.getUserEmpCode() + "");
					session.saveOrUpdate(clubRegistration);
					clubId = clubRegistration.getId();
					if(clubRegistration.getClubType().equalsIgnoreCase("sub"))
						clubRegistration.setClubCode(clubRegistration.getClubParentCode() + "/" + "SUB"+clubId);
					else if(clubRegistration.getClubType().equalsIgnoreCase("super"))
						clubRegistration.setClubCode(clubRegistration.getClubParentCode());
					System.out.println("return id is-->" + clubId);
				BMUser user = new BMUser();
				BMUserRole userRole = null;
				if (clubId > 0) {
					if (!clubRegistration.getClubCode().isEmpty() && clubRegistration.getId() > 0) {
						Criteria crit = session.createCriteria(BMUser.class);
						crit.add(Restrictions.eq("userClubRegitrationId", clubRegistration.getId()));
						user = (BMUser) crit.uniqueResult();
						if (user == null) {
							user = new BMUser();
							isNeedToUpdate = true;
						}
					}
					user.setUserClubRegitrationId(clubId);
					user.setVerifyRegistration(false);
					user.setPassword(clubRegistration.getClubPwd());
					user.setUserEmpCode(clubRegistration.getClubCode());
					user.setUserClubName(clubRegistration.getClubName());
					user.setUsername(clubRegistration.getMobileNo());
					user.setUserFName(clubRegistration.getClubFName());
					user.setUserLName(clubRegistration.getClubLName());
					user.setUserCreateDate(new java.util.Date());
					user.setUserJoinDate(new java.util.Date());
					userRole = new BMUserRole();
					userRole.setUser(user);
					userRole.setRole(clubRegistration.getRoleInClub());
					Set<BMUserRole> userRoleSet = new HashSet<>();
					userRoleSet.add(userRole);
					user.setUserRole(userRoleSet);

					session.saveOrUpdate(user);
					if (isNeedToUpdate) {
						long userId = user.getUserUid();
						if (userId > 0) {
							session.saveOrUpdate(userRole);
							System.out.println("data saved in user table successfully!!");
						}
					}
					System.out.println("data saved in user table successfully!!");
				}
			
			} else {
				//call OTP generation logic here and put into setter method and set isOTPVerify as false
				//String OTP=GenerateOTP.getOTP(CommonConstants.OTP_LENGTH);
			    //clubRegistration.setClubOTP(OTP);
			    clubRegistration.setClubIsOTPVerify(false);
			    
				clubRegistration.setIsActive(false);
				clubRegistration.setVerifyRegistration(false);
				// clubRegistration.setClubType("Sub");
				clubRegistration.setClubParentId(loggedUser.getUserClubRegitrationId());
				clubRegistration.setClubParentCode(loggedUser.getUserEmpCode() + "");
				if (!clubRegistration.getClubCode().isEmpty() && clubRegistration.getId() > 0) {
					session.update(clubRegistration);
					clubId = clubRegistration.getId();
					if(clubRegistration.getClubType().equalsIgnoreCase("sub"))
						clubRegistration.setClubCode(clubRegistration.getClubParentCode() + "/" + "SUB"+clubId);
					else if(clubRegistration.getClubType().equalsIgnoreCase("super"))
						clubRegistration.setClubCode(clubRegistration.getClubParentCode());
				} else {
					session.saveOrUpdate(clubRegistration);
					clubId = clubRegistration.getId();
					if(clubRegistration.getClubType().equalsIgnoreCase("sub"))
						clubRegistration.setClubCode(clubRegistration.getClubParentCode() + "/" + "SUB"+clubId);
					else if(clubRegistration.getClubType().equalsIgnoreCase("super"))
						clubRegistration.setClubCode(clubRegistration.getClubParentCode());
					System.out.println("return id is-->" + clubId);
				}
				BMUser user = new BMUser();
				BMUserRole userRole = null;
				if (clubId > 0) {
					if (!clubRegistration.getClubCode().isEmpty() && clubRegistration.getId() > 0) {
						Criteria crit = session.createCriteria(BMUser.class);
						crit.add(Restrictions.eq("userClubRegitrationId", clubRegistration.getId()));
						user = (BMUser) crit.uniqueResult();
						if (user == null) {
							user = new BMUser();
							isNeedToUpdate = true;
						}
					}
					user.setUserClubRegitrationId(clubId);
					user.setVerifyRegistration(false);
					user.setEnabled(false);
					user.setPassword(clubRegistration.getClubPwd());
					user.setUserEmpCode(clubRegistration.getClubCode());
					user.setUserClubName(clubRegistration.getClubName());
					user.setUsername(clubRegistration.getMobileNo());
					user.setUserFName(clubRegistration.getClubFName());
					user.setUserLName(clubRegistration.getClubLName());
					user.setUserCreateDate(new java.util.Date());
					user.setUserJoinDate(new java.util.Date());
					userRole = new BMUserRole();
					userRole.setUser(user);
					userRole.setRole(clubRegistration.getRoleInClub());
					Set<BMUserRole> userRoleSet = new HashSet<>();
					userRoleSet.add(userRole);
					user.setUserRole(userRoleSet);

					session.saveOrUpdate(user);
					if (isNeedToUpdate) {
						long userId = user.getUserUid();
						if (userId > 0) {
							session.saveOrUpdate(userRole);
							System.out.println("data saved in user table successfully!!");
						}
					}
					System.out.println("data saved in user table successfully!!");
				}
			}
			txn.commit();
			result = clubId + "";
			isOTPSend = true;
		} catch (Exception ex) {
			ex.printStackTrace();
			txn.rollback();
			result = ex.getMessage();
			isOTPSend = false;
		} finally {
			session.close();
			if(isOTPSend) {
				//Send otp logic here if any exception not occurr
			}
		}
		return result;
	}

	@Override
	public ClubRegistration getRegistrationData(String regId, String operationType) throws Exception {
		Session session = null;
		Transaction txn = null;
		ClubRegistration clubRegistration = null;
		try {
			session = sessionFactroy.openSession();
			Criteria crit = session.createCriteria(ClubRegistration.class);
			crit.add(Restrictions.eq("id", Long.parseLong(regId)));

			/*if (operationType.equalsIgnoreCase("updateEdit"))
				crit.add(Restrictions.eq("isActive", true));
			else
				crit.add(Restrictions.eq("isVerifyRegistration", false));*/

			clubRegistration = (ClubRegistration) crit.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
					.uniqueResult();
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return clubRegistration;
	}

	@Override
	public String saveVerifyRegistrationData(String regId, String operationType) throws Exception {
		Session session = null;
		Transaction txn = null;
		ClubRegistration clubRegistration = null;
		String returnValue = "failure";
		try {
			session = sessionFactroy.openSession();
			txn = session.beginTransaction();
			Criteria crit = session.createCriteria(ClubRegistration.class);
			crit.add(Restrictions.eq("id", Long.parseLong(regId)));
			if(operationType.equalsIgnoreCase("updateEdit")) {
				crit.add(Restrictions.eq("isActive", true));
			}
			else if(operationType.equalsIgnoreCase("active") || operationType.equalsIgnoreCase("activate"))
				crit.add(Restrictions.eq("isActive", false));
			else if(operationType.equalsIgnoreCase("inactive") || operationType.equalsIgnoreCase("inactivate"))
				crit.add(Restrictions.eq("isActive", true));
			else
				crit.add(Restrictions.eq("isVerifyRegistration", false));
			clubRegistration = (ClubRegistration) crit.uniqueResult();
			if (clubRegistration != null) {
				///set isOTPVerify as true here
				if(operationType.equalsIgnoreCase("active") || operationType.equalsIgnoreCase("activate")) {
					clubRegistration.setClubIsOTPVerify(true);
					clubRegistration.setIsActive(true);
				}
				else if(operationType.equalsIgnoreCase("inactive") || operationType.equalsIgnoreCase("inactivate")) {
					clubRegistration.setIsActive(false);
				}
				clubRegistration.setVerifyRegistration(true);
				session.saveOrUpdate(clubRegistration);
				crit = session.createCriteria(BMUser.class);
				crit.add(Restrictions.eq("userClubRegitrationId", Long.parseLong(regId)));
				if(operationType.equalsIgnoreCase("updateEdit")) {
					crit.add(Restrictions.eq("enabled", true));
				}
				else if(operationType.equalsIgnoreCase("active") || operationType.equalsIgnoreCase("activate")) {
					crit.add(Restrictions.eq("enabled", false));
					crit.add(Restrictions.eq("isVerifyRegistration", true));
				}
				else if(operationType.equalsIgnoreCase("inactive") || operationType.equalsIgnoreCase("inactivate")) {
					crit.add(Restrictions.eq("enabled", true));
					crit.add(Restrictions.eq("isVerifyRegistration", true));
				}
				else {
					crit.add(Restrictions.eq("enabled", false));
					crit.add(Restrictions.eq("isVerifyRegistration", false));
				}
				BMUser user = (BMUser) crit.uniqueResult();
				if (user != null) {
					
					if(operationType.equalsIgnoreCase("active") || operationType.equalsIgnoreCase("activate"))
						user.setEnabled(true);
					else if(operationType.equalsIgnoreCase("inactive") || operationType.equalsIgnoreCase("inactivate")) 
						user.setEnabled(false);
					
					user.setVerifyRegistration(true);
					session.saveOrUpdate(user);
					returnValue = "success";
					if(operationType.equalsIgnoreCase("create") || operationType.equalsIgnoreCase("createEdit")) {
						String OTP=GenerateOTP.getOTP(CommonConstants.OTP_LENGTH);
					    clubRegistration.setClubOTP(OTP);
					    clubRegistration.setClubIsOTPVerify(false);
					    
					    //save data in club_cvn table to save cvn no for activate club
					    
					    ClubValidationNumber cvn = new ClubValidationNumber();
					    cvn.setActive(true);
					    cvn.setClubActivationCode(OTP);
					    cvn.setClubCode(clubRegistration.getClubCode());
					    cvn.setClubId(clubRegistration.getId());
					    
					    session.saveOrUpdate(cvn);
					    returnValue = "success";
					}
				}
			}
			txn.commit();
		} catch (Exception ex) {
			ex.printStackTrace();
			txn.rollback();
			returnValue = ex.getMessage();
		} finally {
			session.close();
		}
		return returnValue;
	}

	@Override
	public String getClubAlreadyRegistered(ClubRegistration clubRegistration, boolean verifyStatus,
			String operationType) throws Exception {
		Session session = null;
		String result = "0";
		try {
			session = sessionFactroy.openSession();
			Criteria crit = session.createCriteria(ClubRegistration.class);
			crit.add(Restrictions.eq("mobileNo", clubRegistration.getMobileNo()));
			if (operationType.equals("updateEdit")) {
				crit.add(Restrictions.ne("id", clubRegistration.getId()));
				crit.add(Restrictions.eq("isActive", true));
			} else
				crit.add(Restrictions.eq("isVerifyRegistration", verifyStatus));
			ClubRegistration clubReg = (ClubRegistration) crit.uniqueResult();
			if (clubReg != null && clubReg.getMobileNo().equalsIgnoreCase(clubRegistration.getMobileNo())) {
				result = "mobileNoAlreadyFound";
			} else {
				crit = session.createCriteria(ClubRegistration.class);
				crit.add(Restrictions.eq("clubName", clubRegistration.getClubName()));
				if (operationType.equals("updateEdit")) {
					crit.add(Restrictions.ne("id", clubRegistration.getId()));
					crit.add(Restrictions.eq("isActive", true));
				} else
					crit.add(Restrictions.eq("isVerifyRegistration", verifyStatus));
				clubReg = (ClubRegistration) crit.uniqueResult();
				if (clubReg != null && clubReg.getClubName().equalsIgnoreCase(clubRegistration.getClubName())) {
					result = "clubNameAlreadyFound";
				} else {
					result = "success";
				}
			}
		}

		catch (Exception ex) {
			ex.printStackTrace();
			result = "failure";
		} finally {
			session.close();
		}
		return result;
	}

	@Override
	public BMUser getLoggedUserData(String username) {
		Session session = null;
		BMUser user = new BMUser();
		try {
			session = sessionFactroy.openSession();
			Criteria crit = session.createCriteria(BMUser.class);
			crit.add(Restrictions.eq("username", username));
			crit.add(Restrictions.eq("enabled", true));
			crit.add(Restrictions.eq("isVerifyRegistration", true));
			user = (BMUser) crit.uniqueResult();
			
			crit = session.createCriteria(ClubValidationNumber.class);
			crit.add(Restrictions.eq("id", user.getUserClubRegitrationId()));
			ClubValidationNumber cvn = (ClubValidationNumber)crit.add(Restrictions.eq("isActive", true)).uniqueResult();
			if(cvn.getClubActivationCode() == null || cvn.getClubActivationCode().equals("") || cvn.getClubActivationCode().isEmpty())
				user.setUserOTP("0");
			else
				user.setUserOTP(cvn.getClubActivationCode());
			
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return user;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ClubRegistration> getAllActiveClub(String clubStatus) throws Exception {

		Session session = null;
		List<ClubRegistration> listClub = null;
		try {
			session = sessionFactroy.openSession();
			Criteria crit = session.createCriteria(ClubRegistration.class);
			if(clubStatus.equalsIgnoreCase("active")) {
			crit.add(Restrictions.eq("isActive", true));
			crit.add(Restrictions.eq("isVerifyRegistration", true));
			}
			listClub = (List<ClubRegistration>) crit.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return listClub;
	}
}
