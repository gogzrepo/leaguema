package com.leaguema.dao;

import java.util.List;

import com.leaguema.model.BMUser;
import com.leaguema.model.ClubRegistration;

public interface ClubRegistrationDao {
	
	public String getClubAlreadyRegistered(ClubRegistration clubRegistration,boolean verifyStatus, String operationType) throws Exception;
	
	public String saveClubRegistrationData(ClubRegistration clubRegistration, BMUser user, String operationType) throws Exception;
	
	public ClubRegistration getRegistrationData(String regId, String operationType) throws Exception;
	
	public String saveVerifyRegistrationData(String regId, String operationType) throws Exception;
	
	public BMUser getLoggedUserData(String username) throws Exception;
	
	public List<ClubRegistration> getAllActiveClub(String clubStatus) throws Exception;

}
