/**
 * 
 */
package com.leaguema.commonfun;

import java.util.Random;

/**
 * @author Sunil
 *
 */
public class GenerateOTP {
	
	
	public static String getOTP(int len)
	    {
	        String numbers = "0123456789";
	        Random rndm_method = new Random();
	 
	        String otp = "";
	        for (int i = 0; i < len; i++)
	        {
	            otp += numbers.charAt(rndm_method.nextInt(numbers.length()));
	        }
	        return otp;
	    }
} 


