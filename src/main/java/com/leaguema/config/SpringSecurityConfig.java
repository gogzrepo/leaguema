package com.leaguema.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	@Qualifier("userDetailsService")
	UserDetailsService userDetailsService;

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService);
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		
		http.authorizeRequests()
		.antMatchers("/testing/**").permitAll()
		.antMatchers("/","/home").permitAll()
		.antMatchers("/cmnurl/**")
				.access("hasRole('ROLE_SUPER_ADMIN') "
				+ "or hasRole('ROLE_SUPER_DBA') "
				+ "or hasRole('ROLE_CLUB_ADMIN') "
				+ "or hasRole('ROLE_SUPER_DBA') "
				+ "or hasRole('ROLE_CLUB_USER') "
				+ "or hasRole('ROLE_CLUB_MEMBER') ")
		.antMatchers("/user/**").access("hasRole('ROLE_SUPER_ADMIN') or hasRole('ROLE_SUPER_DBA')")
		.antMatchers("/club/**").access("hasRole('ROLE_SUPER_ADMIN')")
		.antMatchers("/player/**").access("hasRole('ROLE_SUPER_ADMIN') or hasRole('ROLE_CLUB_ADMIN') ")
		.antMatchers("/subclub/**").access("hasRole('ROLE_CLUB_ADMIN') or hasRole('ROLE_SUPER_ADMIN')")
		.antMatchers("/admin/**")
			.access("hasRole('ROLE_SUPER_ADMIN')").
			antMatchers("/dba/**")
			.access("hasRole('ROLE_SUPER_ADMIN') or hasRole('ROLE_SUPER_DBA') ")
			.and().formLogin()
			.loginPage("/login").failureUrl("/login?error")
				.usernameParameter("username")
				.passwordParameter("password")
				.and().logout().logoutSuccessUrl("/login?logout")
				//.and().csrf()
				.and().exceptionHandling().accessDeniedPage("/accessDenied");
		
		http.csrf().disable();
	}
	
	@Bean
	public PasswordEncoder passwordEncoder(){
		PasswordEncoder encoder = new BCryptPasswordEncoder();
		return encoder;
	}
	
}